package org.suny.model;

/**
 * Bts559Id entity. @author MyEclipse Persistence Tools
 */
public class Bts559Id extends AbstractBts559Id implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Bts559Id() {
	}

	/** full constructor */
	public Bts559Id(String gene, Integer id) {
		super(gene, id);
	}

}
