package org.suny.model;

import java.sql.Timestamp;

/**
 * Bts559history entity. @author MyEclipse Persistence Tools
 */
public class Bts559history extends AbstractBts559history implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Bts559history() {
	}

	/** full constructor */
	public Bts559history(String gene, Integer oid, String comments,
			String user, String datecreated, Timestamp datemodified,
			String uesrdate) {
		super(gene, oid, comments, user, datecreated, datemodified, uesrdate);
	}

}
