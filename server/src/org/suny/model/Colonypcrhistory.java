package org.suny.model;

import java.sql.Timestamp;

/**
 * Colonypcrhistory entity. @author MyEclipse Persistence Tools
 */
public class Colonypcrhistory extends AbstractColonypcrhistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Colonypcrhistory() {
	}

	/** full constructor */
	public Colonypcrhistory(String gene, Integer oid, String primer1,
			String primer2, String size, String date1, String date2,
			String date3, String seq, String comments, String datecreated,
			Timestamp datemodified) {
		super(gene, oid, primer1, primer2, size, date1, date2, date3, seq,
				comments, datecreated, datemodified);
	}

}
