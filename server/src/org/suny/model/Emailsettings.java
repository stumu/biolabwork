package org.suny.model;

/**
 * Emailsettings entity. @author MyEclipse Persistence Tools
 */
public class Emailsettings extends AbstractEmailsettings implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Emailsettings() {
	}

	/** full constructor */
	public Emailsettings(String username, String password, String alias,
			String smtpserver, String toaddr) {
		super(username, password, alias, smtpserver, toaddr);
	}

}
