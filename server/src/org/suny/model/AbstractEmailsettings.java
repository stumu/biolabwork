package org.suny.model;

/**
 * AbstractEmailsettings entity provides the base persistence definition of the
 * Emailsettings entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractEmailsettings implements java.io.Serializable {

	// Fields

	private String username;
	private String password;
	private String alias;
	private String smtpserver;
	private String toaddr;

	// Constructors

	/** default constructor */
	public AbstractEmailsettings() {
	}

	/** full constructor */
	public AbstractEmailsettings(String username, String password,
			String alias, String smtpserver, String toaddr) {
		this.username = username;
		this.password = password;
		this.alias = alias;
		this.smtpserver = smtpserver;
		this.toaddr = toaddr;
	}

	// Property accessors

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAlias() {
		return this.alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getSmtpserver() {
		return this.smtpserver;
	}

	public void setSmtpserver(String smtpserver) {
		this.smtpserver = smtpserver;
	}

	public String getToaddr() {
		return this.toaddr;
	}

	public void setToaddr(String toaddr) {
		this.toaddr = toaddr;
	}

}