package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractQuickchnage entity provides the base persistence definition of the
 * Quickchnage entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractQuickchnage implements java.io.Serializable {

	// Fields

	private QuickchnageId id;
	private String userdate;
	private String user;
	private String comments;
	private String datecreated;
	private Timestamp datemodified;

	// Constructors

	/** default constructor */
	public AbstractQuickchnage() {
	}

	/** full constructor */
	public AbstractQuickchnage(QuickchnageId id, String userdate, String user,
			String comments, String datecreated, Timestamp datemodified) {
		this.id = id;
		this.userdate = userdate;
		this.user = user;
		this.comments = comments;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
	}

	// Property accessors

	public QuickchnageId getId() {
		return this.id;
	}

	public void setId(QuickchnageId id) {
		this.id = id;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

}