package org.suny.model;

import java.sql.Timestamp;

/**
 * Colonypcr entity. @author MyEclipse Persistence Tools
 */
public class Colonypcr extends AbstractColonypcr implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Colonypcr() {
	}

	/** full constructor */
	public Colonypcr(ColonypcrId id, String primer1, String primer2,
			String size, String date1, String date2, String date3, String seq,
			String comments, String datecreated, Timestamp datemodified) {
		super(id, primer1, primer2, size, date1, date2, date3, seq, comments,
				datecreated, datemodified);
	}

}
