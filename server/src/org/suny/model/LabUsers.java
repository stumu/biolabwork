package org.suny.model;

/**
 * LabUsers entity. @author MyEclipse Persistence Tools
 */
public class LabUsers extends AbstractLabUsers implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public LabUsers() {
	}

	/** full constructor */
	public LabUsers(String username, String password) {
		super(username, password);
	}

}
