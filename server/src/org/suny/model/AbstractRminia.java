package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractRminia entity provides the base persistence definition of the Rminia
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractRminia implements java.io.Serializable {

	// Fields

	private RminiaId id;
	private String ml;
	private String sequence;
	private String comments;
	private String user;
	private String datecreated;
	private Timestamp datemodified;
	private String colony;
	private String userdate;

	// Constructors

	/** default constructor */
	public AbstractRminia() {
	}

	/** full constructor */
	public AbstractRminia(RminiaId id, String ml, String sequence,
			String comments, String user, String datecreated,
			Timestamp datemodified, String colony, String userdate) {
		this.id = id;
		this.ml = ml;
		this.sequence = sequence;
		this.comments = comments;
		this.user = user;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
		this.colony = colony;
		this.userdate = userdate;
	}

	// Property accessors

	public RminiaId getId() {
		return this.id;
	}

	public void setId(RminiaId id) {
		this.id = id;
	}

	public String getMl() {
		return this.ml;
	}

	public void setMl(String ml) {
		this.ml = ml;
	}

	public String getSequence() {
		return this.sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

	public String getColony() {
		return this.colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

}