package org.suny.model;

import java.sql.Timestamp;

/**
 * Miniahistory entity. @author MyEclipse Persistence Tools
 */
public class Miniahistory extends AbstractMiniahistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Miniahistory() {
	}

	/** full constructor */
	public Miniahistory(String gene, Integer oid, String ml, String sequence,
			String comments, String user, String datecreated,
			Timestamp datemodified, String colony, String uesrdate) {
		super(gene, oid, ml, sequence, comments, user, datecreated,
				datemodified, colony, uesrdate);
	}

}
