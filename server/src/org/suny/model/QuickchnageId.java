package org.suny.model;

/**
 * QuickchnageId entity. @author MyEclipse Persistence Tools
 */
public class QuickchnageId extends AbstractQuickchnageId implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public QuickchnageId() {
	}

	/** full constructor */
	public QuickchnageId(String gene, Integer id) {
		super(gene, id);
	}

}
