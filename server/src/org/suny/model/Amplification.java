package org.suny.model;

import java.sql.Timestamp;

/**
 * Amplification entity. @author MyEclipse Persistence Tools
 */
public class Amplification extends AbstractAmplification implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Amplification() {
	}

	/** full constructor */
	public Amplification(String gene, String ml, String volume, String excess,
			String store, String position, String comments, String id,
			String datecreated, Timestamp datemodified, String user,
			String userdate) {
		super(gene, ml, volume, excess, store, position, comments, id,
				datecreated, datemodified, user, userdate);
	}

}
