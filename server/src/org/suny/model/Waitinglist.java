package org.suny.model;

/**
 * Waitinglist entity. @author MyEclipse Persistence Tools
 */
public class Waitinglist extends AbstractWaitinglist implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Waitinglist() {
	}

	/** full constructor */
	public Waitinglist(Integer labId, String labName, String personName,
			Long dateCreated, Long dateModified, Long releaseDate,
			Integer isactive,String email, String phone) {
		super(labId, labName, personName, dateCreated, dateModified,
				releaseDate, isactive,email,phone);
	}

}
