package org.suny.model;

/**
 * Primersequence entity. @author MyEclipse Persistence Tools
 */
public class Primersequence extends AbstractPrimersequence implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Primersequence() {
	}

	/** full constructor */
	public Primersequence(PrimersequenceId id, String sequence, String lcstart,
			String lcend, String plate, String well, String user) {
		super(id, sequence, lcstart, lcend, plate, well, user);
	}

}
