package org.suny.model;

import java.sql.Timestamp;

/**
 * Quickchnagehistory entity. @author MyEclipse Persistence Tools
 */
public class Quickchnagehistory extends AbstractQuickchnagehistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Quickchnagehistory() {
	}

	/** full constructor */
	public Quickchnagehistory(String gene, Integer oid, String userdate,
			String user, String comments, String datecreated,
			Timestamp datemodified) {
		super(gene, oid, userdate, user, comments, datecreated, datemodified);
	}

}
