package org.suny.model;

/**
 * RminiaId entity. @author MyEclipse Persistence Tools
 */
public class RminiaId extends AbstractRminiaId implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public RminiaId() {
	}

	/** full constructor */
	public RminiaId(String gene, Integer id) {
		super(gene, id);
	}

}
