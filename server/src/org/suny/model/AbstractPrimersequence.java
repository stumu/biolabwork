package org.suny.model;

/**
 * AbstractPrimersequence entity provides the base persistence definition of the
 * Primersequence entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPrimersequence implements java.io.Serializable {

	// Fields

	private PrimersequenceId id;
	private String sequence;
	private String lcstart;
	private String lcend;
	private String plate;
	private String well;
	private String user;

	// Constructors

	/** default constructor */
	public AbstractPrimersequence() {
	}

	/** full constructor */
	public AbstractPrimersequence(PrimersequenceId id, String sequence,
			String lcstart, String lcend, String plate, String well, String user) {
		this.id = id;
		this.sequence = sequence;
		this.lcstart = lcstart;
		this.lcend = lcend;
		this.plate = plate;
		this.well = well;
		this.user = user;
	}

	// Property accessors

	public PrimersequenceId getId() {
		return this.id;
	}

	public void setId(PrimersequenceId id) {
		this.id = id;
	}

	public String getSequence() {
		return this.sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getLcstart() {
		return this.lcstart;
	}

	public void setLcstart(String lcstart) {
		this.lcstart = lcstart;
	}

	public String getLcend() {
		return this.lcend;
	}

	public void setLcend(String lcend) {
		this.lcend = lcend;
	}

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getWell() {
		return this.well;
	}

	public void setWell(String well) {
		this.well = well;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}