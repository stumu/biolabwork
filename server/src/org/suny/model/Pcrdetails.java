package org.suny.model;

import java.sql.Timestamp;

/**
 * Pcrdetails entity. @author MyEclipse Persistence Tools
 */
public class Pcrdetails extends AbstractPcrdetails implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Pcrdetails() {
	}

	/** full constructor */
	public Pcrdetails(String gene, String size, String ampliconn,
			String amplicony, String user, String userdate, String datecreated,
			Timestamp datemodified, String comments) {
		super(gene, size, ampliconn, amplicony, user, userdate, datecreated,
				datemodified, comments);
	}

}
