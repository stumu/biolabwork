package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractColonypcrhistory entity provides the base persistence definition of
 * the Colonypcrhistory entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractColonypcrhistory implements java.io.Serializable {

	// Fields

	private Integer id;
	private String gene;
	private Integer oid;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String comments;
	private String datecreated;
	private Timestamp datemodified;

	// Constructors

	/** default constructor */
	public AbstractColonypcrhistory() {
	}

	/** full constructor */
	public AbstractColonypcrhistory(String gene, Integer oid, String primer1,
			String primer2, String size, String date1, String date2,
			String date3, String seq, String comments, String datecreated,
			Timestamp datemodified) {
		this.gene = gene;
		this.oid = oid;
		this.primer1 = primer1;
		this.primer2 = primer2;
		this.size = size;
		this.date1 = date1;
		this.date2 = date2;
		this.date3 = date3;
		this.seq = seq;
		this.comments = comments;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getPrimer1() {
		return this.primer1;
	}

	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}

	public String getPrimer2() {
		return this.primer2;
	}

	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getDate1() {
		return this.date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return this.date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getDate3() {
		return this.date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public String getSeq() {
		return this.seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

}