package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractMiniahistory entity provides the base persistence definition of the
 * Miniahistory entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractMiniahistory implements java.io.Serializable {

	// Fields

	private Integer id;
	private String gene;
	private Integer oid;
	private String ml;
	private String sequence;
	private String comments;
	private String user;
	private String datecreated;
	private Timestamp datemodified;
	private String colony;
	private String uesrdate;

	// Constructors

	/** default constructor */
	public AbstractMiniahistory() {
	}

	/** full constructor */
	public AbstractMiniahistory(String gene, Integer oid, String ml,
			String sequence, String comments, String user, String datecreated,
			Timestamp datemodified, String colony, String uesrdate) {
		this.gene = gene;
		this.oid = oid;
		this.ml = ml;
		this.sequence = sequence;
		this.comments = comments;
		this.user = user;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
		this.colony = colony;
		this.uesrdate = uesrdate;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getMl() {
		return this.ml;
	}

	public void setMl(String ml) {
		this.ml = ml;
	}

	public String getSequence() {
		return this.sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

	public String getColony() {
		return this.colony;
	}

	public void setColony(String colony) {
		this.colony = colony;
	}

	public String getUesrdate() {
		return this.uesrdate;
	}

	public void setUesrdate(String uesrdate) {
		this.uesrdate = uesrdate;
	}

}