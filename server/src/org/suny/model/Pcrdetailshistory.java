package org.suny.model;

import java.sql.Timestamp;

/**
 * Pcrdetailshistory entity. @author MyEclipse Persistence Tools
 */
public class Pcrdetailshistory extends AbstractPcrdetailshistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Pcrdetailshistory() {
	}

	/** full constructor */
	public Pcrdetailshistory(String gene, String size, String ampliconn,
			String amplicony, String user, String userdate, String datecreated,
			Timestamp datemodified, String comments) {
		super(gene, size, ampliconn, amplicony, user, userdate, datecreated,
				datemodified, comments);
	}

}
