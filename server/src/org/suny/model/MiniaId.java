package org.suny.model;

/**
 * MiniaId entity. @author MyEclipse Persistence Tools
 */
public class MiniaId extends AbstractMiniaId implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public MiniaId() {
	}

	/** full constructor */
	public MiniaId(String gene, Integer id) {
		super(gene, id);
	}

}
