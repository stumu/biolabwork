package org.suny.model;

import java.sql.Timestamp;

/**
 * Rpcrdetailshistory entity. @author MyEclipse Persistence Tools
 */
public class Rpcrdetailshistory extends AbstractRpcrdetailshistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Rpcrdetailshistory() {
	}

	/** full constructor */
	public Rpcrdetailshistory(String gene, String size, String ampliconn,
			String amplicony, String user, String userdate, String datecreated,
			Timestamp datemodified, String comments) {
		super(gene, size, ampliconn, amplicony, user, userdate, datecreated,
				datemodified, comments);
	}

}
