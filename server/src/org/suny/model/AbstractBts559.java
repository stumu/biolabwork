package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractBts559 entity provides the base persistence definition of the Bts559
 * entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBts559 implements java.io.Serializable {

	// Fields

	private Bts559Id id;
	private String comments;
	private String user;
	private String datecreated;
	private Timestamp datemodified;
	private String userdate;

	// Constructors

	/** default constructor */
	public AbstractBts559() {
	}

	/** full constructor */
	public AbstractBts559(Bts559Id id, String comments, String user,
			String datecreated, Timestamp datemodified, String userdate) {
		this.id = id;
		this.comments = comments;
		this.user = user;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
		this.userdate = userdate;
	}

	// Property accessors

	public Bts559Id getId() {
		return this.id;
	}

	public void setId(Bts559Id id) {
		this.id = id;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

}