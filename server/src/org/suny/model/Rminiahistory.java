package org.suny.model;

import java.sql.Timestamp;

/**
 * Rminiahistory entity. @author MyEclipse Persistence Tools
 */
public class Rminiahistory extends AbstractRminiahistory implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Rminiahistory() {
	}

	/** full constructor */
	public Rminiahistory(String gene, Integer oid, String ml, String sequence,
			String comments, String user, String datecreated,
			Timestamp datemodified, String colony, String userdate) {
		super(gene, oid, ml, sequence, comments, user, datecreated,
				datemodified, colony, userdate);
	}

}
