package org.suny.model;

/**
 * AbstractBasicgenedetails entity provides the base persistence definition of
 * the Basicgenedetails entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBasicgenedetails implements java.io.Serializable {

	// Fields

	private String name;
	private String strand;
	private String txstart;
	private String txend;
	private String nterm;
	private String cterm;
	private String comments;

	// Constructors

	/** default constructor */
	public AbstractBasicgenedetails() {
	}

	/** full constructor */
	public AbstractBasicgenedetails(String name, String strand, String txstart,
			String txend, String nterm, String cterm, String comments) {
		this.name = name;
		this.strand = strand;
		this.txstart = txstart;
		this.txend = txend;
		this.nterm = nterm;
		this.cterm = cterm;
		this.comments = comments;
	}

	// Property accessors

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrand() {
		return this.strand;
	}

	public void setStrand(String strand) {
		this.strand = strand;
	}

	public String getTxstart() {
		return this.txstart;
	}

	public void setTxstart(String txstart) {
		this.txstart = txstart;
	}

	public String getTxend() {
		return this.txend;
	}

	public void setTxend(String txend) {
		this.txend = txend;
	}

	public String getNterm() {
		return this.nterm;
	}

	public void setNterm(String nterm) {
		this.nterm = nterm;
	}

	public String getCterm() {
		return this.cterm;
	}

	public void setCterm(String cterm) {
		this.cterm = cterm;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}