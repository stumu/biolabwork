package org.suny.model;



/**
 * MachineInfo entity. @author MyEclipse Persistence Tools
 */
public class MachineInfo extends AbstractMachineInfo implements java.io.Serializable {

    // Constructors

    /** default constructor */
    public MachineInfo() {
    }

    
    /** full constructor */
    public MachineInfo(String machine, String serialNo) {
        super(machine, serialNo);        
    }
   
}
