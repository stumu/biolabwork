package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractPcrdetails entity provides the base persistence definition of the
 * Pcrdetails entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPcrdetails implements java.io.Serializable {

	// Fields

	private String gene;
	private String size;
	private String ampliconn;
	private String amplicony;
	private String user;
	private String userdate;
	private String datecreated;
	private Timestamp datemodified;
	private String comments;

	// Constructors

	/** default constructor */
	public AbstractPcrdetails() {
	}

	/** full constructor */
	public AbstractPcrdetails(String gene, String size, String ampliconn,
			String amplicony, String user, String userdate, String datecreated,
			Timestamp datemodified, String comments) {
		this.gene = gene;
		this.size = size;
		this.ampliconn = ampliconn;
		this.amplicony = amplicony;
		this.user = user;
		this.userdate = userdate;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
		this.comments = comments;
	}

	// Property accessors

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getAmpliconn() {
		return this.ampliconn;
	}

	public void setAmpliconn(String ampliconn) {
		this.ampliconn = ampliconn;
	}

	public String getAmplicony() {
		return this.amplicony;
	}

	public void setAmplicony(String amplicony) {
		this.amplicony = amplicony;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}