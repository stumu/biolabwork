package org.suny.model;

import java.sql.Timestamp;

/**
 * Minia entity. @author MyEclipse Persistence Tools
 */
public class Minia extends AbstractMinia implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Minia() {
	}

	/** full constructor */
	public Minia(MiniaId id, String ml, String sequence, String comments,
			String user, String datecreated, Timestamp datemodified,
			String colony, String userdate) {
		super(id, ml, sequence, comments, user, datecreated, datemodified,
				colony, userdate);
	}

}
