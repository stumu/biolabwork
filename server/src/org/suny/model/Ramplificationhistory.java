package org.suny.model;

import java.sql.Timestamp;

/**
 * Ramplificationhistory entity. @author MyEclipse Persistence Tools
 */
public class Ramplificationhistory extends AbstractRamplificationhistory
		implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Ramplificationhistory() {
	}

	/** full constructor */
	public Ramplificationhistory(String gene, String ml, String volume,
			String excess, String store, String position, String comments,
			String datecreated, Timestamp datemodified, String user,
			String userdate) {
		super(gene, ml, volume, excess, store, position, comments, datecreated,
				datemodified, user, userdate);
	}

}
