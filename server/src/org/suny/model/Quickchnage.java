package org.suny.model;

import java.sql.Timestamp;

/**
 * Quickchnage entity. @author MyEclipse Persistence Tools
 */
public class Quickchnage extends AbstractQuickchnage implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Quickchnage() {
	}

	/** full constructor */
	public Quickchnage(QuickchnageId id, String userdate, String user,
			String comments, String datecreated, Timestamp datemodified) {
		super(id, userdate, user, comments, datecreated, datemodified);
	}

}
