package org.suny.model;

/**
 * AbstractPrimersequenceId entity provides the base persistence definition of
 * the PrimersequenceId entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractPrimersequenceId implements java.io.Serializable {

	// Fields

	private String gene;
	private String primer;

	// Constructors

	/** default constructor */
	public AbstractPrimersequenceId() {
	}

	/** full constructor */
	public AbstractPrimersequenceId(String gene, String primer) {
		this.gene = gene;
		this.primer = primer;
	}

	// Property accessors

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public String getPrimer() {
		return this.primer;
	}

	public void setPrimer(String primer) {
		this.primer = primer;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AbstractPrimersequenceId))
			return false;
		AbstractPrimersequenceId castOther = (AbstractPrimersequenceId) other;

		return ((this.getGene() == castOther.getGene()) || (this.getGene() != null
				&& castOther.getGene() != null && this.getGene().equals(
				castOther.getGene())))
				&& ((this.getPrimer() == castOther.getPrimer()) || (this
						.getPrimer() != null
						&& castOther.getPrimer() != null && this.getPrimer()
						.equals(castOther.getPrimer())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGene() == null ? 0 : this.getGene().hashCode());
		result = 37 * result
				+ (getPrimer() == null ? 0 : this.getPrimer().hashCode());
		return result;
	}

}