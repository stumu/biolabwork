package org.suny.model;

/**
 * Basicgenedetails entity. @author MyEclipse Persistence Tools
 */
public class Basicgenedetails extends AbstractBasicgenedetails implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Basicgenedetails() {
	}

	/** full constructor */
	public Basicgenedetails(String name, String strand, String txstart,
			String txend, String nterm, String cterm, String comments) {
		super(name, strand, txstart, txend, nterm, cterm, comments);
	}

}
