package org.suny.model;

import java.sql.Timestamp;

/**
 * Amplificationhistory entity. @author MyEclipse Persistence Tools
 */
public class Amplificationhistory extends AbstractAmplificationhistory
		implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Amplificationhistory() {
	}

	/** full constructor */
	public Amplificationhistory(String gene, String ml, String volume,
			String excess, String store, String position, String comments,
			String datecreated, Timestamp datemodified, String user,
			String userdate) {
		super(gene, ml, volume, excess, store, position, comments, datecreated,
				datemodified, user, userdate);
	}

}
