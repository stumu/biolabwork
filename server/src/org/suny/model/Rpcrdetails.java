package org.suny.model;

import java.sql.Timestamp;

/**
 * Rpcrdetails entity. @author MyEclipse Persistence Tools
 */
public class Rpcrdetails extends AbstractRpcrdetails implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Rpcrdetails() {
	}

	/** full constructor */
	public Rpcrdetails(String gene, String size, String ampliconn,
			String amplicony, String user, String userdate, String datecreated,
			Timestamp datemodified, String comments) {
		super(gene, size, ampliconn, amplicony, user, userdate, datecreated,
				datemodified, comments);
	}

}
