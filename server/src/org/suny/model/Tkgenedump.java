package org.suny.model;

/**
 * Tkgenedump entity. @author MyEclipse Persistence Tools
 */
public class Tkgenedump extends AbstractTkgenedump implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public Tkgenedump() {
	}

	/** minimal constructor */
	public Tkgenedump(String name) {
		super(name);
	}

	/** full constructor */
	public Tkgenedump(String name, String binno, String chrom, String strand,
			String txstart, String txend, String cdsstart, String cdsend,
			String exoncount, String exonstarts, String exonends, String score,
			String name2, String cdsstartstat, String cdsendstart,
			String exonframes) {
		super(name, binno, chrom, strand, txstart, txend, cdsstart, cdsend,
				exoncount, exonstarts, exonends, score, name2, cdsstartstat,
				cdsendstart, exonframes);
	}

}
