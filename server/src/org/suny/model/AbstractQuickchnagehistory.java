package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractQuickchnagehistory entity provides the base persistence definition of
 * the Quickchnagehistory entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractQuickchnagehistory implements
		java.io.Serializable {

	// Fields

	private Integer id;
	private String gene;
	private Integer oid;
	private String userdate;
	private String user;
	private String comments;
	private String datecreated;
	private Timestamp datemodified;

	// Constructors

	/** default constructor */
	public AbstractQuickchnagehistory() {
	}

	/** full constructor */
	public AbstractQuickchnagehistory(String gene, Integer oid,
			String userdate, String user, String comments, String datecreated,
			Timestamp datemodified) {
		this.gene = gene;
		this.oid = oid;
		this.userdate = userdate;
		this.user = user;
		this.comments = comments;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public Integer getOid() {
		return this.oid;
	}

	public void setOid(Integer oid) {
		this.oid = oid;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

}