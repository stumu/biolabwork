package org.suny.model;

import java.sql.Timestamp;

/**
 * Bts559 entity. @author MyEclipse Persistence Tools
 */
public class Bts559 extends AbstractBts559 implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Bts559() {
	}

	/** full constructor */
	public Bts559(Bts559Id id, String comments, String user,
			String datecreated, Timestamp datemodified, String userdate) {
		super(id, comments, user, datecreated, datemodified, userdate);
	}

}
