package org.suny.model;

/**
 * LabsInfo entity. @author MyEclipse Persistence Tools
 */
public class LabsInfo extends AbstractLabsInfo implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public LabsInfo() {
	}

	/** full constructor */
	public LabsInfo(String labName, String labProfessor, Long dateCreated,
			Long dateModified) {
		super(labName, labProfessor, dateCreated, dateModified);
	}

}
