package org.suny.model;

/**
 * AbstractWaitinglist entity provides the base persistence definition of the
 * Waitinglist entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractWaitinglist implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer labId;
	private String labName;
	private String personName;
	private Long dateCreated;
	private Long dateModified;
	private Long releaseDate;
	private Integer isactive;
	private String email;
	private String phone;

	// Constructors

	/** default constructor */
	public AbstractWaitinglist() {
	}

	/** full constructor */
	public AbstractWaitinglist(Integer labId, String labName,
			String personName, Long dateCreated, Long dateModified,
			Long releaseDate, Integer isactive, String email, String phone) {
		this.labId = labId;
		this.labName = labName;
		this.personName = personName;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.releaseDate = releaseDate;
		this.isactive = isactive;
		this.email = email;
		this.phone = phone;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLabId() {
		return this.labId;
	}

	public void setLabId(Integer labId) {
		this.labId = labId;
	}

	public String getLabName() {
		return this.labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Long dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Long getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Long dateModified) {
		this.dateModified = dateModified;
	}

	public Long getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getIsactive() {
		return this.isactive;
	}

	public void setIsactive(Integer isactive) {
		this.isactive = isactive;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}