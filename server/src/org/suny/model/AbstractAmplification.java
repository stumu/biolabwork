package org.suny.model;

import java.sql.Timestamp;

/**
 * AbstractAmplification entity provides the base persistence definition of the
 * Amplification entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractAmplification implements java.io.Serializable {

	// Fields

	private String gene;
	private String ml;
	private String volume;
	private String excess;
	private String store;
	private String position;
	private String comments;
	private String id;
	private String datecreated;
	private Timestamp datemodified;
	private String user;
	private String userdate;

	// Constructors

	/** default constructor */
	public AbstractAmplification() {
	}

	/** full constructor */
	public AbstractAmplification(String gene, String ml, String volume,
			String excess, String store, String position, String comments,
			String id, String datecreated, Timestamp datemodified, String user,
			String userdate) {
		this.gene = gene;
		this.ml = ml;
		this.volume = volume;
		this.excess = excess;
		this.store = store;
		this.position = position;
		this.comments = comments;
		this.id = id;
		this.datecreated = datecreated;
		this.datemodified = datemodified;
		this.user = user;
		this.userdate = userdate;
	}

	// Property accessors

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public String getMl() {
		return this.ml;
	}

	public void setMl(String ml) {
		this.ml = ml;
	}

	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getExcess() {
		return this.excess;
	}

	public void setExcess(String excess) {
		this.excess = excess;
	}

	public String getStore() {
		return this.store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDatecreated() {
		return this.datecreated;
	}

	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}

	public Timestamp getDatemodified() {
		return this.datemodified;
	}

	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserdate() {
		return this.userdate;
	}

	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}

}