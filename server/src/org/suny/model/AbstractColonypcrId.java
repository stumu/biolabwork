package org.suny.model;

/**
 * AbstractColonypcrId entity provides the base persistence definition of the
 * ColonypcrId entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractColonypcrId implements java.io.Serializable {

	// Fields

	private String gene;
	private Integer id;

	// Constructors

	/** default constructor */
	public AbstractColonypcrId() {
	}

	/** full constructor */
	public AbstractColonypcrId(String gene, Integer id) {
		this.gene = gene;
		this.id = id;
	}

	// Property accessors

	public String getGene() {
		return this.gene;
	}

	public void setGene(String gene) {
		this.gene = gene;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof AbstractColonypcrId))
			return false;
		AbstractColonypcrId castOther = (AbstractColonypcrId) other;

		return ((this.getGene() == castOther.getGene()) || (this.getGene() != null
				&& castOther.getGene() != null && this.getGene().equals(
				castOther.getGene())))
				&& ((this.getId() == castOther.getId()) || (this.getId() != null
						&& castOther.getId() != null && this.getId().equals(
						castOther.getId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getGene() == null ? 0 : this.getGene().hashCode());
		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		return result;
	}

}