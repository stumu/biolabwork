package org.suny.model;

/**
 * AbstractTkgenedump entity provides the base persistence definition of the
 * Tkgenedump entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractTkgenedump implements java.io.Serializable {

	// Fields

	private String name;
	private String binno;
	private String chrom;
	private String strand;
	private String txstart;
	private String txend;
	private String cdsstart;
	private String cdsend;
	private String exoncount;
	private String exonstarts;
	private String exonends;
	private String score;
	private String name2;
	private String cdsstartstat;
	private String cdsendstart;
	private String exonframes;

	// Constructors

	/** default constructor */
	public AbstractTkgenedump() {
	}

	/** minimal constructor */
	public AbstractTkgenedump(String name) {
		this.name = name;
	}

	/** full constructor */
	public AbstractTkgenedump(String name, String binno, String chrom,
			String strand, String txstart, String txend, String cdsstart,
			String cdsend, String exoncount, String exonstarts,
			String exonends, String score, String name2, String cdsstartstat,
			String cdsendstart, String exonframes) {
		this.name = name;
		this.binno = binno;
		this.chrom = chrom;
		this.strand = strand;
		this.txstart = txstart;
		this.txend = txend;
		this.cdsstart = cdsstart;
		this.cdsend = cdsend;
		this.exoncount = exoncount;
		this.exonstarts = exonstarts;
		this.exonends = exonends;
		this.score = score;
		this.name2 = name2;
		this.cdsstartstat = cdsstartstat;
		this.cdsendstart = cdsendstart;
		this.exonframes = exonframes;
	}

	// Property accessors

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBinno() {
		return this.binno;
	}

	public void setBinno(String binno) {
		this.binno = binno;
	}

	public String getChrom() {
		return this.chrom;
	}

	public void setChrom(String chrom) {
		this.chrom = chrom;
	}

	public String getStrand() {
		return this.strand;
	}

	public void setStrand(String strand) {
		this.strand = strand;
	}

	public String getTxstart() {
		return this.txstart;
	}

	public void setTxstart(String txstart) {
		this.txstart = txstart;
	}

	public String getTxend() {
		return this.txend;
	}

	public void setTxend(String txend) {
		this.txend = txend;
	}

	public String getCdsstart() {
		return this.cdsstart;
	}

	public void setCdsstart(String cdsstart) {
		this.cdsstart = cdsstart;
	}

	public String getCdsend() {
		return this.cdsend;
	}

	public void setCdsend(String cdsend) {
		this.cdsend = cdsend;
	}

	public String getExoncount() {
		return this.exoncount;
	}

	public void setExoncount(String exoncount) {
		this.exoncount = exoncount;
	}

	public String getExonstarts() {
		return this.exonstarts;
	}

	public void setExonstarts(String exonstarts) {
		this.exonstarts = exonstarts;
	}

	public String getExonends() {
		return this.exonends;
	}

	public void setExonends(String exonends) {
		this.exonends = exonends;
	}

	public String getScore() {
		return this.score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getName2() {
		return this.name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getCdsstartstat() {
		return this.cdsstartstat;
	}

	public void setCdsstartstat(String cdsstartstat) {
		this.cdsstartstat = cdsstartstat;
	}

	public String getCdsendstart() {
		return this.cdsendstart;
	}

	public void setCdsendstart(String cdsendstart) {
		this.cdsendstart = cdsendstart;
	}

	public String getExonframes() {
		return this.exonframes;
	}

	public void setExonframes(String exonframes) {
		this.exonframes = exonframes;
	}

}