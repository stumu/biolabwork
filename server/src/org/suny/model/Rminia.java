package org.suny.model;

import java.sql.Timestamp;

/**
 * Rminia entity. @author MyEclipse Persistence Tools
 */
public class Rminia extends AbstractRminia implements java.io.Serializable {

	// Constructors

	/** default constructor */
	public Rminia() {
	}

	/** full constructor */
	public Rminia(RminiaId id, String ml, String sequence, String comments,
			String user, String datecreated, Timestamp datemodified,
			String colony, String userdate) {
		super(id, ml, sequence, comments, user, datecreated, datemodified,
				colony, userdate);
	}

}
