package org.suny.model;

/**
 * AbstractBorrowerDetails entity provides the base persistence definition of
 * the BorrowerDetails entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractBorrowerDetails implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer machineId;
	private String machineName;
	private Integer labId;
	private String personName;
	private Long dateCreated;
	private String labName;
	private Long dateModified;
	private Long releaseDate;
	private Integer usageflag;
	private String email;
	private String phone;

	// Constructors

	/** default constructor */
	public AbstractBorrowerDetails() {
	}

	/** full constructor */
	public AbstractBorrowerDetails(Integer machineId, String machineName,
			Integer labId, String personName, Long dateCreated, String labName,
			Long dateModified, Long releaseDate, Integer usageflag,
			String email, String phone) {
		this.machineId = machineId;
		this.machineName = machineName;
		this.labId = labId;
		this.personName = personName;
		this.dateCreated = dateCreated;
		this.labName = labName;
		this.dateModified = dateModified;
		this.releaseDate = releaseDate;
		this.usageflag = usageflag;
		this.email = email;
		this.phone = phone;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMachineId() {
		return this.machineId;
	}

	public void setMachineId(Integer machineId) {
		this.machineId = machineId;
	}

	public String getMachineName() {
		return this.machineName;
	}

	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}

	public Integer getLabId() {
		return this.labId;
	}

	public void setLabId(Integer labId) {
		this.labId = labId;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Long getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Long dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLabName() {
		return this.labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public Long getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Long dateModified) {
		this.dateModified = dateModified;
	}

	public Long getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Integer getUsageflag() {
		return this.usageflag;
	}

	public void setUsageflag(Integer usageflag) {
		this.usageflag = usageflag;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}