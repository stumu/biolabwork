package org.suny.model;

/**
 * ColonypcrId entity. @author MyEclipse Persistence Tools
 */
public class ColonypcrId extends AbstractColonypcrId implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public ColonypcrId() {
	}

	/** full constructor */
	public ColonypcrId(String gene, Integer id) {
		super(gene, id);
	}

}
