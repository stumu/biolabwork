package org.suny.model;

/**
 * BorrowerDetails entity. @author MyEclipse Persistence Tools
 */
public class BorrowerDetails extends AbstractBorrowerDetails implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public BorrowerDetails() {
	}

	/** full constructor */
	public BorrowerDetails(Integer machineId, String machineName,
			Integer labId, String personName, Long dateCreated, String labName,
			Long dateModified, Long releaseDate,Integer usageflag,String email, String phone) {
		super(machineId, machineName, labId, personName, dateCreated, labName,
				dateModified, releaseDate,usageflag,email,phone);
	}

}
