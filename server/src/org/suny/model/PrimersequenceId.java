package org.suny.model;

/**
 * PrimersequenceId entity. @author MyEclipse Persistence Tools
 */
public class PrimersequenceId extends AbstractPrimersequenceId implements
		java.io.Serializable {

	// Constructors

	/** default constructor */
	public PrimersequenceId() {
	}

	/** full constructor */
	public PrimersequenceId(String gene, String primer) {
		super(gene, primer);
	}

}
