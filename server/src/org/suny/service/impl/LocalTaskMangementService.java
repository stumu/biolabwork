package org.suny.service.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.suny.beans.AmplificationDetailsBean;
import org.suny.beans.BTS559DetailsBean;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.CTS559DetailsBean;
import org.suny.beans.ColonyPCRDetailsBean;
import org.suny.beans.GeneBean;
import org.suny.beans.LabsInfoBean;
import org.suny.beans.MachineInfoBean;
import org.suny.beans.MiniADetailsBean;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.beans.PrimerSequenceDataBean;
import org.suny.beans.QuickChangeDetailsBean;
import org.suny.beans.SearchGeneBean;
import org.suny.beans.ValueList;
import org.suny.beans.WaitingListEntryBean;
import org.suny.dao.PersistenceException;
import org.suny.dao.hibernate.HibernateSession;
import org.suny.dao.hibernate.HibernateSessionFactory;
import org.suny.dao.hibernate.MachineDao;
import org.suny.dao.hibernate.UserDao;
import org.suny.dao.hibernate.WaitingListDao;
import org.suny.model.Amplification;
import org.suny.model.Amplificationhistory;
import org.suny.model.Basicgenedetails;
import org.suny.model.BorrowerDetails;
import org.suny.model.Bts559;
import org.suny.model.Bts559Id;
import org.suny.model.Colonypcr;
import org.suny.model.ColonypcrId;
import org.suny.model.Emailsettings;
import org.suny.model.LabsInfo;
import org.suny.model.MachineInfo;
import org.suny.model.Minia;
import org.suny.model.MiniaId;
import org.suny.model.Pcrdetails;
import org.suny.model.Pcrdetailshistory;
import org.suny.model.Primersequence;
import org.suny.model.PrimersequenceId;
import org.suny.model.Quickchnage;
import org.suny.model.QuickchnageId;
import org.suny.model.Ramplification;
import org.suny.model.Rpcrdetails;
import org.suny.model.Rpcrdetailshistory;
import org.suny.model.Tkgenedump;
import org.suny.model.Waitinglist;
import org.suny.service.TaskMangementService;
import org.suny.service.util.SimpleMail;

import com.sun.security.auth.UserPrincipal;





public class LocalTaskMangementService implements TaskMangementService {
	
	private UserPrincipal principal = null;
	
	public LocalTaskMangementService(UserPrincipal principal) {
		this.principal = principal;
	}
	public LocalTaskMangementService() {
		
	}
	
	
	public List getUserByLogin(String login,String password) throws PersistenceException {
		HibernateSession session = null;
		List list = new ArrayList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			list = UserDao.dao.getUserByLogin(login,password);
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			throw e;
		} catch (Exception e) {
			session.rollback();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public ValueList getMachineUnderUse() throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getMachineUnderUse();
			BusyMachinesBean m = null;
			BorrowerDetails b = null;
			if(l != null){
				for(int i=0;i<l.size();i++){
					m= new BusyMachinesBean();
					b = (BorrowerDetails)l.get(i); 
					m.setId(b.getId());
					m.setBorrowdDate(getDateByTimestamp(Long.parseLong(String.valueOf(b.getDateCreated()))));
					m.setLabName(b.getLabName());
					m.setMachineId(Integer.parseInt(String.valueOf(b.getMachineId())));
					m.setMachineName(b.getMachineName());
					m.setPersonName(b.getPersonName());
					m.setReleaseDate(getDateByTimestamp(Long.parseLong(String.valueOf(b.getReleaseDate()))));
					m.setEmail(b.getEmail());
					m.setPhone(b.getPhone());
					list.add(m);
				}
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public ValueList getPrimarySequenceList(String geneName) throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getPrimarySequence(geneName.substring(2));
			PrimerSequenceDataBean p = null;
			String val=null;
			for(int i=1;i<=29;i++)
			{
				p = new PrimerSequenceDataBean();
				p.setEnd("");
				p.setPlate("");
				val = "00"+i;
				if(i>9)
					val = "0"+i;
				p.setPrimer(val);
				p.setStart("");
				p.setWell_Position("");
				p.setPrimerSequence("");
				list.add(p);
			}
			Primersequence mp = null;
			if(l != null){
				for(int i=0;i<l.size();i++){
					mp = (Primersequence)l.get(i); 
					PrimerSequenceDataBean p1 = (PrimerSequenceDataBean)list.get(Integer.parseInt(mp.getId().getPrimer()));
					p1.setEnd(mp.getLcend());
					p1.setPlate(mp.getPlate());
					p1.setPrimerSequence(mp.getSequence());
					p1.setStart(mp.getLcstart());
					p1.setWell_Position(mp.getWell());
				}
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	public PrimaryGeneDetailsBean getSelectedGeneDetails(String geneName) throws PersistenceException 
	{
		HibernateSession session = null;
		PrimaryGeneDetailsBean p = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getGeneDetails(geneName);
			Tkgenedump t= null;
			GeneBean g  = null;
			if(l != null){
				for(int i=0;i<l.size();i++){
					t = (Tkgenedump)l.get(i);
					p = new PrimaryGeneDetailsBean();
					p.setComments("None");
					p.setCtermY("false");
					p.setCtermN("true");
					p.setNtermY("true");
					p.setNtermN("false");
					p.setGeneLength(Integer.parseInt(t.getTxend()) - Integer.parseInt(t.getTxstart()) );
					p.setGeneLocationStart(t.getTxstart());
					p.setGeneLocationEnd(t.getTxend());
					if(t.getStrand().equals("+"))
					{
						p.setPositiveStrand("true");
						p.setNegativeStrand("false");
					}
					else
					{
						p.setPositiveStrand("false");
						p.setNegativeStrand("true");
					}
				}
			}
			l = MachineDao.dao.getBasicGeneDetails(geneName);
			if(l != null){
				Basicgenedetails b = (Basicgenedetails)l.get(0);
				p.setComments(b.getComments());
				p.setGeneLocationEnd(b.getTxend());
				p.setGeneLocationStart(b.getTxstart());
				if(b.getStrand().equals("+"))
				{
					p.setPositiveStrand("true");
					p.setNegativeStrand("false");
				}
				else
				{
					p.setPositiveStrand("false");
					p.setNegativeStrand("true");
				}
				if(b.getCterm().equals("YES"))
				{
					p.setCtermY("true");
					p.setCtermN("false");
				}
				else
				{
					p.setCtermY("false");
					p.setCtermN("true");
				}
				if(b.getNterm().equals("YES"))
				{
					p.setNtermY("true");
					p.setNtermN("false");
				}
				else
				{
					p.setNtermY("false");
					p.setNtermN("true");
				}
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public PrimaryGeneDetailsBean saveBasicGeneDetails(String geneName,PrimaryGeneDetailsBean p) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Basicgenedetails b = new Basicgenedetails();
			b.setComments(p.getComments());
			if(p.getCtermN().equals("true"))
				b.setCterm("NO");
			else
				b.setCterm("YES");
			if(p.getNtermN().equals("true"))
				b.setNterm("NO");
			else
				b.setNterm("YES");
			if(p.getNegativeStrand().equals("true"))
				b.setStrand("-");
			else
				b.setStrand("+");
			b.setTxend(p.getGeneLocationEnd());
			b.setTxstart(p.getGeneLocationStart());
			b.setName(geneName);
			
			MachineDao.dao.saveBasicGeneDetails(b);
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return getSelectedGeneDetails(geneName);
	}
	public String getPresentDateTime()
	{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		   //get current date time with Date()
		   Date date = new Date();
		   //get current date time with Calendar()
		   Calendar cal = Calendar.getInstance();
		   return dateFormat.format(cal.getTime());
	}
	public PCR2PDetailsBean savePCR2PDetails(String geneName,PCR2PDetailsBean p ) throws PersistenceException  
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Pcrdetails b = new Pcrdetails();
			b.setGene(geneName);
			b.setSize(p.getSize());
			b.setUserdate(p.getPcrdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(p.getComments());
			if(p.getAmpliconn().equals("true"))
			{
				b.setAmpliconn("true");
				b.setAmplicony("false");
			}
			else
			{
				b.setAmpliconn("false");
				b.setAmplicony("true");
			}
			b.setUser("crc");
			MachineDao.dao.savePCR2PDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getPCR2PDetails(geneName);
	}
	public MiniADetailsBean saveMiniADetails(String geneName,MiniADetailsBean getForm ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Minia b = new Minia();
			MiniaId id = new MiniaId();
			id.setGene(geneName);
			id.setId(Integer.parseInt(getForm.getId()));
			b.setColony(getForm.getColony());
			b.setUserdate(getForm.getUserdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(getForm.getComments());
			b.setMl(getForm.getMl());
			b.setId(id);
			if(getForm.getSequence().equals("true"))
			{
				b.setSequence("true");
			}
			else
			{
				b.setSequence("false");
			}
			b.setUser("crc");
			MachineDao.dao.saveMiniADetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getMiniADetails(geneName,getForm.getId());
	}
	public BTS559DetailsBean saveBTS559Details(String geneName,BTS559DetailsBean getForm ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Bts559 b = new Bts559();
			Bts559Id id = new Bts559Id();
			id.setGene(geneName);
			id.setId(Integer.parseInt(getForm.getBasicVal()));
			b.setUserdate(getForm.getUserdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(getForm.getComments());
			b.setId(id);
			b.setUser("crc");
			MachineDao.dao.saveBTS559Details(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getBTS559Details(geneName,getForm.getBasicVal());
	}
	public CTS559DetailsBean saveBTS559Details(String geneName,CTS559DetailsBean getForm ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Bts559 b = new Bts559();
			Bts559Id id = new Bts559Id();
			id.setGene(geneName);
			id.setId(Integer.parseInt(getForm.getBasicVal()));
			b.setUserdate(getForm.getUserdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(getForm.getComments());
			b.setId(id);
			b.setUser("crc");
			MachineDao.dao.saveBTS559Details(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getBTS559Details(geneName,getForm.getBasicVal(),null);
	}
	public ColonyPCRDetailsBean saveColonyPCRCDetails(String geneName,ColonyPCRDetailsBean g ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Colonypcr b = new Colonypcr();
			ColonypcrId id = new ColonypcrId();
			id.setGene(geneName);
			id.setId(Integer.parseInt(g.getId()));
			b.setDate1(g.getDate1());
			b.setDate2(g.getDate2());
			b.setDate3(g.getDate3());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setSeq(g.getSeq());
			b.setId(id);
			b.setPrimer1(g.getPrimer1());
			b.setPrimer2(g.getPrimer2());
			b.setSize(g.getSize());
			b.setComments("crc"); // using for User
			MachineDao.dao.saveColonyPCRCDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getColonyPCRCDetails(geneName,g.getId());
	}
	public ColonyPCRDetailsBean getColonyPCRCDetails(String geneName,String oid) throws PersistenceException 
	{
		HibernateSession session = null;
		ColonyPCRDetailsBean b = new ColonyPCRDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.geColonyPCRCDetails(geneName,oid);
			Colonypcr t= null;
			if(l != null){
				t = (Colonypcr)l.get(0);
				b.setDate1(t.getDate1());
				b.setDate2(t.getDate2());
				b.setDate3(t.getDate3());
				b.setDatecreated(getPresentDateTime());
				b.setDatemodified(new Timestamp(System.currentTimeMillis()));
				b.setSeq(t.getSeq());
				b.setSequence(t.getSeq().split("[$]"));
				b.setPrimer1(t.getPrimer1());
				b.setPrimer2(t.getPrimer2());
				b.setSize(t.getSize());
				b.setComments("crc"); 
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return b;
	}
	public MiniADetailsBean getMiniADetails(String geneName,String oid) throws PersistenceException 
	{
		HibernateSession session = null;
		MiniADetailsBean p = new MiniADetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getMiniADetails(geneName,oid);
			Minia t= null;
			if(l != null){
				t = (Minia)l.get(0);
				p = new MiniADetailsBean();
				p.setUserdate(t.getUserdate());
				p.setSequence(t.getSequence());
				p.setComments(t.getComments());
				p.setMl(t.getMl());
				p.setColony(t.getColony());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public CTS559DetailsBean getBTS559Details(String geneName,String oid,String temp) throws PersistenceException 
	{
		HibernateSession session = null;
		CTS559DetailsBean p = new CTS559DetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getBTS559Details(geneName,oid);
			Bts559 t= null;
			if(l != null){
				t = (Bts559)l.get(0);
				p = new CTS559DetailsBean();
				p.setUserdate(t.getUserdate());
				p.setComments(t.getComments());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public BTS559DetailsBean getBTS559Details(String geneName,String oid) throws PersistenceException 
	{
		HibernateSession session = null;
		BTS559DetailsBean p = new BTS559DetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getBTS559Details(geneName,oid);
			Bts559 t= null;
			if(l != null){
				t = (Bts559)l.get(0);
				p = new BTS559DetailsBean();
				p.setUserdate(t.getUserdate());
				p.setComments(t.getComments());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public QuickChangeDetailsBean saveQchangeBDetails(String geneName,QuickChangeDetailsBean getForm ) throws PersistenceException
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Quickchnage b = new Quickchnage();
			QuickchnageId id = new QuickchnageId();
			id.setGene(geneName);
			id.setId(Integer.parseInt(getForm.getId()));
			b.setUserdate(getForm.getUserdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(getForm.getComments());
			b.setId(id);
			b.setUser("crc");
			MachineDao.dao.saveQchangeBDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getQchangeBDetails(geneName,getForm.getId());
	}
	public QuickChangeDetailsBean getQchangeBDetails(String geneName,String oid) throws PersistenceException 
	{
		HibernateSession session = null;
		QuickChangeDetailsBean p = new QuickChangeDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getQChangeBDetails(geneName,oid);
			Quickchnage t= null;
			if(l != null){
				t = (Quickchnage)l.get(0);
				p = new QuickChangeDetailsBean();
				p.setUserdate(t.getUserdate());
				p.setComments(t.getComments());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public AmplificationDetailsBean saveAmplificationDetails(String geneName,AmplificationDetailsBean p ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Amplification b = new Amplification();
			b.setGene(geneName);
			b.setComments(p.getComments());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setExcess(p.getExcess());
			b.setId("3");
			b.setMl(p.getMl());
			b.setPosition(p.getPosition());
			b.setStore(p.getStore());
			b.setUserdate(p.getUserdate());
			b.setVolume(p.getVolume());
			b.setUser("crc");
			MachineDao.dao.saveAmplificationDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getAmplificationDetails(geneName);
	}
	public AmplificationDetailsBean saveRAmplificationDetails(String geneName,AmplificationDetailsBean p ) throws PersistenceException 
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Ramplification b = new Ramplification();
			b.setGene(geneName);
			b.setComments(p.getComments());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setExcess(p.getExcess());
			b.setId("3");
			b.setMl(p.getMl());
			b.setPosition(p.getPosition());
			b.setStore(p.getStore());
			b.setUserdate(p.getUserdate());
			b.setVolume(p.getVolume());
			b.setUser("crc");
			MachineDao.dao.saveRAmplificationDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getRAmplificationDetails(geneName);
	}
	public AmplificationDetailsBean getRAmplificationDetails(String geneName) throws PersistenceException 
	{
		HibernateSession session = null;
		AmplificationDetailsBean p = new AmplificationDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getRAmplificationDetails(geneName);
			Ramplification t= null;
			if(l != null){
				t = (Ramplification)l.get(0);
				p.setUser(t.getUser());
				p.setComments(t.getComments());
				p.setExcess(t.getExcess());
				p.setGene(t.getGene());
				p.setMl(t.getMl());
				p.setPosition(t.getPosition());
				p.setStore(t.getStore());
				p.setUserdate(t.getUserdate());
				p.setVolume(t.getVolume());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public AmplificationDetailsBean getAmplificationDetailsHistory(String geneName,String id) throws PersistenceException 
	{
		HibernateSession session = null;
		AmplificationDetailsBean p = new AmplificationDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getAmplificationDetailsHistory(geneName,id);
			Amplificationhistory t= null;
			if(l != null){
				t = (Amplificationhistory)l.get(0);
				p.setUser(t.getUser());
				p.setComments(t.getComments());
				p.setExcess(t.getExcess());
				p.setGene(t.getGene());
				p.setMl(t.getMl());
				p.setPosition(t.getPosition());
				p.setStore(t.getStore());
				p.setUserdate(t.getUserdate());
				p.setVolume(t.getVolume());
				p.setId(t.getId()+"");
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public AmplificationDetailsBean getAmplificationDetails(String geneName) throws PersistenceException 
	{
		HibernateSession session = null;
		AmplificationDetailsBean p = new AmplificationDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getAmplificationDetails(geneName);
			Amplification t= null;
			if(l != null){
				t = (Amplification)l.get(0);
				p.setUser(t.getUser());
				p.setComments(t.getComments());
				p.setExcess(t.getExcess());
				p.setGene(t.getGene());
				p.setMl(t.getMl());
				p.setPosition(t.getPosition());
				p.setStore(t.getStore());
				p.setUserdate(t.getUserdate());
				p.setVolume(t.getVolume());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public PCR2PDetailsBean saveRPCR2PDetails(String geneName,PCR2PDetailsBean p ) throws PersistenceException  
	{
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			Rpcrdetails b = new Rpcrdetails();
			b.setGene(geneName);
			b.setSize("0");
			b.setUserdate(p.getPcrdate());
			b.setDatecreated(getPresentDateTime());
			b.setDatemodified(new Timestamp(System.currentTimeMillis()));
			b.setComments(p.getComments());
			if(p.getAmpliconn().equals("true"))
			{
				b.setAmpliconn("true");
				b.setAmplicony("false");
			}
			else
			{
				b.setAmpliconn("false");
				b.setAmplicony("true");
			}
			b.setUser("crc");
			MachineDao.dao.saveRPCR2PDetails(b);
			session.commit();
			b=null;
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return getRPCR2PDetails(geneName);
	}
	public PCR2PDetailsBean getPCR2PDetails(String geneName) throws PersistenceException 
	{
		HibernateSession session = null;
		PCR2PDetailsBean p = new PCR2PDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getPCR2PDetails(geneName);
			Pcrdetails t= null;
			if(l != null){
				t = (Pcrdetails)l.get(0);
				p = new PCR2PDetailsBean();
				p.setAmpliconn(t.getAmpliconn());
				p.setAmplicony(t.getAmplicony());
				p.setBasicGeneName(t.getGene());
				p.setComments(t.getComments());
				p.setPcrdate(t.getUserdate());
				p.setSize(t.getSize());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	
	public PCR2PDetailsBean getRPCR2PDetails(String geneName) throws PersistenceException 
	{
		HibernateSession session = null;
		PCR2PDetailsBean p = new PCR2PDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getRPCR2PDetails(geneName);
			Rpcrdetails t= null;
			if(l != null){
				t = (Rpcrdetails)l.get(0);
				p = new PCR2PDetailsBean();
				p.setAmpliconn(t.getAmpliconn());
				p.setAmplicony(t.getAmplicony());
				p.setBasicGeneName(t.getGene());
				p.setComments(t.getComments());
				p.setPcrdate(t.getUserdate());
				p.setSize(t.getSize());
				p.setUser(t.getUser());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public PCR2PDetailsBean getRPCR2PDetailsHistory(String geneName,String id) throws PersistenceException 
	{
		HibernateSession session = null;
		PCR2PDetailsBean p = new PCR2PDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getRPCR2PDetailsHistory(geneName,id);
			Rpcrdetailshistory t= null;
			if(l != null){
				t = (Rpcrdetailshistory)l.get(0);
				p = new PCR2PDetailsBean();
				p.setAmpliconn(t.getAmpliconn());
				p.setAmplicony(t.getAmplicony());
				p.setBasicGeneName(t.getGene());
				p.setComments(t.getComments());
				p.setPcrdate(t.getUserdate());
				p.setSize(t.getSize());
				p.setUser(t.getUser());
				p.setId(t.getId());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public PCR2PDetailsBean getPCR2PDetailsHistory(String geneName,String id) throws PersistenceException 
	{
		HibernateSession session = null;
		PCR2PDetailsBean p = new PCR2PDetailsBean();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getPCR2PDetailsHistory(geneName,id);
			Pcrdetailshistory t= null;
			if(l != null){
				t = (Pcrdetailshistory)l.get(0);
				p = new PCR2PDetailsBean();
				p.setAmpliconn(t.getAmpliconn());
				p.setAmplicony(t.getAmplicony());
				p.setBasicGeneName(t.getGene());
				p.setComments(t.getComments());
				p.setPcrdate(t.getUserdate());
				p.setSize(t.getSize());
				p.setUser(t.getUser());
				p.setId(t.getId());
			}
			t = null;
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return p;
	}
	public void uploadSequences(ArrayList beanList) throws PersistenceException  
	{
		HibernateSession session = null;
		
			if(beanList != null)
			{
				for(int i=0;i<beanList.size();i++)
				{
					try {
						PrimerSequenceDataBean p = (PrimerSequenceDataBean)beanList.get(i);
						if(p.getGene().length() == 4)
						{
							session = HibernateSessionFactory.openSession();
							session.beginTransaction();
							Primersequence mp = new Primersequence();
							PrimersequenceId pId = new PrimersequenceId(p.getGene(), p.getPrimer());
							mp.setId(pId);
							mp.setLcend(p.getEnd());
							mp.setLcstart(p.getStart());
							mp.setPlate(p.getPlate());
							mp.setWell(p.getWell_Position());
							mp.setSequence(p.getPrimerSequence());
							mp.setUser("crc");
							MachineDao.dao.savePrimerSequenceDetails(mp);
							session.commit();
							session.flush();
						}
					} catch (PersistenceException e) {
						session.rollback();
						e.printStackTrace();
						throw e;
					} catch (Exception e) {
						session.rollback();
						e.printStackTrace();
						throw new PersistenceException(e.getMessage(), e);
					} finally {
						session.close();
					}
				}
			}
			
	}
	public ValueList searchGene(SearchGeneBean b) throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.searchGeneList(b.getGeneName());
			Tkgenedump t= null;
			GeneBean g  = null;
			if(l != null){
				for(int i=0;i<l.size();i++){
					t = (Tkgenedump)l.get(i);
					g = new GeneBean();
					g.setName(t.getName());
					list.add(g);
				}
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public ValueList getWaitingList() throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = WaitingListDao.dao.getActiveWaitingList();
			WaitingListEntryBean m = null;
			Waitinglist b = null;
			if(l != null){
				for(int i=0;i<l.size();i++){
					m= new WaitingListEntryBean();
					b = (Waitinglist)l.get(i); 
					m.setId(b.getId());
					m.setBorrowdDate(getDateByTimestamp(Long.parseLong(String.valueOf(b.getDateCreated()))));
					m.setLabName(b.getLabName());
					m.setPersonName(b.getPersonName());
					m.setReleaseDate(getDateByTimestamp(Long.parseLong(String.valueOf(b.getReleaseDate()))));
					m.setEmail(b.getEmail());
					m.setPhone(b.getPhone());
					list.add(m);
				}
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public ValueList getLabsInfo() throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getLabs();
			
			LabsInfo b = null;
			LabsInfoBean m = null;
			for(int i=0;i<l.size();i++){
				
				b = (LabsInfo)l.get(i); 
				m=new LabsInfoBean();
				m.setLabName(b.getLabName());
				list.add(m);
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public ValueList getMachinesInfo() throws PersistenceException {
		HibernateSession session = null;
		ValueList list = new ValueList();
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getMachines();
			List useList = MachineDao.dao.getMachineUnderUse();
			List useListNames = new ArrayList();
			if(useList != null){
				for(int i=0;i<useList.size();i++){
					BorrowerDetails b = (BorrowerDetails)useList.get(i); 
					useListNames.add(b.getMachineName());
					//m1 = null;
				}
			}
			MachineInfo b = null;
			MachineInfoBean m = null;
			for(int i=0;i<l.size();i++){
				
				b = (MachineInfo)l.get(i); 
				m=new MachineInfoBean();
				m.setMachine(b.getMachine());
				m.setSerialNo(b.getSerialNo());
				if(!useListNames.contains(b.getMachine()))
					list.add(m);
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return list;
	}
	
	public void sendEMailForReserve(BusyMachinesBean getForm) throws PersistenceException {
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getEmailSettings();
			if( l!= null){
				Emailsettings e = (Emailsettings)l.get(0);
				String rec = e.getToaddr();
				String[] receipeints = null;
				if(rec.contains(";")){
					receipeints = rec.split(";");
				}
				else{
					receipeints = new String[] {rec};
				}
				String msg = " Laptop Reserve Notification: \n "+
								"Name : "+getForm.getPersonName() +"\n"+
								"Email: "+getForm.getEmail() +" \n "+
								"Phone: "+getForm.getPhone() +" \n" +
								"Lab name: "+getForm.getLabName() ;
				String subject= "Pathway Studio Laptop Reserve Notfication";
				SimpleMail m = new SimpleMail();//new SimpleMail(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,msg,receipeints);
				m.sendMessage(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,msg,receipeints);				
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
	}
	
	public void sendEMailForWaiting(WaitingListEntryBean getForm) throws PersistenceException {
		HibernateSession session = null;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getEmailSettings();
			if( l!= null){
				Emailsettings e = (Emailsettings)l.get(0);
				String rec = e.getToaddr();
				String[] receipeints = null;
				if(rec.contains(";")){
					receipeints = rec.split(";");
				}
				else{
					receipeints = new String[] {rec};
				}
				String msg = " Laptop Waiting List Notification: \n "+
								"Name : "+getForm.getPersonName() +"\n"+
								"Email: "+getForm.getEmail() +" \n "+
								"Phone: "+getForm.getPhone() +" \n" +
								"Lab name: "+getForm.getLabName() ;
				String subject= "Pathway Studio Laptop Waiting List Notfication";
				SimpleMail m = new SimpleMail();//new SimpleMail(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,msg,receipeints);
				m.sendMessage(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,msg,receipeints);
								
			}
			session.commit();
		} catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		
	}
	
	public int  saveAssignedMachine(BusyMachinesBean getForm) throws PersistenceException {
		// TODO Auto-generated method stub
		HibernateSession session = null;
		int response = 1;
		long time=System.currentTimeMillis();
		try{
			
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			System.out.println("Reserve Machine Transaction Started::::::::::::::");
			BorrowerDetails m = new BorrowerDetails();
			m.setDateCreated( getTimestampByDateString(getForm.getBorrowdDate()));
			m.setDateModified(time);
			String labName = getForm.getLabName();
			List l = MachineDao.dao.getLabIdByLabName(labName);
			if(l == null || l.size() <= 0 || l.size() > 1){
				return response;
			}
			LabsInfo labInfo = (LabsInfo)l.get(0);
			m.setLabId(labInfo.getId());
			m.setLabName(getForm.getLabName());
			String machineName = getForm.getMachineName();
			l = MachineDao.dao.getMachineIdByMachineName(machineName);
			if(l == null || l.size() <= 0 || l.size() > 1){
				return response;
			}
			MachineInfo machineInfo  = (MachineInfo)l.get(0);
			m.setMachineId(machineInfo.getId());
			m.setMachineName(getForm.getMachineName());
			m.setPersonName(getForm.getPersonName());
			m.setUsageflag(1);
			long l1 = getTimestampByDateString(getForm.getBorrowdDate());
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(l1);
			cal.add(Calendar.MONTH,2);
			m.setReleaseDate(cal.getTimeInMillis());//getTimestampByDateString(getForm.getReleaseDate()));
			m.setEmail(getForm.getEmail());
			m.setPhone(getForm.getPhone());
			// SAVE HIBERNATE SESSION BEAN
			MachineDao.dao.saveAssignMachineBean(m);
			session.commit();
			response=0;
		}
		catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
			
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		try{
			sendEMailForReserve(getForm);
		}
		catch(Exception e){
			
		}
		return response;
		
	}
	
	public int  saveWaitingListEntry(WaitingListEntryBean getForm) throws PersistenceException {
		// TODO Auto-generated method stub
		HibernateSession session = null;
		int response = 1;
		long time=System.currentTimeMillis();
		try{
			
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			System.out.println("Reserve waiting list Transaction Started::::::::::::::");
			Waitinglist m = new Waitinglist();
			m.setDateCreated( getTimestampByDateString(getForm.getBorrowdDate()));// time);
			m.setDateModified(time);
			String labName = getForm.getLabName();
			List l = MachineDao.dao.getLabIdByLabName(labName);
			if(l == null || l.size() <= 0 || l.size() > 1){
				return response;
			}
			LabsInfo labInfo = (LabsInfo)l.get(0);
			m.setLabId(labInfo.getId());
			m.setLabName(getForm.getLabName());
			
			m.setPersonName(getForm.getPersonName());
			m.setIsactive(1);
			long l1 = getTimestampByDateString(getForm.getBorrowdDate());
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(l1);
			cal.add(Calendar.MONTH,2);
			m.setReleaseDate(cal.getTimeInMillis());
			m.setEmail(getForm.getEmail());
			m.setPhone(getForm.getPhone());
			// SAVE HIBERNATE SESSION BEAN
			WaitingListDao.dao.saveWaitingListEntry(m);
			session.commit();
			response=0;
		}
		catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
			
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		try{
			String subject= "Pathway Studio Waiting List Notfication";
			sendEMailForWaiting(getForm);
		}
		catch(Exception e){
			
		}
		return response;
		
	}
	
	
	public int  deleteAssignedMachine(String[] deleteList) throws PersistenceException {
		// TODO Auto-generated method stub
		HibernateSession session = null;
		int response = 1;
		long time=System.currentTimeMillis();
		try{
			
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			System.out.println(" Delete Machine Transaction Started::::::::::::::");
			response = MachineDao.dao.deleteMachineByMachineId(deleteList);
			session.commit();
			//response=0;
		}
		catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
			
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return response;
		
	}
	
	
	public int  deleteWaitingListEntry(String[] deleteList) throws PersistenceException {
		// TODO Auto-generated method stub
		HibernateSession session = null;
		int response = 1;
		long time=System.currentTimeMillis();
		try{
			
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			System.out.println(" Delete waitinglist Transaction Started::::::::::::::");
			response = WaitingListDao.dao.deleteWaitingListEntryById(deleteList);
			session.commit();
			//response=0;
		}
		catch (PersistenceException e) {
			session.rollback();
			e.printStackTrace();
			throw e;
			
		} catch (Exception e) {
			session.rollback();
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return response;
		
	}
	
	public int  sendEmailNotifications(String message) throws PersistenceException {
		// TODO Auto-generated method stub
		HibernateSession session = null;
		int response = 1;
		try {
			session = HibernateSessionFactory.openSession();
			session.beginTransaction();
			List l = MachineDao.dao.getEmailSettings();
			if( l!= null){
				Emailsettings e = (Emailsettings)l.get(0);
				List l1 = MachineDao.dao.getMachineUnderUse();
				String[] receipeints = null;
				BorrowerDetails b = null;
				if(l1 != null){
					receipeints = new String[l1.size()];
					for(int i=0;i<l1.size();i++){
						b = (BorrowerDetails)l1.get(i); 
						receipeints[i] = b.getEmail();
					}
				}
				String subject= "Pathway Studio Laptop Notfication";
				SimpleMail m = new SimpleMail();//new SimpleMail(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,msg,receipeints);
				m.sendMessage(e.getSmtpserver(),e.getUsername(),e.getPassword(),e.getAlias(), subject,message,receipeints);
								
			
				
			}
			session.commit();
			response = 0;
		} catch (PersistenceException e) {
			session.rollback();
			response = 1;
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			session.rollback();
			response = 1;
			e.printStackTrace();
			throw new PersistenceException(e.getMessage(), e);
		} finally {
			session.close();
		}
		return response;
		
	}
	
	public long getTimestampByDateString(String dateVal){
		long dt = 0;
		Calendar cal  = Calendar.getInstance();
		String[] str = dateVal.split("/");
		int mon = Integer.parseInt(str[0])-1;
		cal.set(Calendar.MONTH,mon);
		cal.set(Calendar.DAY_OF_MONTH,Integer.parseInt(str[1]));
		cal.set(Calendar.YEAR,Integer.parseInt(str[2]));
		dt = cal.getTimeInMillis();
		return dt;
	}
	
	public String getDateByTimestamp(long timestampVal){
		String dt = null;
		Calendar cal  = Calendar.getInstance();
		cal.setTimeInMillis(timestampVal);
		int mon = cal.get(Calendar.MONTH)+1;
		String month = null;
		if(mon < 10)
			month = "0"+mon;
		else
			month = String.valueOf(mon);
		if(cal.get(Calendar.DATE) >= 10)
			dt =month+"/"+cal.get(Calendar.DATE)+"/"+cal.get(Calendar.YEAR);
		else
			dt =month+"/0"+cal.get(Calendar.DATE)+"/"+cal.get(Calendar.YEAR);
		return dt;
	}
}