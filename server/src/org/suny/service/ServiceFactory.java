package org.suny.service;


import org.suny.security.UserPrincipal;
//import org.suny.service.impl.LocalTaskMangementService;
import org.suny.service.impl.LocalTaskMangementService;
import org.suny.service.impl.LocalUserMangementService;

public class ServiceFactory {
	
	private static ServiceFactory _factory = null;
	
	public static final short SERVICE_USER = 0;
	public static final short SERVICE_LIC = 1;
	
	private ServiceFactory() {
		
	}
	
	public static ServiceFactory  getInstance() {
		if(_factory == null) {
			_factory = new ServiceFactory();
		}
		return _factory;
	}
	
	/*public Service getService(UserPrincipal principal,short serviceType) {
		Service service = null;
		
		if(principal == null)
			return null;
		
		switch(serviceType){
		case SERVICE_USER :
			service = new LocalUserMangementService(principal);
			break;
		case SERVICE_LIC :
			service = new LocalTaskMangementService(principal);
			break;
		}
		return service;
	}*/
	public Service getService() {
		Service service = null;
		
		
		
		
		service = new LocalTaskMangementService();
			
		
		return service;
	}
	
	
}