package org.suny.service.util;
import javax.mail.PasswordAuthentication;

public class SMTPAuthenticator extends javax.mail.Authenticator  
    {  
		
		String userName = null;
		String password = null;
		public SMTPAuthenticator(String userName,String password){
			this.userName = userName;
			this.password = password;
		}
          public PasswordAuthentication getPasswordAuthentication()  
          {  
             
              return new PasswordAuthentication(userName, password);  
          }  
     }  
    