package org.suny.service.util;
import javax.mail.*;
import javax.mail.internet.*;

import java.util.Properties;

public class SimpleMail {
   /*public static void main(String[] args) throws Exception{
      Properties props = new Properties();
      props.setProperty("mail.transport.protocol", "smtp");
      props.setProperty("mail.host", "smtp.albany.edu");
      props.put("mail.smtp.auth", "true"); 
      props.put("mail.smtp.starttls.enable", "true");
      props.setProperty("mail.user", "pstudio");
      //props.setProperty("mail.password", "subbarao");
      Authenticator auth = new SMTPAuthenticator();  
      Session mailSession = Session.getDefaultInstance(props, auth);
      Transport transport = mailSession.getTransport();
      
      
      MimeMessage message = new MimeMessage(mailSession);
      message.setSubject("Testing javamail plain");
      message.setContent("This is a test", "text/plain");
      message.addRecipient(Message.RecipientType.TO,
           new InternetAddress("st613863@albany.edu"));

      transport.connect();
      //transport.sendMessage(message,
        //  message.getRecipients(Message.RecipientType.TO));
      transport.send(message);
      transport.close();
    }
   */
	public SimpleMail(String smtpServer,String userName,String password,String alias, String subject,String msg,String recipients[] ) throws Exception{
	      Properties props = new Properties();
	      props.setProperty("mail.transport.protocol", "smtp");
	      //props.setProperty("mail.host", "smtp.albany.edu");
	      props.setProperty("mail.host", smtpServer);
	      props.put("mail.smtp.auth", "true");
	      props.put("mail.debug", "true");
	      props.put("mail.smtp.starttls.enable", "true");
	      //props.setProperty("mail.user", "pstudio");
	      props.setProperty("mail.user", alias);
	      //props.setProperty("mail.password", "subbarao");
	      Authenticator auth = new SMTPAuthenticator(userName,password);  
	      Session mailSession = Session.getDefaultInstance(props, auth);
	      Transport transport = mailSession.getTransport();
	      MimeMessage message = new MimeMessage(mailSession);
	      //message.setSubject("Testing javamail plain");
	      message.setSubject(subject);
	      //message.setContent("This is a test", "text/plain");
	      message.setContent(msg, "text/plain");
	      //message.addRecipient(Message.RecipientType.TO,
	          // new InternetAddress("st613863@albany.edu"));
	      InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++) {
				addressTo[i] = new InternetAddress(recipients[i]);
			}
		  message.setRecipients(Message.RecipientType.TO, addressTo);	
	      transport.connect();
	      //transport.sendMessage(message,
	        //  message.getRecipients(Message.RecipientType.TO));
	      transport.send(message);
	      transport.close();
	 }
	
   
   
    public SimpleMail() {
		// TODO Auto-generated constructor stub
	}

    public void sendMessage(String smtpServer,String userName,String password,String alias, String subject,String mesg,String recipients[] ) throws Exception{
    	try {
    	    
    	   
    	    Properties props = System.getProperties();
    	    // XXX - could use Session.getTransport() and Transport.connect()
    	    // XXX - assume we're using SMTP
    		props.put("mail.smtp.host", smtpServer);

    	   /* // Get a Session object
    	    Session session = Session.getInstance(props, null);
    		session.setDebug(true);

    	    Message msg = new MimeMessage(session);*/
    		 Authenticator auth = new SMTPAuthenticator(userName,password);  
   	      Session mailSession = Session.getDefaultInstance(props, auth);
   	      Transport transport = mailSession.getTransport();
   	      MimeMessage msg = new MimeMessage(mailSession);
    		msg.setFrom(new InternetAddress(userName+"@albany.edu"));
    	    
    		InternetAddress[] addressTo = new InternetAddress[recipients.length]; 
            for (int i = 0; i < recipients.length; i++)
            {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);
           
    	   
    	    msg.setSubject(subject);

    	   
    	    msg.setText(mesg);
    	    msg.setHeader("X-Mailer", "msgsend");
    	   // msg.setSentDate(new Date());
    	    transport.connect();
    	    // send the thing off
    	    transport.send(msg);
    	    transport.close();
    	    System.out.println("\nMail was sent successfully.");

    	    /*
    	     * Save a copy of the message, if requested.
    	     */

    	} catch (Exception e) {
    	    e.printStackTrace();
    	}
        
    }

	public void postMail( String recipients[ ], String subject, String message , String from) throws MessagingException
    {
        boolean debug = false;

         //Set the host smtp address
         Properties props = new Properties();
         props.put("mail.smtp.host", "smtp.gmail.com");
         props.put("mail.smtp.starttls.enable", "true");
        // create some properties and get the default Session
        Session session = Session.getDefaultInstance(props, null);
        session.setDebug(debug);

        // create a message
        Message msg = new MimeMessage(session);

        // set the from and to address
        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);

        InternetAddress[] addressTo = new InternetAddress[recipients.length]; 
        for (int i = 0; i < recipients.length; i++)
        {
            addressTo[i] = new InternetAddress(recipients[i]);
        }
        msg.setRecipients(Message.RecipientType.TO, addressTo);
       

        // Optional : You can also set your custom headers in the Email if you Want
        //msg.addHeader("MyHeaderName", "myHeaderValue");

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message, "text/plain");
        Transport.send(msg);
    }
    
    
    
    /*public static void main(String args[]){
    	SimpleMail s = new SimpleMail();
		try {
			s.postMail(new String[]{"st613863@albany.edu"},"Hey Demo","Demo content","tumusudheer");
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
    
}
