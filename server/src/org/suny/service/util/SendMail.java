package org.suny.service.util;

import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class SendMail {

	private static SendMail sendMail = null;
	private Session  session = null;
	

	private SendMail() {
		try {
            Context envCtx = (Context)(new InitialContext()).lookup("java:comp/env");
            session = (Session) envCtx.lookup("mail/Session");
            session.setDebug(false);
        } catch (NamingException ex) {
            ResourceBundle mailProps = ResourceBundle.getBundle("mail");
            Properties props = new Properties();
            props.setProperty("mail.debug", mailProps.getString("mail.debug"));
            props.setProperty("mail.transport.protocol", mailProps.getString("mail.transport.protocol"));
            props.setProperty("mail.host", mailProps.getString("mail.host"));
            props.setProperty("mail.user", mailProps.getString("mail.user"));
            props.setProperty("mail.password", mailProps.getString("mail.password"));
            props.setProperty("mail.user", mailProps.getString("mail.user"));
            session = Session.getDefaultInstance(props, null);
            session.setDebug(false);
        }
	}

	public static SendMail getInstance() {
		if (sendMail == null) {
			sendMail = new SendMail();
		}
		return sendMail;
	}

	public void postMail(String recipients[], String subject, String message,
			String from) throws MessagingException {
		// create a message
		Message msg = new MimeMessage(session);
		msg.setSentDate(new Date());
		// set the from and to address
		InternetAddress addressFrom = new InternetAddress(from);
		msg.setFrom(addressFrom);
		InternetAddress[] addressTo = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			addressTo[i] = new InternetAddress(recipients[i]);
		}
		msg.setRecipients(Message.RecipientType.TO, addressTo);
		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/html");
		Transport.send(msg);
		return;
	}
	
}