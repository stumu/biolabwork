package org.suny.service;

import java.util.ArrayList;
import java.util.List;

import org.suny.beans.AmplificationDetailsBean;
import org.suny.beans.BTS559DetailsBean;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.CTS559DetailsBean;
import org.suny.beans.ColonyPCRDetailsBean;
import org.suny.beans.MiniADetailsBean;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.beans.QuickChangeDetailsBean;
import org.suny.beans.SearchGeneBean;
import org.suny.beans.ValueList;
import org.suny.beans.WaitingListEntryBean;
import org.suny.dao.PersistenceException;


public interface TaskMangementService  extends Service {
	public List getUserByLogin(String login,String password) throws PersistenceException ;
	public ValueList getMachineUnderUse() throws PersistenceException ;
	public ValueList searchGene(SearchGeneBean b) throws PersistenceException ;
	public ValueList getWaitingList() throws PersistenceException ;
	public ValueList getLabsInfo() throws PersistenceException ;
	public ValueList getMachinesInfo() throws PersistenceException ;
	public int saveAssignedMachine(BusyMachinesBean getForm) throws PersistenceException ;
	public int saveWaitingListEntry(WaitingListEntryBean b)  throws PersistenceException ;
	public int deleteAssignedMachine(String[] machineList) throws PersistenceException ;
	public int deleteWaitingListEntry(String[] machineList) throws PersistenceException ;
	public int sendEmailNotifications(String s) throws PersistenceException ;
	public PrimaryGeneDetailsBean getSelectedGeneDetails(String geneName) throws PersistenceException ;
	public ValueList getPrimarySequenceList(String geneName) throws PersistenceException ;
	public void uploadSequences(ArrayList beanList) throws PersistenceException ;
	public PrimaryGeneDetailsBean saveBasicGeneDetails(String geneName,PrimaryGeneDetailsBean getForm ) throws PersistenceException ;
	public PCR2PDetailsBean savePCR2PDetails(String geneName,PCR2PDetailsBean getForm ) throws PersistenceException ;
	public AmplificationDetailsBean saveAmplificationDetails(String geneName,AmplificationDetailsBean getForm ) throws PersistenceException ;
	public AmplificationDetailsBean saveRAmplificationDetails(String geneName,AmplificationDetailsBean getForm ) throws PersistenceException ;
	public PCR2PDetailsBean saveRPCR2PDetails(String geneName,PCR2PDetailsBean getForm ) throws PersistenceException ;
	public MiniADetailsBean saveMiniADetails(String geneName,MiniADetailsBean getForm ) throws PersistenceException ;
	public BTS559DetailsBean saveBTS559Details(String geneName,BTS559DetailsBean getForm ) throws PersistenceException ;
	public CTS559DetailsBean saveBTS559Details(String geneName,CTS559DetailsBean getForm ) throws PersistenceException ;
	public MiniADetailsBean getMiniADetails(String geneName,String oid) throws PersistenceException ;
	public BTS559DetailsBean getBTS559Details(String geneName,String oid) throws PersistenceException ;
	public CTS559DetailsBean getBTS559Details(String geneName,String oid,String temp) throws PersistenceException ;
	public ColonyPCRDetailsBean saveColonyPCRCDetails(String geneName,ColonyPCRDetailsBean getForm ) throws PersistenceException ;
	public ColonyPCRDetailsBean getColonyPCRCDetails(String geneName,String oid) throws PersistenceException ;
	public QuickChangeDetailsBean saveQchangeBDetails(String geneName,QuickChangeDetailsBean getForm ) throws PersistenceException ;
	public QuickChangeDetailsBean getQchangeBDetails(String geneName,String oid) throws PersistenceException ;
	public PCR2PDetailsBean getPCR2PDetails(String geneName) throws PersistenceException ;
	public AmplificationDetailsBean getAmplificationDetails(String geneName) throws PersistenceException ;
	public AmplificationDetailsBean getRAmplificationDetails(String geneName) throws PersistenceException ;
	public PCR2PDetailsBean getRPCR2PDetails(String geneName) throws PersistenceException ;
	public PCR2PDetailsBean getPCR2PDetailsHistory(String geneName,String id) throws PersistenceException ;
	public PCR2PDetailsBean getRPCR2PDetailsHistory(String geneName,String id) throws PersistenceException ;
	public AmplificationDetailsBean getAmplificationDetailsHistory(String geneName,String id) throws PersistenceException ;
	
}