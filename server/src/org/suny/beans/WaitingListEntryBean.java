
package org.suny.beans;


public class WaitingListEntryBean
{
    private int id;
    private String personName;
    private String labName;
    private String borrowdDate;
    private String releaseDate;
    private String phone;
    private String email;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBorrowdDate() {
		return borrowdDate;
	}
	public void setBorrowdDate(String borrowdDate) {
		this.borrowdDate = borrowdDate;
	}
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}