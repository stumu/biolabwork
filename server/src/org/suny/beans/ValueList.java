/**
 * ValueList.java
 *
 * Array list of value objects.
 */
package org.suny.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ValueList<E> extends ArrayList<E>
{
    /**serialVersionUID
	 * 
	 */
	private static final long serialVersionUID = -8809120138700215034L;

	/*
     * Constructor.
     * 
     * @param size
     */
    public ValueList(int size)
    {
        super(size);
    }

    /*
     * Constructor.
     */
    public ValueList()
    {
        super();
    }

    /*
     * Constructor.
     * 
     * @param collection
     */
    public ValueList(Collection collection)
    {
        super(collection);
    }

    /*
     * Returns a sublist.
     *
     * @param fromIndex starting index
     * @param toIndex end index
     */
    public List subList(int fromIndex, int toIndex)
    {
        if (toIndex < fromIndex) return null;
        
        ValueList subList = new ValueList(toIndex-fromIndex);
        for (int ii = fromIndex; ii < toIndex; ii++)
        {
            subList.add(this.get(ii));
        }
        return subList;
    }
    
    /*
     * Returns the size of this list.
     * 
     * @return size
     */
    public int getSize()
    {
        return size();
    }
    
}
