package org.suny.beans;

import java.sql.Timestamp;

public class MiniADetailsBean {
	private String id;
	private String ml;
	private String sequence;
	private String comments;
	private String user;
	private String datecreated;
	private Timestamp datemodified;
	private String colony;
	private String userdate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMl() {
		return ml;
	}
	public void setMl(String ml) {
		this.ml = ml;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
}
