package org.suny.beans;



/**
 * AbstractMachineInfo entity provides the base persistence definition of the MachineInfo entity. @author MyEclipse Persistence Tools
 */

public  class MachineInfoBean{


    // Fields    

     private Integer id;
     private String machine;
     private String serialNo;


    // Constructors

    /** default constructor */
    public MachineInfoBean() {
    }

    
   
    // Property accessors

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getMachine() {
        return this.machine;
    }
    
    public void setMachine(String machine) {
        this.machine = machine;
    }

    public String getSerialNo() {
        return this.serialNo;
    }
    
    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }
   








}