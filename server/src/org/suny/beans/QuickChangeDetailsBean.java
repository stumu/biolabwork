package org.suny.beans;

import java.sql.Timestamp;

public class QuickChangeDetailsBean {
	private String id;
	private String gene;
	private String comments;
	private String user;
	private String userdate= null;
	private String datecreated;
	private Timestamp datemodified;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGene() {
		return gene;
	}
	public void setGene(String gene) {
		this.gene = gene;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
	
}
