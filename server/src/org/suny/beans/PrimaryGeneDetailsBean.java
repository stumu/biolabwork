package org.suny.beans;

public class PrimaryGeneDetailsBean {
	public String primaryGeneName;
	public String getPrimaryGeneName() {
		return primaryGeneName;
	}
	public void setPrimaryGeneName(String primaryGeneName) {
		this.primaryGeneName = primaryGeneName;
	}
	public int geneLength;
	public String geneLocationStart;
	public String geneLocationEnd;
	String positiveStrand;
	String negativeStrand;
	String NtermY;
	String NtermN;
	String CtermY;
	String CtermN;
	String comments;
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getGeneLength() {
		return geneLength;
	}
	public void setGeneLength(int geneLength) {
		this.geneLength = geneLength;
	}
	public String getGeneLocationStart() {
		return geneLocationStart;
	}
	public void setGeneLocationStart(String geneLocationStart) {
		this.geneLocationStart = geneLocationStart;
	}
	public String getGeneLocationEnd() {
		return geneLocationEnd;
	}
	public void setGeneLocationEnd(String geneLocationEnd) {
		this.geneLocationEnd = geneLocationEnd;
	}
	public String getPositiveStrand() {
		return positiveStrand;
	}
	public void setPositiveStrand(String positiveStrand) {
		this.positiveStrand = positiveStrand;
	}
	public String getNegativeStrand() {
		return negativeStrand;
	}
	public void setNegativeStrand(String negativeStrand) {
		this.negativeStrand = negativeStrand;
	}
	public String getNtermY() {
		return NtermY;
	}
	public void setNtermY(String ntermY) {
		NtermY = ntermY;
	}
	public String getNtermN() {
		return NtermN;
	}
	public void setNtermN(String ntermN) {
		NtermN = ntermN;
	}
	public String getCtermY() {
		return CtermY;
	}
	public void setCtermY(String ctermY) {
		CtermY = ctermY;
	}
	public String getCtermN() {
		return CtermN;
	}
	public void setCtermN(String ctermN) {
		CtermN = ctermN;
	}
	
	
}
