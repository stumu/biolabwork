package org.suny.beans;

public class PrimerSequenceDataBean {
	public String gene;
	public String primer;
	public String primerSequence;
	public String start;
	public String end;
	public String Plate;
	public String well_Position;
	public String user;
	public String getGene() {
		return gene;
	}
	public void setGene(String gene) {
		this.gene = gene;
	}
	public String getPrimer() {
		return primer;
	}
	public void setPrimer(String primer) {
		this.primer = primer;
	}
	public String getPrimerSequence() {
		return primerSequence;
	}
	public void setPrimerSequence(String primerSequence) {
		this.primerSequence = primerSequence;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getPlate() {
		return Plate;
	}
	public void setPlate(String plate) {
		Plate = plate;
	}
	public String getWell_Position() {
		return well_Position;
	}
	public void setWell_Position(String well_Position) {
		this.well_Position = well_Position;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
}
