package org.suny.beans;

import java.sql.Timestamp;

public class ColonyPCRDetailsBean {
	private String basicGeneName  ;
	private String basicVal  ;
	private String id;
	private String oid;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String comments;
	private String datecreated;
	private Timestamp datemodified;
	private String[] sequence;
	
	public String[] getSequence() {
		return sequence;
	}
	public void setSequence(String[] sequence) {
		this.sequence = sequence;
	}
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getPrimer1() {
		return primer1;
	}
	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}
	public String getPrimer2() {
		return primer2;
	}
	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	
}
