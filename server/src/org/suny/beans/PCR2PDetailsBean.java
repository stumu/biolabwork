package org.suny.beans;

public class PCR2PDetailsBean {
	private String user = null;
	private String serverDate = null;
	private String oid = null;
	
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getServerDate() {
		return serverDate;
	}
	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}
	private String basicGeneName  = null;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	private String pcrdate  = null;
	private String size = null;
	private String amplicony = null;
	private String ampliconn = null;
	private String comments = null;
	public String getPcrdate() {
		return pcrdate;
	}
	public void setPcrdate(String pcrdate) {
		this.pcrdate = pcrdate;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getAmplicony() {
		return amplicony;
	}
	public void setAmplicony(String amplicony) {
		this.amplicony = amplicony;
	}
	public String getAmpliconn() {
		return ampliconn;
	}
	public void setAmpliconn(String ampliconn) {
		this.ampliconn = ampliconn;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
