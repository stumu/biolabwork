package org.suny.beans;



public class LabsInfoBean {

	// Fields

	private Integer id;
	private String labName;
	private String labProfessor;
	private Long dateCreated;
	private Long dateModified;

	// Constructors

	/** default constructor */
	public LabsInfoBean() {
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLabName() {
		return this.labName;
	}

	public void setLabName(String labName) {
		this.labName = labName;
	}

	public String getLabProfessor() {
		return this.labProfessor;
	}

	public void setLabProfessor(String labProfessor) {
		this.labProfessor = labProfessor;
	}

	public Long getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Long dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Long getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Long dateModified) {
		this.dateModified = dateModified;
	}

}