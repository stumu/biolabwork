package org.suny.dao.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class HibernateSession {

	private Session _session = null;
	

	private Transaction _transaction = null;
	
	

	public HibernateSession(Session session) {
		session.setFlushMode(FlushMode.COMMIT);
		this._session = session;
		SessionManager.setSession(session);
	}

	public Session getSession() {
		
		return _session;
	}

	public void beginTransaction() {
		if (_transaction == null) {
			_transaction = _session.beginTransaction();
		}
	}

	private void checkSession() {
		// TODO implement
	}

	public void close() {
		if (_session != null)
			_session.close();
		_session = null;
		_transaction = null;
	}

	public void commit() {
		if (_transaction != null)
			_transaction.commit();

	}

	public void rollback() {
		if (_transaction != null)
			_transaction.rollback();
	}
	
	public Query createQuery(String hql){
		
		return	_session.createQuery(hql);
		
	}
	
	public void evict(Object obj){
		_session.evict(obj);
	}
	
	public void flush(){
		if (_session != null)
			_session.flush();
	}
}