package org.suny.dao.hibernate;

import java.util.List;

import org.suny.dao.PersistenceException;


/**
 * UserDao.java
 * 
 * Hibernate implementation of User DAO.
 */
public class UserDao extends BaseDaoImpl {

    //static private Logger log = Logger.getLogger(UserDao.class.getName());
	public static UserDao dao = new UserDao();
    public UserDao() {
        super();
    }

	
    /**
     * Get user with the given login.
     * 
     * @param login
     * @return User
     * @throws PersistenceException
     */
    public List getUserByLogin(String login,String password) throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from LabUsers as user where user.username=? and user.password = ?", new Object[]{login,password});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }

   
    
}
