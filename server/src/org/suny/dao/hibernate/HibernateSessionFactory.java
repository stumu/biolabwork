package org.suny.dao.hibernate;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.suny.dao.PersistenceException;
import org.suny.model.Amplification;
import org.suny.model.Amplificationhistory;
import org.suny.model.Basicgenedetails;
import org.suny.model.BorrowerDetails;
import org.suny.model.Bts559;
import org.suny.model.Bts559history;
import org.suny.model.Colonypcr;
import org.suny.model.Colonypcrhistory;
import org.suny.model.Emailsettings;
import org.suny.model.LabUsers;
import org.suny.model.LabsInfo;
import org.suny.model.MachineInfo;
import org.suny.model.Minia;
import org.suny.model.Miniahistory;
import org.suny.model.Pcrdetails;
import org.suny.model.Pcrdetailshistory;
import org.suny.model.Primersequence;
import org.suny.model.Quickchnage;
import org.suny.model.Quickchnagehistory;
import org.suny.model.Ramplification;
import org.suny.model.Ramplificationhistory;
import org.suny.model.Rminia;
import org.suny.model.Rminiahistory;
import org.suny.model.Rpcrdetails;
import org.suny.model.Rpcrdetailshistory;
import org.suny.model.Tkgenedump;
import org.suny.model.Waitinglist;

public class HibernateSessionFactory {
	private static SessionFactory _sessionFactory;

	static {
		CacheConfiguration cfg = null;
		try {
			cfg = new CacheConfiguration();
			cfg.setNamingStrategy(ImprovedNamingStrategy.INSTANCE);
			cfg.addClass(LabUsers.class)
			.addClass(BorrowerDetails.class)
			.addClass(LabsInfo.class)
			.addClass(MachineInfo.class)
			.addClass(Waitinglist.class)
			.addClass(Emailsettings.class)
			.addClass(Tkgenedump.class)
			.addClass(Basicgenedetails.class)
			.addClass(Primersequence.class)
			.addClass(Pcrdetails.class)
			.addClass(Pcrdetailshistory.class)
			.addClass(Rpcrdetails.class)
			.addClass(Rpcrdetailshistory.class)
			.addClass(Amplification.class)
			.addClass(Amplificationhistory.class)
			.addClass(Ramplification.class)
			.addClass(Ramplificationhistory.class)
			.addClass(Minia.class)
			.addClass(Miniahistory.class)
			.addClass(Rminia.class)
			.addClass(Rminiahistory.class)
			.addClass(Quickchnage.class)
			.addClass(Quickchnagehistory.class)
			.addClass(Colonypcr.class)
			.addClass(Colonypcrhistory.class)
			.addClass(Bts559.class)
			.addClass(Bts559history.class);
			_sessionFactory = cfg.buildSessionFactory();
		} catch (HibernateException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	static public HibernateSession openSession() throws PersistenceException {
		try {
			Session session = _sessionFactory.openSession();
			if (session.connection().getAutoCommit())
				session.connection().setAutoCommit(false);
			return new HibernateSession(session);
		} catch (HibernateException e) {
			throw new PersistenceException("Failed to open hibernate session.",
					e);
		} catch (SQLException e) {
			throw new HibernateException("Failed to open hibernate session.", e);
		}
	}

	static public void close() {
		_sessionFactory.close();
	}
}