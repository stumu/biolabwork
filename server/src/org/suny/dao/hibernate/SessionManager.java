package org.suny.dao.hibernate;

import org.hibernate.Session;

public class SessionManager {
	private static ThreadLocal sessionHolder = new ThreadLocal();

	public static void setSession(Session session) {
		sessionHolder.set(session);
	}

	public static Session getSession() {
		return (Session) sessionHolder.get();
	}
}