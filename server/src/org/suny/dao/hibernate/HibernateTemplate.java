package org.suny.dao.hibernate;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.suny.dao.PersistenceException;





/**
 * HibernateTemplate.java
 * 
 * Provides convenience methods for hibernate.
 */
public class HibernateTemplate {

	// private Session session = null;

	private boolean cacheQueries = false;

	private String queryCacheRegion = null;

	HibernateTemplate() {
	}

	private Session getSession() {
		return SessionManager.getSession();
	}

	// -------------------------------------------------------------------------
	// Convenience finder methods for HQL strings
	// -------------------------------------------------------------------------

	public List find(String queryString) throws PersistenceException {
		return find(queryString, (Object[]) null);
	}

	public List find(String queryString, Object value)
			throws PersistenceException {
		return find(queryString, new Object[] { value });
	}

	public List find(final String queryString, final Object[] values)
			throws PersistenceException {
		try {
			Query query = getSession().createQuery(queryString);
			//System.out.println("$$$$$$: "+query);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					query.setParameter(i, values[i]);
				}
			}
			return query.list();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}


	public List findByNamedParam(String queryString, String paramName,
			Object value) throws PersistenceException {
		return findByNamedParam(queryString, new String[] { paramName },
				new Object[] { value });
	}

	public List findByNamedParam(final String queryString,
			final String[] paramNames, final Object[] values)
			throws PersistenceException {

		try {
			if (paramNames.length != values.length) {
				throw new IllegalArgumentException(
						"Length of paramNames array must match length of values array");
			}

			Query query = getSession().createQuery(queryString);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					applyNamedParameterToQuery(query, paramNames[i], values[i]);
				}
			}
			return query.list();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}

	/**
	 * Apply the given name parameter to the given Query object.
	 * 
	 * @param queryObject
	 *            the Query object
	 * @param paramName
	 *            the name of the parameter
	 * @param value
	 *            the value of the parameter
	 * @throws HibernateException
	 *             if thrown by the Query object
	 */
	protected void applyNamedParameterToQuery(Query queryObject,
			String paramName, Object value) throws HibernateException {
		if (value instanceof Collection) {
			queryObject.setParameterList(paramName, (Collection) value);
		} else if (value instanceof Object[]) {
			queryObject.setParameterList(paramName, (Object[]) value);
		} else {
			queryObject.setParameter(paramName, value);
		}
	}

	// -------------------------------------------------------------------------
	// Convenience finder methods for named queries
	// -------------------------------------------------------------------------

	public List findByNamedQuery(String queryName) throws PersistenceException {
		return findByNamedQuery(queryName, (Object[]) null);
	}

	public List findByNamedQuery(String queryName, Object value)
			throws PersistenceException {
		return findByNamedQuery(queryName, new Object[] { value });
	}

	public List findByNamedQuery(final String queryName, final Object[] values)
			throws PersistenceException {
		try {
			Query query = getSession().getNamedQuery(queryName);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					query.setParameter(i, values[i]);
				}
			}
			return query.list();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}

	public List findByNamedQueryAndNamedParam(String queryName,
			String paramName, Object value) throws PersistenceException {
		return findByNamedQueryAndNamedParam(queryName,
				new String[] { paramName }, new Object[] { value });
	}

	public List findByNamedQueryAndNamedParam(final String queryName,
			final String[] paramNames, final Object[] values)
			throws PersistenceException {

		try {
			if (paramNames != null && values != null
					&& paramNames.length != values.length) {
				throw new IllegalArgumentException(
						"Length of paramNames array must match length of values array");
			}

			Query query = getSession().getNamedQuery(queryName);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					applyNamedParameterToQuery(query, paramNames[i], values[i]);
				}
			}
			return query.list();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}

	// -------------------------------------------------------------------------
	// Convenience query methods for iteratation
	// -------------------------------------------------------------------------

	public Iterator iterate(String queryString) throws PersistenceException {
		return iterate(queryString, (Object[]) null);
	}

	public Iterator iterate(String queryString, Object value)
			throws PersistenceException {
		return iterate(queryString, new Object[] { value });
	}

	public Iterator iterate(final String queryString, final Object[] values)
			throws PersistenceException {
		try {
			Query query = getSession().createQuery(queryString);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					query.setParameter(i, values[i]);
				}
			}
			return query.iterate();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}

	public void closeIterator(Iterator it) throws PersistenceException {
		try {
			Hibernate.close(it);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to close iterator.", e);
		}
	}

	/**
	 * Prepare the given Query object, applying cache settings and/or a
	 * transaction timeout.
	 * 
	 * @param queryObject
	 *            the Query object to prepare
	 */
	protected void prepareQuery(Query queryObject) {
		if (isCacheQueries()) {
			queryObject.setCacheable(true);
			if (getQueryCacheRegion() != null) {
				queryObject.setCacheRegion(getQueryCacheRegion());
			}
		}
	}

	/**
	 * Set whether to cache all queries executed by this template. If this is
	 * true, all Query and Criteria objects created by this template will be
	 * marked as cacheable (including all queries through find methods).
	 */
	public void setCacheQueries(boolean cacheQueries) {
		this.cacheQueries = cacheQueries;
	}

	/**
	 * Return whether to cache all queries executed by this template.
	 */
	public boolean isCacheQueries() {
		return cacheQueries;
	}

	/**
	 * Set the name of the cache region for queries executed by this template.
	 * If this is specified, it will be applied to all Query and Criteria
	 * objects created by this template (including all queries through find
	 * methods).
	 */
	public void setQueryCacheRegion(String queryCacheRegion) {
		this.queryCacheRegion = queryCacheRegion;
	}

	/**
	 * Return the name of the cache region for queries executed by this
	 * template.
	 */
	public String getQueryCacheRegion() {
		return queryCacheRegion;
	}

	// -------------------------------------------------------------------------
	// Convenience methods for deleting individual objects
	// -------------------------------------------------------------------------

	public void delete(Object entity) throws PersistenceException {
		delete(entity, null);
	}

	public void delete(final Object entity, final LockMode lockMode)
			throws PersistenceException {
		try {
			if (lockMode != null) {
				getSession().lock(entity, lockMode);
			}
			getSession().delete(entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to delete object.", e);
		}
	}

	public void deleteAll(final Collection entities)
			throws PersistenceException {
		try {
			for (Iterator it = entities.iterator(); it.hasNext();) {
				getSession().delete(it.next());
			}
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to delete objects.", e);
		}
	}

	// -------------------------------------------------------------------------
	// Convenience methods for loading individual objects
	// -------------------------------------------------------------------------

	public Object get(Class entityClass, Serializable id)
			throws PersistenceException {
		return get(entityClass, id, null);
	}

	public Object get(final Class entityClass, final Serializable id,
			final LockMode lockMode) throws PersistenceException {

		try {
			if (lockMode != null) {
				return getSession().get(entityClass, id, lockMode);
			}
			return getSession().get(entityClass, id);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to get object.", e);
		}
	}

	public Object get(String entityName, Serializable id)
			throws PersistenceException {
		return get(entityName, id, null);
	}

	public Object get(final String entityName, final Serializable id,
			final LockMode lockMode) throws PersistenceException {

		try {
			if (lockMode != null) {
				return getSession().get(entityName, id, lockMode);
			}
			return getSession().get(entityName, id);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to get object.", e);
		}
	}

	public void refresh(final Object entity) throws PersistenceException {
		refresh(entity, null);
	}

	public void refresh(final Object entity, final LockMode lockMode)
			throws PersistenceException {
		try {
			if (lockMode != null) {
				getSession().refresh(entity, lockMode);
			} else {
				getSession().refresh(entity);
			}
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to refresh object.", e);
		}
	}

	// -------------------------------------------------------------------------
	// Convenience methods for storing individual objects
	// -------------------------------------------------------------------------

	public void lock(final Object entity, final LockMode lockMode)
			throws PersistenceException {
		try {
			getSession().lock(entity, lockMode);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to lock object.", e);
		}
	}

	public void lock(final String entityName, final Object entity,
			final LockMode lockMode) throws PersistenceException {
		try {
			getSession().lock(entityName, entity, lockMode);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to lock object.", e);
		}
	}

	public Object save(final Object entity) throws PersistenceException {
		try {
			return getSession().save(entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to save object.", e);
		}
	}

	public Object save(final String entityName, final Object entity)
			throws PersistenceException {
		try {
			return getSession().save(entityName, entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to save object.", e);
		}
	}

	public void update(Object entity) throws PersistenceException {
		update(entity, null);
	}

	public void update(final Object entity, final LockMode lockMode)
			throws PersistenceException {
		try {
			getSession().update(entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to update object.", e);
		}
	}

	public void update(String entityName, Object entity)
			throws PersistenceException {
		update(entityName, entity, null);
	}

	public void update(final String entityName, final Object entity,
			final LockMode lockMode) throws PersistenceException {
		try {
			getSession().update(entityName, entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to update object.", e);
		}
	}

	public void saveOrUpdate(final Object entity) throws PersistenceException {
		try {
			// getSession().saveOrUpdate(entity);
			SessionManager.getSession().saveOrUpdate(entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to save or update object.", e);
		}
	}

	public void saveOrUpdate(final String entityName, final Object entity)
			throws PersistenceException {
		try {
			getSession().saveOrUpdate(entityName, entity);
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to save or update object.", e);
		}
	}

	public void saveOrUpdateAll(final Collection entities)
			throws PersistenceException {
		try {
			for (Iterator it = entities.iterator(); it.hasNext();) {
				getSession().saveOrUpdate(it.next());
			}
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to save or update object.", e);
		}
	}

	public List find(final String queryString, final Object[] values,
			int maxCount) throws PersistenceException {
		try {
			Query query = getSession().createQuery(queryString);
			prepareQuery(query);
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					query.setParameter(i, values[i]);
				}
			}
			if (maxCount > 0)
				query.setMaxResults(maxCount);
			return query.list();
		} catch (HibernateException e) {
			throw new PersistenceException("Fail to execute query.", e);
		}
	}
}
