package org.suny.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.suny.dao.PersistenceException;
import org.suny.model.BorrowerDetails;
import org.suny.model.Waitinglist;



/**
 * UserDao.java
 * 
 * Hibernate implementation of User DAO.
 */
public class WaitingListDao extends BaseDaoImpl {

    //static private Logger log = Logger.getLogger(UserDao.class.getName());
	public static WaitingListDao dao = new WaitingListDao();
    public WaitingListDao() {
        super();
    }

	
    /**
     * Get user with the given login.
     * 
     * @param login
     * @return User
     * @throws PersistenceException
     */
    public List getActiveWaitingList() throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from Waitinglist as machine where machine.isactive=?", "1");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    
    public void saveWaitingListEntry(Waitinglist i) throws PersistenceException {
		 getHibernateTemplate().save(i);
	}
    public int deleteWaitingListEntryById(String[] idList) throws PersistenceException {
    	int result = 1;
    	List params = new ArrayList();
    	StringBuffer queryStringBuf = new StringBuffer();
    	//String query="from BorrowerDetails alert where alert.id in (?)";
    	queryStringBuf.append("from Waitinglist alert where alert.id in (");
    	for (int i = 0; i < idList.length; i++) {
			queryStringBuf.append(" ? ");
			params.add(new Integer(idList[i]));
			if (i != idList.length - 1)
				queryStringBuf.append(" , ");
		}
		queryStringBuf.append(" )  ");
		String query = queryStringBuf.toString().trim();
    	List list = getHibernateTemplate().find(query,idList);		
		if(list != null && list.size() != 0) {
			 getHibernateTemplate().deleteAll(list);
			 result = 0;
		     return result;
		 }
		return result;
    }
}
