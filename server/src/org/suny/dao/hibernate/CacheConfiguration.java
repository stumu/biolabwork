/*
 * $Id: CacheConfiguration.java,v 1.1 2008/07/17 05:58:28 sudheer Exp $ 
 *
 * Copyright 2005 NetEnrich, Inc. All rights reserved.
 * NetEnrich PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package org.suny.dao.hibernate;

import java.net.URL;
import java.util.logging.Logger;

import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;

/**
 * CacheConfiguration.java
 * 
 * Extends the default hibernate Configuration to cache the xml mapping files.
 */
public class CacheConfiguration extends Configuration
{
    private static Logger log = Logger.getLogger( CacheConfiguration.class.getName() );

    public CacheConfiguration addCachableClass(Class persistentClass) throws MappingException
    {
        log.info( "Mapping resource: " + persistentClass.getName() );
        String fileName = persistentClass.getName().replace( '.', '/' ) + ".hbm.xml";
        URL url = persistentClass.getClassLoader().getResource( fileName );
        if ( url == null )
        {
            log.info( "Cannot find resource xml "+fileName );
            return this;
        }
        String rscFile = url.getFile();
        log.info( "Using resource file: " + rscFile );
        addCacheableFile( rscFile );
        return this;
    }

}
