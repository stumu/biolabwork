package org.suny.dao.hibernate;

/**
 * BaseDAOImpl.java
 */
public abstract class BaseDaoImpl {

	private HibernateTemplate template = null;

	public BaseDaoImpl() {
		this.template = new HibernateTemplate();
	}

	protected HibernateTemplate getHibernateTemplate() {
		return template;
	}
}
