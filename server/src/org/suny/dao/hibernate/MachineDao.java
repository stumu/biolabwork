package org.suny.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.suny.dao.PersistenceException;
import org.suny.model.Amplification;
import org.suny.model.Amplificationhistory;
import org.suny.model.Basicgenedetails;
import org.suny.model.BorrowerDetails;
import org.suny.model.Bts559;
import org.suny.model.Bts559history;
import org.suny.model.Colonypcr;
import org.suny.model.Colonypcrhistory;
import org.suny.model.Minia;
import org.suny.model.Miniahistory;
import org.suny.model.Pcrdetails;
import org.suny.model.Pcrdetailshistory;
import org.suny.model.Primersequence;
import org.suny.model.Quickchnage;
import org.suny.model.Quickchnagehistory;
import org.suny.model.Ramplification;
import org.suny.model.Ramplificationhistory;
import org.suny.model.Rpcrdetails;
import org.suny.model.Rpcrdetailshistory;



/**
 * UserDao.java
 * 
 * Hibernate implementation of User DAO.
 */
public class MachineDao extends BaseDaoImpl {

    //static private Logger log = Logger.getLogger(UserDao.class.getName());
	public static MachineDao dao = new MachineDao();
    public MachineDao() {
        super();
    }

	
    /**
     * Get user with the given login.
     * 
     * @param login
     * @return User
     * @throws PersistenceException
     */
    public List getMachineUnderUse() throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from BorrowerDetails as machine where machine.usageflag=?", "1");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getPrimarySequence(String geneName) throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from Primersequence as m where m.id.gene=?", geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getLabs() throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from LabsInfo as lab");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    
    public List getLabIdByLabName(String labName) throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from LabsInfo as lab where lab.labName=?",labName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    
    
    public List getMachines() throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from MachineInfo as m");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    
    public List getEmailSettings() throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from Emailsettings as m");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getMachineIdByMachineName(String machineName) throws PersistenceException {
        List resultSet = getHibernateTemplate().find("from MachineInfo as m where m.machine=?",machineName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List searchGeneList(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Tkgenedump as m where m.name like ?","%"+geneName+"%");
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getGeneDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Tkgenedump as m where m.name = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getBasicGeneDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Basicgenedetails as m where m.name = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public void saveAssignMachineBean(BorrowerDetails i) throws PersistenceException {
		 getHibernateTemplate().save(i);
	}
    public void saveBasicGeneDetails(Basicgenedetails i) throws PersistenceException {
		 getHibernateTemplate().saveOrUpdate(i);
	}
    public void savePrimerSequenceDetails(Primersequence i) throws PersistenceException {
		 getHibernateTemplate().saveOrUpdate(i);
	}
    public List getMiniADetails(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Minia as m where m.id.gene = ? and m.id.id=?",new Object[]{geneName,id});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getBTS559Details(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Bts559 as m where m.id.gene = ? and m.id.id=?",new Object[]{geneName,id});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List geColonyPCRCDetails(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Colonypcr as m where m.id.gene = ? and m.id.id=?",new Object[]{geneName,id});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getQChangeBDetails(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Quickchnage as m where m.id.gene = ? and m.id.id=?",new Object[]{geneName,id});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getPCR2PDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Pcrdetails as m where m.gene = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getAmplificationDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Amplification as m where m.gene = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getAmplificationDetailsHistory(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
    	List resultSet = null;
        if(Integer.parseInt(id) == 0)
        	resultSet = getHibernateTemplate().find("from Amplificationhistory as m where m.gene = ? ",new Object[]{geneName},1);
        else
        	resultSet = getHibernateTemplate().find("from Amplificationhistory as m where m.gene = ? and m.id=?",new Object[]{geneName,Integer.parseInt(id)});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getRAmplificationDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Ramplification as m where m.gene = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getRPCR2PDetails(String geneName) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
        List resultSet = getHibernateTemplate().find("from Rpcrdetails as m where m.gene = ?",geneName);
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getRPCR2PDetailsHistory(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
    	
        List resultSet = null;
        if(Integer.parseInt(id) == 0)
        	resultSet = getHibernateTemplate().find("from Rpcrdetailshistory as m where m.gene = ? ",new Object[]{geneName},1);
        else
        	resultSet = getHibernateTemplate().find("from Rpcrdetailshistory as m where m.gene = ? and m.id=?",new Object[]{geneName,Integer.parseInt(id)});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public List getPCR2PDetailsHistory(String geneName,String id) throws PersistenceException {
    	//geneName="%"+geneName+"%'";
    	
        List resultSet = null;
        if(Integer.parseInt(id) == 0)
        	resultSet = getHibernateTemplate().find("from Pcrdetailshistory as m where m.gene = ? ",new Object[]{geneName},1);
        else
        	resultSet = getHibernateTemplate().find("from Pcrdetailshistory as m where m.gene = ? and m.id=?",new Object[]{geneName,Integer.parseInt(id)});
        if (resultSet != null && resultSet.size() > 0) return resultSet;
        return null;
    }
    public void savePCR2PDetails(Pcrdetails i) throws PersistenceException {
    	/*List resultSet = getHibernateTemplate().find("from Pcrdetails as m where m.gene = ?",i.getGene());
    	Pcrdetails old = null;
        if (resultSet != null && resultSet.size() > 0)
        {*/
        	Pcrdetailshistory ph = new Pcrdetailshistory();
        	//old = (Pcrdetails)resultSet.get(0);
        	ph.setAmpliconn(i.getAmpliconn());
        	ph.setAmplicony(i.getAmplicony());
        	ph.setComments(i.getComments());
        	ph.setDatecreated(i.getDatecreated());
        	ph.setDatemodified(i.getDatemodified());
        	ph.setGene(i.getGene());
        	ph.setSize(i.getSize());
        	ph.setUser(i.getUser());
        	ph.setUserdate(i.getUserdate());
        	getHibernateTemplate().save(ph);
        //}
        //if( old == null)
        	getHibernateTemplate().saveOrUpdate(i);
        /*else
        {
        	old.setAmpliconn(i.getAmpliconn());
        	old.setAmplicony(i.getAmplicony());
        	old.setComments(i.getComments());
        	old.setDatecreated(i.getDatecreated());
        	old.setDatemodified(i.getDatemodified());
        	old.setGene(i.getGene());
        	old.setSize(i.getSize());
        	old.setUser(i.getUser());
        	old.setUserdate(i.getUserdate());
        	getHibernateTemplate().saveOrUpdate(old);
        }*/
	}
    
    public void saveMiniADetails(Minia i) throws PersistenceException {
        	Miniahistory ph = new Miniahistory();
        	ph.setColony(i.getColony());
        	ph.setComments(i.getComments());
        	ph.setDatecreated(i.getDatecreated());
        	ph.setDatemodified(i.getDatemodified());
        	ph.setGene(i.getId().getGene());
        	ph.setMl(i.getMl());
        	ph.setOid(i.getId().getId());
        	ph.setSequence(i.getSequence());
        	ph.setUesrdate(i.getUserdate());
        	ph.setUser(i.getUser());
        	getHibernateTemplate().save(ph);
        	getHibernateTemplate().saveOrUpdate(i);
	}
    public void saveBTS559Details(Bts559 i) throws PersistenceException {
    	Bts559history ph = new Bts559history();
    	ph.setComments(i.getComments());
    	ph.setDatecreated(i.getDatecreated());
    	ph.setDatemodified(i.getDatemodified());
    	ph.setGene(i.getId().getGene());
    	ph.setOid(i.getId().getId());
    	ph.setUesrdate(i.getUserdate());
    	ph.setUser(i.getUser());
    	getHibernateTemplate().save(ph);
    	getHibernateTemplate().saveOrUpdate(i);
}
    public void saveColonyPCRCDetails(Colonypcr i) throws PersistenceException {
    	Colonypcrhistory ph = new Colonypcrhistory();
    	ph.setOid(i.getId().getId());
    	ph.setComments(i.getComments());
    	ph.setGene(i.getId().getGene());
    	ph.setDate1(i.getDate1());
    	ph.setDate2(i.getDate2());
    	ph.setDate3(i.getDate3());
    	ph.setPrimer1(i.getPrimer1());
    	ph.setPrimer2(i.getPrimer2());
    	ph.setSeq(i.getSeq());
    	ph.setSize(i.getSize());
    	ph.setDatecreated(i.getDatecreated());
    	ph.setDatemodified(i.getDatemodified());
    	getHibernateTemplate().save(ph);
    	getHibernateTemplate().saveOrUpdate(i);
}
    public void saveQchangeBDetails(Quickchnage i) throws PersistenceException {
    	Quickchnagehistory ph = new Quickchnagehistory();
    	ph.setComments(i.getComments());
    	ph.setDatecreated(i.getDatecreated());
    	ph.setDatemodified(i.getDatemodified());
    	ph.setGene(i.getId().getGene());
    	ph.setOid(i.getId().getId());
    	ph.setUserdate(i.getUserdate());
    	ph.setUser(i.getUser());
    	getHibernateTemplate().save(ph);
    	getHibernateTemplate().saveOrUpdate(i);
}
    public void saveAmplificationDetails(Amplification i) throws PersistenceException {
        	Amplificationhistory ph = new Amplificationhistory();
        	ph.setComments(i.getComments());
        	ph.setDatecreated(i.getDatecreated());
        	ph.setDatemodified(i.getDatemodified());
        	ph.setExcess(i.getExcess());
        	ph.setGene(i.getGene());
        	ph.setMl(i.getMl());
        	ph.setPosition(i.getPosition());
        	ph.setStore(i.getStore());
        	ph.setUser(i.getUser());
        	ph.setUserdate(i.getUserdate());
        	ph.setVolume(i.getVolume());
        	getHibernateTemplate().save(ph);
        	getHibernateTemplate().saveOrUpdate(i);
	}
    public void saveRAmplificationDetails(Ramplification i) throws PersistenceException {
    	Ramplificationhistory ph = new Ramplificationhistory();
    	ph.setComments(i.getComments());
    	ph.setDatecreated(i.getDatecreated());
    	ph.setDatemodified(i.getDatemodified());
    	ph.setExcess(i.getExcess());
    	ph.setGene(i.getGene());
    	ph.setMl(i.getMl());
    	ph.setPosition(i.getPosition());
    	ph.setStore(i.getStore());
    	ph.setUser(i.getUser());
    	ph.setUserdate(i.getUserdate());
    	ph.setVolume(i.getVolume());
    	getHibernateTemplate().save(ph);
    	getHibernateTemplate().saveOrUpdate(i);
}
    public void saveRPCR2PDetails(Rpcrdetails i) throws PersistenceException {
    	/*List resultSet = getHibernateTemplate().find("from Pcrdetails as m where m.gene = ?",i.getGene());
    	Pcrdetails old = null;
        if (resultSet != null && resultSet.size() > 0)
        {*/
        	Rpcrdetailshistory ph = new Rpcrdetailshistory();
        	//old = (Pcrdetails)resultSet.get(0);
        	ph.setAmpliconn(i.getAmpliconn());
        	ph.setAmplicony(i.getAmplicony());
        	ph.setComments(i.getComments());
        	ph.setDatecreated(i.getDatecreated());
        	ph.setDatemodified(i.getDatemodified());
        	ph.setGene(i.getGene());
        	ph.setSize(i.getSize());
        	ph.setUser(i.getUser());
        	ph.setUserdate(i.getUserdate());
        	getHibernateTemplate().save(ph);
        //}
        //if( old == null)
        	getHibernateTemplate().saveOrUpdate(i);
        /*else
        {
        	old.setAmpliconn(i.getAmpliconn());
        	old.setAmplicony(i.getAmplicony());
        	old.setComments(i.getComments());
        	old.setDatecreated(i.getDatecreated());
        	old.setDatemodified(i.getDatemodified());
        	old.setGene(i.getGene());
        	old.setSize(i.getSize());
        	old.setUser(i.getUser());
        	old.setUserdate(i.getUserdate());
        	getHibernateTemplate().saveOrUpdate(old);
        }*/
	}
    public int deleteMachineByMachineId(String[] idList) throws PersistenceException {
    	int result = 1;
    	List params = new ArrayList();
    	StringBuffer queryStringBuf = new StringBuffer();
    	//String query="from BorrowerDetails alert where alert.id in (?)";
    	queryStringBuf.append("from BorrowerDetails alert where alert.id in (");
    	for (int i = 0; i < idList.length; i++) {
			queryStringBuf.append(" ? ");
			params.add(new Integer(idList[i]));
			if (i != idList.length - 1)
				queryStringBuf.append(" , ");
		}
		queryStringBuf.append(" )  ");
		String query = queryStringBuf.toString().trim();
    	List list = getHibernateTemplate().find(query,idList);		
		if(list != null && list.size() != 0) {
			 getHibernateTemplate().deleteAll(list);
			 result = 0;
		     return result;
		 }
		return result;
    }
}
