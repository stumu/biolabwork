package org.suny.security;

import java.security.Principal;


public class UserPrincipal implements Principal, java.io.Serializable
{
	public static  UserPrincipal adminPrincipal = new UserPrincipal("admin",1,"",true);
	
    private String name;
	private long userId;
	private String loginName;
	private boolean isAdmin = false;
	
	public UserPrincipal(String loginName, long userId, String name,boolean isAdmin)
	{
		this.name = name;
		this.loginName = loginName;
		this.userId = userId;
		this.isAdmin = isAdmin;
	}

	public boolean equals(Object o)
	{
		if (o == null)
			return false;

		if (this == o)
			return true;

		if(!(o instanceof UserPrincipal)) {
			return false; 
		}
		
		if(((UserPrincipal) o).getName().equals(loginName))	
			return true;
		else
			return false;
	}
	
	public int hashCode(){
		return loginName.hashCode();
	}

	public String toString(){
		return loginName;
	}
	  
	public String getName(){
		return name;
	}
	
	public String getLoginName(){
		return loginName;
	}
	
	public long getUserId(){
		return userId;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}
}

