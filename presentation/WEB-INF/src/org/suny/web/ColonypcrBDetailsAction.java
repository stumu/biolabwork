package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ColonyPCRDetailsBean;
import org.suny.form.ColonyPCRBForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;

public class ColonypcrBDetailsAction extends Action 
{
	public ActionForward execute(
		    ActionMapping mapping,
		    ActionForm form,
		    HttpServletRequest request,
		    HttpServletResponse response) throws Exception{

		ColonyPCRBForm getForm = (ColonyPCRBForm) form;
			  String action = null;
			  try{
			       		String geneName = getForm.getBasicGeneName();
			       		System.out.println("baisc Gene: "+geneName);
			       		ColonyPCRDetailsBean p = new ColonyPCRDetailsBean();
			       		String seq = "";
			       		
			       		p.setBasicGeneName(geneName);
			       		p.setBasicVal(getForm.getBasicVal());
			       		p.setDate1(getForm.getDate1());
			       		p.setDate2(getForm.getDate1());
			       		p.setDate3(getForm.getDate3());
			       		p.setPrimer1("NA");
			       		p.setPrimer2("NA");
			       		p.setSize(getForm.getSize());
			       		
			       		if(getForm.getBy80()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy81()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy82()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy83()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy84()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy85()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy86()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy87()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy88()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy89()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy90()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy91()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy92()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy93()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy94()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy95()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy96()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy97()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy98()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getBy99()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		p.setSeq(seq);
			       		String[] temp = seq.split("[$]");
			       		p.setComments(getForm.getComments());
			       		p.setId(getForm.getBasicVal());
			    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
			    	   ColonyPCRDetailsBean result = service.saveColonyPCRCDetails(geneName,p);
			           if(result != null){
			        	   request.setAttribute("cpcrbDetails", result);
				           request.setAttribute("selectedGeneName", geneName);
				           action="success"; 
			           }
			           else{
			        	   action="failure";
			           }
				  //action="success"; 
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  return mapping.findForward(action);
		  }
}
