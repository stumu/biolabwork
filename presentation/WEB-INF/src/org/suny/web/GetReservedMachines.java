package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ValueList;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class GetReservedMachines extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  
	  String action = "failure";
	  try{
		  		   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
		  		   ValueList deviceList = service.getMachineUnderUse();
	               request.setAttribute("deviceList", deviceList);
	               ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	               ValueList machineList = service.getMachinesInfo();
	               request.setAttribute("machinesList", machineList);
	               action="success";
	       
	  }
	  catch(Exception e){
		  action = "failure";
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}