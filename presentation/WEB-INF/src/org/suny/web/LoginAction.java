package org.suny.web;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ValueList;
import org.suny.form.LoginForm;

import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class LoginAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  LoginForm loginform = (LoginForm) form;
	  String action = null;
	  try{
	       if (loginform.getUsername() != null && loginform.getPassword() != null) {
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   List list = service.getUserByLogin(loginform.getUsername().trim(),loginform.getPassword().trim());
	    	   if(list == null)
	    		   action = "failure";
	    	   else if(list.size() > 0 || list == null){
	    		   action="success";
	    		   HttpSession session = request.getSession(true);
	    		   request.setAttribute("username",loginform.getUsername());
	    		   
	    		   session.setAttribute("logged-in","true");
	    		   session.setAttribute("username",loginform.getUsername());
	    		   ValueList deviceList = service.getMachineUnderUse();
	               
	               request.setAttribute("deviceList", deviceList);
	               ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	               
	               ValueList machineList = service.getMachinesInfo();
	               request.setAttribute("machinesList", machineList);
	               ValueList geneList= new ValueList<>();
	               request.setAttribute("geneList", geneList);
	               
	    	   } 	   
	           //return mapping.findForward("success");
	       } else {
	           // not allowed
	           //return mapping.findForward("failure");
	    	   action="failure";
	       }
	       
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}