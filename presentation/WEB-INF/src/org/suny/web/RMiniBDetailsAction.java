package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.MiniADetailsBean;
import org.suny.form.RMiniBDetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class RMiniBDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  RMiniBDetailsForm getForm = (RMiniBDetailsForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		MiniADetailsBean p = new MiniADetailsBean();
	       		p.setUserdate(getForm.getUserdate());
	       		if(getForm.getScy()== null)
	       		{
	       			p.setSequence("false");
	       		}
	       		else
	       		{
	       			p.setSequence("true");
	       		}
	       		p.setColony(getForm.getColony());
	       		p.setMl(getForm.getMl());
	       		p.setComments(getForm.getComments());
	       		p.setId(getForm.getBasicVal());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   MiniADetailsBean result = service.saveMiniADetails(geneName,p);
	           if(result != null){
	        	   request.setAttribute("rminibDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}