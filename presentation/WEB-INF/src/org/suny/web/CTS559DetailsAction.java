package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BTS559DetailsBean;
import org.suny.beans.CTS559DetailsBean;
import org.suny.form.CTS559Form;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class CTS559DetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  CTS559Form getForm = (CTS559Form) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		CTS559DetailsBean p = new CTS559DetailsBean();
	       		p.setUserdate(getForm.getUserdate());
	       		p.setComments(getForm.getComments());
	       		p.setBasicVal(getForm.getBasicVal());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   CTS559DetailsBean result = service.saveBTS559Details(geneName,p);
	           if(result != null){
	        	   request.setAttribute("cts559Details", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}