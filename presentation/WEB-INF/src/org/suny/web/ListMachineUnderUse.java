package org.suny.web;

/**
 * ListDevices.java
 * 
 * Action servlet to list accessible devices.
 */

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.dao.PersistenceException;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class ListMachineUnderUse extends Action
{
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        ActionForward forward = null;
        String action=null;
        try
        {
            
            TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
            
            List deviceList = service.getMachineUnderUse();
            
            
            
            request.setAttribute("deviceList", deviceList);
            action="success";
        }
        catch (PersistenceException e)
        {
            action="error";
        	e.printStackTrace();
            
        }
        return mapping.findForward(action);
    }
}