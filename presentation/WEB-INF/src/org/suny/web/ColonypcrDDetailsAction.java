package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ColonyPCRDetailsBean;
import org.suny.form.ColonyPCRDForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;

public class ColonypcrDDetailsAction extends Action 
{
	public ActionForward execute(
		    ActionMapping mapping,
		    ActionForm form,
		    HttpServletRequest request,
		    HttpServletResponse response) throws Exception{

		ColonyPCRDForm getForm = (ColonyPCRDForm) form;
			  String action = null;
			  try{
			       		String geneName = getForm.getBasicGeneName();
			       		System.out.println("baisc Gene: "+geneName);
			       		ColonyPCRDetailsBean p = new ColonyPCRDetailsBean();
			       		String seq = "";
			       		
			       		p.setBasicGeneName(geneName);
			       		p.setBasicVal(getForm.getBasicVal());
			       		p.setDate1(getForm.getDate1());
			       		p.setDate2(getForm.getDate1());
			       		p.setDate3(getForm.getDate3());
			       		p.setPrimer1(getForm.getPrimer1());
			       		p.setPrimer2(getForm.getPrimer2());
			       		p.setSize(getForm.getSize());
			       		
			       		if(getForm.getDy80()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy81()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy82()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy83()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy84()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy85()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy86()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy87()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy88()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy89()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy90()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy91()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy92()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy93()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy94()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy95()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy96()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy97()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy98()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getDy99()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		p.setSeq(seq);
			       		String[] temp = seq.split("[$]");
			       		p.setComments(getForm.getComments());
			       		p.setId(getForm.getBasicVal());
			    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
			    	   ColonyPCRDetailsBean result = service.saveColonyPCRCDetails(geneName,p);
			           if(result != null){
			        	   request.setAttribute("cpcrdDetails", result);
				           request.setAttribute("selectedGeneName", geneName);
				           action="success"; 
			           }
			           else{
			        	   action="failure";
			           }
				  //action="success"; 
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  return mapping.findForward(action);
		  }
}
