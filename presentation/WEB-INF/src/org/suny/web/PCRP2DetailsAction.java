package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.form.PCRP2DetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class PCRP2DetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  PCRP2DetailsForm getForm = (PCRP2DetailsForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		PCR2PDetailsBean p = new PCR2PDetailsBean();
	       		p.setComments(getForm.getComments());
	       		if(getForm.getAmpliconn() == null)
	       		{
	       			p.setAmpliconn("false");
	       			p.setAmplicony("true");
	       		}
	       		else
	       		{
	       			p.setAmpliconn("true");
	       			p.setAmplicony("false");
	       		}
	       		p.setPcrdate(getForm.getPcrdate());
	       		p.setSize(getForm.getSize());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   PCR2PDetailsBean result = service.savePCR2PDetails(geneName,p);
	           if(result != null){
		           request.setAttribute("pcrDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}