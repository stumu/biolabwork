package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ValueList;
import org.suny.form.ReceiveMachineForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class DeleteUsedMachine extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  ReceiveMachineForm loginform = (ReceiveMachineForm) form;
	  String action = null;
	  try{
	       
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	           //String templateIdStr = request.getParameter("templateList");
	           String[] s = loginform.getTemplateIds();
	           if(s == null){
	        	   action="success";
	        	   ValueList deviceList = service.getMachineUnderUse();
		           request.setAttribute("deviceList", deviceList);
		           ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	               ValueList machineList = service.getMachinesInfo();
	               request.setAttribute("machinesList", machineList);
	           }
	           else{
		           System.out.println(" size is:"+s.length);
		           int result = service.deleteAssignedMachine(s);
		           if(result == 0){
			           ValueList deviceList = service.getMachineUnderUse();
			           request.setAttribute("deviceList", deviceList);
			           ValueList labList = service.getLabsInfo();
		               request.setAttribute("labName", labList);
		               ValueList machineList = service.getMachinesInfo();
		               request.setAttribute("machinesList", machineList);
			           action="success"; 
		           }
		           else{
		        	   action="failure";
		           }
	           }
	  }
	  catch(Exception e){
		  action="failure";
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}