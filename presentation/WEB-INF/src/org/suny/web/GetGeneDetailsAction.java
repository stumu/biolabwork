package org.suny.web;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.beans.ValueList;
import org.suny.form.GetMachineForm;
import org.suny.form.ReceiveMachineForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class GetGeneDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  
	  String action = null;
	  try{
	       		String geneName = request.getParameter("geneName").toString();
	       		System.out.println("selected Gene: "+geneName);
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           
	           PrimaryGeneDetailsBean result = service.getSelectedGeneDetails(geneName);
	           if(result != null){
	        	   ValueList sequenceList = service.getPrimarySequenceList(geneName);
		           request.setAttribute("geneDetails", result);
		           request.setAttribute("sequenceList", sequenceList);
		           request.setAttribute("pcrDetails", service.getPCR2PDetails(geneName));
		           request.setAttribute("rpcrDetails", service.getRPCR2PDetails(geneName));
		           request.setAttribute("ampDetails", service.getAmplificationDetails(geneName));
		           request.setAttribute("rampDetails", service.getRAmplificationDetails(geneName));
		           request.setAttribute("miniaDetails", service.getMiniADetails(geneName, "1"));
		           request.setAttribute("rminiaDetails", service.getMiniADetails(geneName, "2"));
		           request.setAttribute("minibDetails", service.getMiniADetails(geneName, "3"));
		           request.setAttribute("rminibDetails", service.getMiniADetails(geneName, "4"));
		           request.setAttribute("minicDetails", service.getMiniADetails(geneName, "5"));
		           request.setAttribute("rminicDetails", service.getMiniADetails(geneName, "6"));
		           request.setAttribute("minidDetails", service.getMiniADetails(geneName, "7"));
		           request.setAttribute("rminidDetails", service.getMiniADetails(geneName, "8"));
		           request.setAttribute("bts559Details", service.getBTS559Details(geneName, "1"));
		           request.setAttribute("btsRespotDetails", service.getBTS559Details(geneName, "2"));
		           request.setAttribute("repeatBtsRespotDetails", service.getBTS559Details(geneName, "3"));
		           request.setAttribute("repeatBts559Details", service.getBTS559Details(geneName, "4"));
		           request.setAttribute("cts559Details", service.getBTS559Details(geneName, "5"));
		           request.setAttribute("qchangeBDetails", service.getQchangeBDetails(geneName, "1"));
		           request.setAttribute("rqchangeBDetails", service.getQchangeBDetails(geneName, "2"));
		           request.setAttribute("qchangeCDetails", service.getQchangeBDetails(geneName, "3"));
		           request.setAttribute("rqchangeCDetails", service.getQchangeBDetails(geneName, "4"));
		           request.setAttribute("qchangeDDetails", service.getQchangeBDetails(geneName, "5"));
		           request.setAttribute("rqchangeDDetails", service.getQchangeBDetails(geneName, "6"));
		           request.setAttribute("cpcrcDetails", service.getColonyPCRCDetails(geneName, "1"));
		           request.setAttribute("cpcrdDetails", service.getColonyPCRCDetails(geneName, "2"));
		           request.setAttribute("cpcrbDetails", service.getColonyPCRCDetails(geneName, "3"));
		           request.setAttribute("cpcraDetails", service.getColonyPCRCDetails(geneName, "4"));
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}