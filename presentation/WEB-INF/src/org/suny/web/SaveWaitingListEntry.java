package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ValueList;
import org.suny.beans.WaitingListEntryBean;
import org.suny.form.CreateWaitingForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class SaveWaitingListEntry extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  CreateWaitingForm getForm = (CreateWaitingForm) form;
	  String action = null;
	  try{
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           String s = getForm.getPersonName();
	           System.out.println(" person is:"+s );
	           System.out.println(" Get machine action called ******************* \n" + request.getParameter("personName"));
	           WaitingListEntryBean b = new WaitingListEntryBean();
	           b.setBorrowdDate(getForm.getDatetaking());
	           b.setLabName(getForm.getLabName());
	           b.setPersonName(getForm.getPersonName());
	           b.setReleaseDate(getForm.getReleaseDate());
	           b.setEmail(getForm.getEmail());
	           b.setPhone(getForm.getPhone());
	           int result = service.saveWaitingListEntry(b);
	           if(result == 0){
	        	   ValueList deviceList = service.getWaitingList();
	               request.setAttribute("deviceList", deviceList);
		           ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	              
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}