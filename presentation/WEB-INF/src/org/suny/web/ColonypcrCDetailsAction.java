package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ColonyPCRDetailsBean;
import org.suny.form.ColonyPCRCForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;

public class ColonypcrCDetailsAction extends Action 
{
	public ActionForward execute(
		    ActionMapping mapping,
		    ActionForm form,
		    HttpServletRequest request,
		    HttpServletResponse response) throws Exception{

		ColonyPCRCForm getForm = (ColonyPCRCForm) form;
			  String action = null;
			  try{
			       		String geneName = getForm.getBasicGeneName();
			       		System.out.println("baisc Gene: "+geneName);
			       		ColonyPCRDetailsBean p = new ColonyPCRDetailsBean();
			       		String seq = "";
			       		
			       		p.setBasicGeneName(geneName);
			       		p.setBasicVal(getForm.getBasicVal());
			       		p.setDate1(getForm.getDate1());
			       		p.setDate2(getForm.getDate1());
			       		p.setDate3(getForm.getDate3());
			       		p.setPrimer1(getForm.getPrimer1());
			       		p.setPrimer2(getForm.getPrimer2());
			       		p.setSize(getForm.getSize());
			       		
			       		if(getForm.getY80()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY81()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY82()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY83()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY84()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY85()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY86()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY87()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY88()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY89()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY90()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY91()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY92()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY93()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY94()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY95()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY96()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY97()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY98()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		seq+="$";
			       		if(getForm.getY99()!= null)
			       		{
			       			seq+="1";
			       		}
			       		else
			       		{
			       			seq+="0";
			       		}
			       		p.setSeq(seq);
			       		String[] temp = seq.split("[$]");
			       		p.setComments(getForm.getComments());
			       		p.setId(getForm.getBasicVal());
			    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
			    	   ColonyPCRDetailsBean result = service.saveColonyPCRCDetails(geneName,p);
			           if(result != null){
			        	   request.setAttribute("cpcrcDetails", result);
				           request.setAttribute("selectedGeneName", geneName);
				           action="success"; 
			           }
			           else{
			        	   action="failure";
			           }
				  //action="success"; 
			  }
			  catch(Exception e){
				  e.printStackTrace();
			  }
			  return mapping.findForward(action);
		  }
}
