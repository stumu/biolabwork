package org.suny.web;


import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.ValueList;
import org.suny.form.GetMachineForm;
import org.suny.form.ReceiveMachineForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class GetMachineAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  GetMachineForm getForm = (GetMachineForm) form;
	  String action = null;
	  try{
	       
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           String s = getForm.getPersonName();
	           System.out.println(" person is:"+s );
	           System.out.println(" Get machine action called ******************* \n" + request.getParameter("personName"));
	           BusyMachinesBean b = new BusyMachinesBean();
	           b.setBorrowdDate(getForm.getDatetaking());
	           b.setLabName(getForm.getLabName());
	           b.setMachineName(getForm.getMachineName());
	           b.setPersonName(getForm.getPersonName());
	           b.setReleaseDate(getForm.getReleaseDate());
	           b.setPhone(getForm.getPhone());
	           b.setEmail(getForm.getEmail());
	           int result = service.saveAssignedMachine(b);
	           if(result == 0){
		           ValueList deviceList = service.getMachineUnderUse();
		           request.setAttribute("deviceList", deviceList);
		           ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	               ValueList machineList = service.getMachinesInfo();
	               request.setAttribute("machinesList", machineList);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}