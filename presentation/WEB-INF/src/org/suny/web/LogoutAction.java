package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


public class LogoutAction extends Action
{
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
        throws Exception
    {
        
        //create the session
        HttpSession session = request.getSession(false);
        if (session != null)
        {
        	session.removeAttribute("logged-in");
  	    	System.out.println(" logout action called: ");
  	    	session.invalidate();
        }
        
        return mapping.findForward("success");
    }
}