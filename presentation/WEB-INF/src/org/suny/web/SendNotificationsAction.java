package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.ValueList;
import org.suny.form.GetMachineForm;
import org.suny.form.SendNotificationsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class SendNotificationsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  SendNotificationsForm getForm = (SendNotificationsForm) form;
	  String action = null;
	  try{
	       
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           String s = getForm.getDescription();
	           System.out.println(" message is:"+s );
	           
	           int result = service.sendEmailNotifications(s);
	           if(result == 0){
		           ValueList deviceList = service.getMachineUnderUse();
		           request.setAttribute("deviceList", deviceList);
		           ValueList labList = service.getLabsInfo();
	               request.setAttribute("labName", labList);
	               ValueList machineList = service.getMachinesInfo();
	               request.setAttribute("machinesList", machineList);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}