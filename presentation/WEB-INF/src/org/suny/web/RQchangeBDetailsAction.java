package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.QuickChangeDetailsBean;
import org.suny.form.RQchangeBDetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class RQchangeBDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  RQchangeBDetailsForm getForm = (RQchangeBDetailsForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		QuickChangeDetailsBean p = new QuickChangeDetailsBean();
	       		p.setUserdate(getForm.getUserdate());
	       		p.setComments(getForm.getComments());
	       		p.setId(getForm.getBasicVal());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   QuickChangeDetailsBean result = service.saveQchangeBDetails(geneName,p);
	           if(result != null){
	        	   request.setAttribute("rqchangeBDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}