package org.suny.web;


import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BusyMachinesBean;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.beans.ValueList;
import org.suny.form.GetMachineForm;
import org.suny.form.ReceiveMachineForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class GetPCRDetailsHistoryAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  
	  String action = null;
	  try{
	       		String geneName = request.getParameter("geneName").toString();
	       		System.out.println("selected Gene: "+geneName);
	       		String id = request.getParameter("id").toString();
	           // we are in
	       		System.out.println("selected Gene id: "+id);
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	           PCR2PDetailsBean result = service.getPCR2PDetailsHistory(geneName,id);
	           if(result != null){
		           request.setAttribute("pcrDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           request.setAttribute("id", result.getId());
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}