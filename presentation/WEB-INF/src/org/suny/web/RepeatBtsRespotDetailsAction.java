package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.BTS559DetailsBean;
import org.suny.form.RepeatBtsRespotActionForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class RepeatBtsRespotDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  RepeatBtsRespotActionForm getForm = (RepeatBtsRespotActionForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		BTS559DetailsBean p = new BTS559DetailsBean();
	       		p.setUserdate(getForm.getUserdate());
	       		p.setComments(getForm.getComments());
	       		p.setBasicVal(getForm.getBasicVal());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   BTS559DetailsBean result = service.saveBTS559Details(geneName,p);
	           if(result != null){
	        	   request.setAttribute("repeatBtsRespotDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}