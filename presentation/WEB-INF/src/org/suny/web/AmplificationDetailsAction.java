package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.AmplificationDetailsBean;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.form.AmplificationForm;
import org.suny.form.PCRP2DetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class AmplificationDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  AmplificationForm getForm = (AmplificationForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		AmplificationDetailsBean p = new AmplificationDetailsBean();
	       		p.setComments(getForm.getComments());
	       		p.setExcess(getForm.getExcess());
	       		p.setGene(geneName);
	       		p.setMl(getForm.getMl());
	       		p.setPosition(getForm.getPosition());
	       		p.setStore(getForm.getStore());
	       		p.setUserdate(getForm.getUserdate());
	       		p.setVolume(getForm.getVolume());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   AmplificationDetailsBean result = null;
	    	   if(geneName != null)
	    		   result =  service.saveAmplificationDetails(geneName,p);
	    	   else{
	    		   geneName = request.getParameter("geneName").toString();
		       		System.out.println("selected Gene: "+geneName);
		       		String id = "0";
		       		if(request.getParameter("oid") != null)
		       			id = request.getParameter("oid").toString();
		       		if(id.equals("null"))
		       			id="0";
		       		int oid = Integer.parseInt(id);
		       		if(oid > 0 )
		       		{
		       			String direc = "1";
		       			if(request.getParameter("direc") !=null)
		       				direc = request.getParameter("direc").toString();
		       			if(direc.equals("0"))
		       				oid= oid - 1;
		       			else if(direc.equals("1"))
		       				oid= oid+ 1;
		       		}
		       		result = service.getAmplificationDetailsHistory(geneName,oid+"");
		       		request.setAttribute("oid", result.getId()+"");
	    	   }	   
	           if(result != null){
		           request.setAttribute("ampDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}