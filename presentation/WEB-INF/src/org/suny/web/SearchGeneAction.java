package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.SearchGeneBean;
import org.suny.beans.ValueList;
import org.suny.form.SearchGeneForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class SearchGeneAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  SearchGeneForm getForm = (SearchGeneForm) form;
	  String action = null;
	  try{
	       
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           String s = getForm.getGeneName();
	           System.out.println(" gene is:"+s );
	           System.out.println(" Get machine action called ******************* \n" + request.getParameter("geneName"));
	           SearchGeneBean b = new SearchGeneBean();
	           b.setGeneName(s);
	           ValueList geneList= service.searchGene(b);
	           if(geneList != null){
		           request.setAttribute("geneList", geneList);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}