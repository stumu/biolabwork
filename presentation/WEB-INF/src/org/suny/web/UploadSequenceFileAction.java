package org.suny.web;


import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.ValueList;
import org.suny.form.SequenceFileUploadForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class UploadSequenceFileAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  SequenceFileUploadForm getForm = (SequenceFileUploadForm) form;
	  String action = null;
	  try{
	       
	           // we are in
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   String file = getForm.getFileSequence();
	    	   
	    	   if(!file.equals("")){  
	    		   System.out.println("Server path:" +file);
	    		   ArrayList beanList =    ParseSequenceCSV.parseSequenceCSVFile("C:/OSU/Workbench/Tom/"+file);
	    		   //Create file
	    		   service.uploadSequences(beanList);
	    		   getForm.setSequenceFile(null);	
	    		   request.setAttribute("erMsg", "File successfully uploaded");
		    	   action="success";    	
	    		}
	           else{
	        	   action="failure";
	           }

	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}