package org.suny.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.beans.SearchGeneBean;
import org.suny.beans.ValueList;
import org.suny.form.BasicGeneDetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class BasicGeneDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  BasicGeneDetailsForm getForm = (BasicGeneDetailsForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		PrimaryGeneDetailsBean p = new PrimaryGeneDetailsBean();
	       		p.setComments(getForm.getGeneDetails());
	       		if(getForm.getCtoverlapn() == null)
	       			p.setCtermN("false");
	       		else
	       			p.setCtermN("true");
	       		if(getForm.getCtoverlapy() == null)
	       			p.setCtermY("false");
	       		else
	       			p.setCtermY("true");
	       		p.setGeneLength(Integer.parseInt(getForm.getGeneLength()));
	       		p.setGeneLocationEnd(getForm.getGeneLocationEnd());
	       		p.setGeneLocationStart(getForm.getGeneLocationStart());
	       		if(getForm.getNstrand() == null)
	       			p.setNegativeStrand("false");
	       		else
	       			p.setNegativeStrand("true");
	       		if(getForm.getNtoverlapn() == null)
	       			p.setNtermN("false");
	       		else
	       			p.setNtermN("true");
	       		if(getForm.getNtoverlapy() == null)
	       			p.setNtermY("false");
	       		else
	       			p.setNtermY("true");
	       		if(getForm.getPstrand() == null)
	       			p.setPositiveStrand("false");
	       		else
	       			p.setPositiveStrand("true");
	       		p.setPrimaryGeneName(geneName);
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   
	           
	           PrimaryGeneDetailsBean result = service.saveBasicGeneDetails(geneName,p);
	           if(result != null){
		           request.setAttribute("geneDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}