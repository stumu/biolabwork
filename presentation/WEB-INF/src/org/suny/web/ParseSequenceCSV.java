package org.suny.web;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.suny.beans.PrimerSequenceDataBean;

public class ParseSequenceCSV {
	
	public static ArrayList parseSequenceCSVFile(String file){
		ArrayList beanList = null;
		try{
			  // Open the file that is the first 
			  // command line parameter
			  //FileInputStream fstream = new FileInputStream(file);
			  // Get the object of DataInputStream
			  FileInputStream fstream = new FileInputStream(file);
			  DataInputStream in = new DataInputStream(fstream);
			  BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  String strLine;
			  //Read File Line By Line
			  String header =  br.readLine();
			  System.out.println(header);
			  String[] hdColArr = Constants.sequenceColumns;
			  int [] colIndexArr = new int[hdColArr.length];
			  String[] hdStrArr = header.split(",");
			  for(int i=0;i<hdColArr.length;i++)
			  {
				  for(int j=0;j<hdStrArr.length;j++)
				  {
					  if(hdColArr[i].equalsIgnoreCase(hdStrArr[j]))
					  {
						  colIndexArr[i] = j;
						  break;
					  }
				  }
			  }
			  beanList = new ArrayList();
			  String[] dataArr = null;
			  PrimerSequenceDataBean pr = null;
			  while ((strLine = br.readLine()) != null)   {
				  // Print the content on the console
				  System.out.println (strLine);
				  dataArr = strLine.split(",");
				  pr  = new PrimerSequenceDataBean();
				  pr.setGene(dataArr[colIndexArr[0]]);
				  pr.setPrimer(dataArr[colIndexArr[1]]);
				  pr.setPrimerSequence(dataArr[colIndexArr[2]]);
				  pr.setStart(dataArr[colIndexArr[3]]);
				  pr.setEnd(dataArr[colIndexArr[4]]);
				  pr.setPlate(dataArr[colIndexArr[5]]);
				  pr.setWell_Position(dataArr[colIndexArr[6]]);
				  beanList.add(pr);
			  }
			  //Close the input stream
			  in.close();
		    }catch (Exception e){//Catch exception if any
		    	//System.err.println("Error: " + e.getMessage());
		    	beanList = null;
		    	e.printStackTrace();
		  }
		return beanList;
	}
	
	public static void main(String args[])
	{
		//String file = "C:/OSU/Workbench/Tom/112111_Cubonova_sequences.csv";
		//File f = new File(file);
		//ParseSequenceCSV p = new ParseSequenceCSV(file);
		String gene="TK0001";
		System.out.println(gene.substring(2));
	}
}
