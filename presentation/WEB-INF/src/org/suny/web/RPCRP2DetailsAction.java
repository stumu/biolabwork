package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.form.RPCRP2DetailsForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class RPCRP2DetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  RPCRP2DetailsForm getForm = (RPCRP2DetailsForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		PCR2PDetailsBean p = new PCR2PDetailsBean();
	       		p.setComments(getForm.getComments());
	       		if(getForm.getRampliconn() == null)
	       		{
	       			p.setAmpliconn("false");
	       			p.setAmplicony("true");
	       		}
	       		else
	       		{
	       			p.setAmpliconn("true");
	       			p.setAmplicony("false");
	       		}
	       		p.setPcrdate(getForm.getPcrdate());
	       		p.setSize(getForm.getSize());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   PCR2PDetailsBean result = null;
	    	   if(geneName != null)
	    		   result = service.saveRPCR2PDetails(geneName,p);
	    	   else{
	    		   geneName = request.getParameter("geneName").toString();
		       		System.out.println("selected Gene: "+geneName);
		       		String id = "0";
		       		if(request.getParameter("oid") != null)
		       			id = request.getParameter("oid").toString();
		       		
		       		int oid = Integer.parseInt(id);
		       		if(oid > 0 )
		       		{
		       			String direc = "1";
		       			if(request.getParameter("direc") !=null)
		       				direc = request.getParameter("direc").toString();
		       			if(direc.equals("0"))
		       				oid= oid - 1;
		       			else if(direc.equals("1"))
		       				oid= oid+ 1;
		       		}
		       		result = service.getRPCR2PDetailsHistory(geneName,oid+"");
		       		request.setAttribute("oid", result.getId()+"");
	    	   }
	           if(result != null){
		           request.setAttribute("rpcrDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}