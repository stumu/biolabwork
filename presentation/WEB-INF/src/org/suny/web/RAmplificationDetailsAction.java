package org.suny.web;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.suny.beans.AmplificationDetailsBean;
import org.suny.beans.PCR2PDetailsBean;
import org.suny.beans.PrimaryGeneDetailsBean;
import org.suny.form.AmplificationForm;
import org.suny.form.PCRP2DetailsForm;
import org.suny.form.RAmplificationForm;
import org.suny.service.ServiceFactory;
import org.suny.service.TaskMangementService;


public class RAmplificationDetailsAction extends Action
{
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm form,
    HttpServletRequest request,
    HttpServletResponse response) throws Exception{

	  RAmplificationForm getForm = (RAmplificationForm) form;
	  String action = null;
	  try{
	       		String geneName = getForm.getBasicGeneName();
	       		System.out.println("baisc Gene: "+geneName);
	           // we are in
	       		AmplificationDetailsBean p = new AmplificationDetailsBean();
	       		p.setComments(getForm.getComments());
	       		p.setExcess(getForm.getExcess());
	       		p.setGene(geneName);
	       		p.setMl(getForm.getMl());
	       		p.setPosition(getForm.getPosition());
	       		p.setStore(getForm.getStore());
	       		p.setUserdate(getForm.getUserdate());
	       		p.setVolume(getForm.getVolume());
	    	   TaskMangementService service  = (TaskMangementService) ServiceFactory.getInstance().getService();//new EmpBean();
	    	   AmplificationDetailsBean result = service.saveRAmplificationDetails(geneName,p);
	           if(result != null){
		           request.setAttribute("rampDetails", result);
		           request.setAttribute("selectedGeneName", geneName);
		           action="success"; 
	           }
	           else{
	        	   action="failure";
	           }
		  //action="success"; 
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
	  return mapping.findForward(action);
  }
}