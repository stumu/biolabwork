package org.suny.form;

import org.apache.struts.action.ActionForm;


public class BasicGeneDetailsForm extends ActionForm
{
	  private String basicGeneName  = null;
      public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	private String geneLength=null;
      private String geneLocationStart=null;
      private String geneLocationEnd=null;
	  public String getGeneLocationEnd() {
		return geneLocationEnd;
	}
	public void setGeneLocationEnd(String geneLocationEnd) {
		this.geneLocationEnd = geneLocationEnd;
	}
	private String nstrand = null;
	  private String pstrand = null;
	  private String ntoverlapy = null;
	  private String ntoverlapn = null;
	  private String ctoverlapy = null;
	  private String ctoverlapn = null;
	  private String geneDetails = null;
	public String getGeneLength() {
		return geneLength;
	}
	public void setGeneLength(String geneLength) {
		this.geneLength = geneLength;
	}
	public String getGeneLocationStart() {
		return geneLocationStart;
	}
	public void setGeneLocationStart(String geneLocationStart) {
		this.geneLocationStart = geneLocationStart;
	}
	
	public String getNstrand() {
		return nstrand;
	}
	public void setNstrand(String nstrand) {
		this.nstrand = nstrand;
	}
	public String getPstrand() {
		return pstrand;
	}
	public void setPstrand(String pstrand) {
		this.pstrand = pstrand;
	}
	public String getNtoverlapy() {
		return ntoverlapy;
	}
	public void setNtoverlapy(String ntoverlapy) {
		this.ntoverlapy = ntoverlapy;
	}
	public String getNtoverlapn() {
		return ntoverlapn;
	}
	public void setNtoverlapn(String ntoverlapn) {
		this.ntoverlapn = ntoverlapn;
	}
	public String getCtoverlapy() {
		return ctoverlapy;
	}
	public void setCtoverlapy(String ctoverlapy) {
		this.ctoverlapy = ctoverlapy;
	}
	public String getCtoverlapn() {
		return ctoverlapn;
	}
	public void setCtoverlapn(String ctoverlapn) {
		this.ctoverlapn = ctoverlapn;
	}
	public String getGeneDetails() {
		return geneDetails;
	}
	public void setGeneDetails(String geneDetails) {
		this.geneDetails = geneDetails;
	}
	  
} 