package org.suny.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.*;


public class LoginForm extends ActionForm
{
	 
      private String action="add";

	
	  private String username=null;
	  private String password=null;
	 
	 




      public void setAction(String action){
		this.action=action;
	  }

	  public String getAction(){
		return this.action;
	  }

	 


	  public void setUsername(String username){
		this.username=username;
	  }

	  public String getUsername(){
		return this.username;
	  }



	   public void setPassword(String password){
		this.password=password;
	  }

	  public String getPassword(){
		return this.password;
	  }
      



	   
	  

	  public void reset(ActionMapping mapping,HttpServletRequest request){
		  
		  
		  this.username=null;
		  this.password=null;
		  
		  this.action="add";

	  }


      public ActionErrors validate( 
      ActionMapping mapping, HttpServletRequest request ) {
      ActionErrors errors = new ActionErrors();
      
      if( getUsername() == null || getUsername().length() < 1 ) {
        errors.add("username",new ActionMessage("error.username.required"));
      }
      if( getPassword() == null || getPassword().length() < 1 ) {
        errors.add("password",new ActionMessage("error.password.required"));
      }
      

      return errors;
  } 
    


 
} 