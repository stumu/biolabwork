package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;

public class ColonyPCRDForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private String id;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String dy80;
	private String dn80;
	private String dy81;
	private String dn81;
	private String dy82;
	private String dn82;
	private String dy83;
	private String dn83;
	private String dy84;
	private String dn84;
	private String dy85;
	private String dn85;
	private String dy86;
	private String dn86;
	private String dy87;
	private String dn87;
	private String dy88;
	private String dn88;
	private String dy89;
	private String dn89;
	
	private String dy90;
	private String dn90;
	private String dy91;
	private String dn91;
	private String dy92;
	private String dn92;
	private String dy93;
	private String dn93;
	private String dy94;
	private String dn94;
	private String dy95;
	private String dn95;
	private String dy96;
	private String dn96;
	private String dy97;
	private String dn97;
	private String dy98;
	private String dn98;
	private String dy99;
	private String dn99;
	
	public String getDy80() {
		return dy80;
	}
	public void setDy80(String dy80) {
		this.dy80 = dy80;
	}
	public String getDn80() {
		return dn80;
	}
	public void setDn80(String dn80) {
		this.dn80 = dn80;
	}
	public String getDy81() {
		return dy81;
	}
	public void setDy81(String dy81) {
		this.dy81 = dy81;
	}
	public String getDn81() {
		return dn81;
	}
	public void setDn81(String dn81) {
		this.dn81 = dn81;
	}
	public String getDy82() {
		return dy82;
	}
	public void setDy82(String dy82) {
		this.dy82 = dy82;
	}
	public String getDn82() {
		return dn82;
	}
	public void setDn82(String dn82) {
		this.dn82 = dn82;
	}
	public String getDy83() {
		return dy83;
	}
	public void setDy83(String dy83) {
		this.dy83 = dy83;
	}
	public String getDn83() {
		return dn83;
	}
	public void setDn83(String dn83) {
		this.dn83 = dn83;
	}
	public String getDy84() {
		return dy84;
	}
	public void setDy84(String dy84) {
		this.dy84 = dy84;
	}
	public String getDn84() {
		return dn84;
	}
	public void setDn84(String dn84) {
		this.dn84 = dn84;
	}
	public String getDy85() {
		return dy85;
	}
	public void setDy85(String dy85) {
		this.dy85 = dy85;
	}
	public String getDn85() {
		return dn85;
	}
	public void setDn85(String dn85) {
		this.dn85 = dn85;
	}
	public String getDy86() {
		return dy86;
	}
	public void setDy86(String dy86) {
		this.dy86 = dy86;
	}
	public String getDn86() {
		return dn86;
	}
	public void setDn86(String dn86) {
		this.dn86 = dn86;
	}
	public String getDy87() {
		return dy87;
	}
	public void setDy87(String dy87) {
		this.dy87 = dy87;
	}
	public String getDn87() {
		return dn87;
	}
	public void setDn87(String dn87) {
		this.dn87 = dn87;
	}
	public String getDy88() {
		return dy88;
	}
	public void setDy88(String dy88) {
		this.dy88 = dy88;
	}
	public String getDn88() {
		return dn88;
	}
	public void setDn88(String dn88) {
		this.dn88 = dn88;
	}
	public String getDy89() {
		return dy89;
	}
	public void setDy89(String dy89) {
		this.dy89 = dy89;
	}
	public String getDn89() {
		return dn89;
	}
	public void setDn89(String dn89) {
		this.dn89 = dn89;
	}
	public String getDy90() {
		return dy90;
	}
	public void setDy90(String dy90) {
		this.dy90 = dy90;
	}
	public String getDn90() {
		return dn90;
	}
	public void setDn90(String dn90) {
		this.dn90 = dn90;
	}
	public String getDy91() {
		return dy91;
	}
	public void setDy91(String dy91) {
		this.dy91 = dy91;
	}
	public String getDn91() {
		return dn91;
	}
	public void setDn91(String dn91) {
		this.dn91 = dn91;
	}
	public String getDy92() {
		return dy92;
	}
	public void setDy92(String dy92) {
		this.dy92 = dy92;
	}
	public String getDn92() {
		return dn92;
	}
	public void setDn92(String dn92) {
		this.dn92 = dn92;
	}
	public String getDy93() {
		return dy93;
	}
	public void setDy93(String dy93) {
		this.dy93 = dy93;
	}
	public String getDn93() {
		return dn93;
	}
	public void setDn93(String dn93) {
		this.dn93 = dn93;
	}
	public String getDy94() {
		return dy94;
	}
	public void setDy94(String dy94) {
		this.dy94 = dy94;
	}
	public String getDn94() {
		return dn94;
	}
	public void setDn94(String dn94) {
		this.dn94 = dn94;
	}
	public String getDy95() {
		return dy95;
	}
	public void setDy95(String dy95) {
		this.dy95 = dy95;
	}
	public String getDn95() {
		return dn95;
	}
	public void setDn95(String dn95) {
		this.dn95 = dn95;
	}
	public String getDy96() {
		return dy96;
	}
	public void setDy96(String dy96) {
		this.dy96 = dy96;
	}
	public String getDn96() {
		return dn96;
	}
	public void setDn96(String dn96) {
		this.dn96 = dn96;
	}
	public String getDy97() {
		return dy97;
	}
	public void setDy97(String dy97) {
		this.dy97 = dy97;
	}
	public String getDn97() {
		return dn97;
	}
	public void setDn97(String dn97) {
		this.dn97 = dn97;
	}
	public String getDy98() {
		return dy98;
	}
	public void setDy98(String dy98) {
		this.dy98 = dy98;
	}
	public String getDn98() {
		return dn98;
	}
	public void setDn98(String dn98) {
		this.dn98 = dn98;
	}
	public String getDy99() {
		return dy99;
	}
	public void setDy99(String dy99) {
		this.dy99 = dy99;
	}
	public String getDn99() {
		return dn99;
	}
	public void setDn99(String dn99) {
		this.dn99 = dn99;
	}
	private String comments;
	private String datecreated;
	private Timestamp datemodified;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrimer1() {
		return primer1;
	}
	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}
	public String getPrimer2() {
		return primer2;
	}
	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	
}
