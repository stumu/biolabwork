package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;
import org.suny.model.MiniaId;

public class MiniADetailsForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private String scy=null;
	private String scn=null;
	
	public String getScy() {
		return scy;
	}
	public void setScy(String scy) {
		this.scy = scy;
	}
	public String getScn() {
		return scn;
	}
	public void setScn(String scn) {
		this.scn = scn;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	private MiniaId id;
	private String ml;
	private String comments;
	private String user;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public MiniaId getId() {
		return id;
	}
	public void setId(MiniaId id) {
		this.id = id;
	}
	public String getMl() {
		return ml;
	}
	public void setMl(String ml) {
		this.ml = ml;
	}
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getColony() {
		return colony;
	}
	public void setColony(String colony) {
		this.colony = colony;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
	private String colony;
	private String userdate;

}
