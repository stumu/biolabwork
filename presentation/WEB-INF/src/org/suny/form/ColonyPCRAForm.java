package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;

public class ColonyPCRAForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private String id;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String ay80;
	private String an80;
	private String ay81;
	private String an81;
	private String ay82;
	private String an82;
	private String ay83;
	private String an83;
	private String ay84;
	private String an84;
	private String ay85;
	private String an85;
	private String ay86;
	private String an86;
	private String ay87;
	private String an87;
	private String ay88;
	private String an88;
	private String ay89;
	private String an89;
	
	private String ay90;
	private String an90;
	private String ay91;
	private String an91;
	private String ay92;
	private String an92;
	private String ay93;
	private String an93;
	private String ay94;
	private String an94;
	private String ay95;
	private String an95;
	private String ay96;
	private String an96;
	private String ay97;
	private String an97;
	private String ay98;
	private String an98;
	private String ay99;
	private String an99;
	
	private String ay50;
	private String an50;
	private String ay51;
	private String an51;
	private String ay52;
	private String an52;
	private String ay53;
	private String an53;
	private String ay54;
	private String an54;
	private String ay55;
	private String an55;
	private String ay56;
	private String an56;
	private String ay57;
	private String an57;
	private String ay58;
	private String an58;
	private String ay59;
	private String an59;

	
	public String getAy80() {
		return ay80;
	}
	public void setAy80(String ay80) {
		this.ay80 = ay80;
	}
	public String getAn80() {
		return an80;
	}
	public void setAn80(String an80) {
		this.an80 = an80;
	}
	public String getAy81() {
		return ay81;
	}
	public void setAy81(String ay81) {
		this.ay81 = ay81;
	}
	public String getAn81() {
		return an81;
	}
	public void setAn81(String an81) {
		this.an81 = an81;
	}
	public String getAy82() {
		return ay82;
	}
	public void setAy82(String ay82) {
		this.ay82 = ay82;
	}
	public String getAn82() {
		return an82;
	}
	public void setAn82(String an82) {
		this.an82 = an82;
	}
	public String getAy83() {
		return ay83;
	}
	public void setAy83(String ay83) {
		this.ay83 = ay83;
	}
	public String getAn83() {
		return an83;
	}
	public void setAn83(String an83) {
		this.an83 = an83;
	}
	public String getAy84() {
		return ay84;
	}
	public void setAy84(String ay84) {
		this.ay84 = ay84;
	}
	public String getAn84() {
		return an84;
	}
	public void setAn84(String an84) {
		this.an84 = an84;
	}
	public String getAy85() {
		return ay85;
	}
	public void setAy85(String ay85) {
		this.ay85 = ay85;
	}
	public String getAn85() {
		return an85;
	}
	public void setAn85(String an85) {
		this.an85 = an85;
	}
	public String getAy86() {
		return ay86;
	}
	public void setAy86(String ay86) {
		this.ay86 = ay86;
	}
	public String getAn86() {
		return an86;
	}
	public void setAn86(String an86) {
		this.an86 = an86;
	}
	public String getAy87() {
		return ay87;
	}
	public void setAy87(String ay87) {
		this.ay87 = ay87;
	}
	public String getAn87() {
		return an87;
	}
	public void setAn87(String an87) {
		this.an87 = an87;
	}
	public String getAy88() {
		return ay88;
	}
	public void setAy88(String ay88) {
		this.ay88 = ay88;
	}
	public String getAn88() {
		return an88;
	}
	public void setAn88(String an88) {
		this.an88 = an88;
	}
	public String getAy89() {
		return ay89;
	}
	public void setAy89(String ay89) {
		this.ay89 = ay89;
	}
	public String getAn89() {
		return an89;
	}
	public void setAn89(String an89) {
		this.an89 = an89;
	}
	public String getAy90() {
		return ay90;
	}
	public void setAy90(String ay90) {
		this.ay90 = ay90;
	}
	public String getAn90() {
		return an90;
	}
	public void setAn90(String an90) {
		this.an90 = an90;
	}
	public String getAy91() {
		return ay91;
	}
	public void setAy91(String ay91) {
		this.ay91 = ay91;
	}
	public String getAn91() {
		return an91;
	}
	public void setAn91(String an91) {
		this.an91 = an91;
	}
	public String getAy92() {
		return ay92;
	}
	public void setAy92(String ay92) {
		this.ay92 = ay92;
	}
	public String getAn92() {
		return an92;
	}
	public void setAn92(String an92) {
		this.an92 = an92;
	}
	public String getAy93() {
		return ay93;
	}
	public void setAy93(String ay93) {
		this.ay93 = ay93;
	}
	public String getAn93() {
		return an93;
	}
	public void setAn93(String an93) {
		this.an93 = an93;
	}
	public String getAy94() {
		return ay94;
	}
	public void setAy94(String ay94) {
		this.ay94 = ay94;
	}
	public String getAn94() {
		return an94;
	}
	public void setAn94(String an94) {
		this.an94 = an94;
	}
	public String getAy95() {
		return ay95;
	}
	public void setAy95(String ay95) {
		this.ay95 = ay95;
	}
	public String getAn95() {
		return an95;
	}
	public void setAn95(String an95) {
		this.an95 = an95;
	}
	public String getAy96() {
		return ay96;
	}
	public void setAy96(String ay96) {
		this.ay96 = ay96;
	}
	public String getAn96() {
		return an96;
	}
	public void setAn96(String an96) {
		this.an96 = an96;
	}
	public String getAy97() {
		return ay97;
	}
	public void setAy97(String ay97) {
		this.ay97 = ay97;
	}
	public String getAn97() {
		return an97;
	}
	public void setAn97(String an97) {
		this.an97 = an97;
	}
	public String getAy98() {
		return ay98;
	}
	public void setAy98(String ay98) {
		this.ay98 = ay98;
	}
	public String getAn98() {
		return an98;
	}
	public void setAn98(String an98) {
		this.an98 = an98;
	}
	public String getAy99() {
		return ay99;
	}
	public void setAy99(String ay99) {
		this.ay99 = ay99;
	}
	public String getAn99() {
		return an99;
	}
	public void setAn99(String an99) {
		this.an99 = an99;
	}
	public String getAy50() {
		return ay50;
	}
	public void setAy50(String ay50) {
		this.ay50 = ay50;
	}
	public String getAn50() {
		return an50;
	}
	public void setAn50(String an50) {
		this.an50 = an50;
	}
	public String getAy51() {
		return ay51;
	}
	public void setAy51(String ay51) {
		this.ay51 = ay51;
	}
	public String getAn51() {
		return an51;
	}
	public void setAn51(String an51) {
		this.an51 = an51;
	}
	public String getAy52() {
		return ay52;
	}
	public void setAy52(String ay52) {
		this.ay52 = ay52;
	}
	public String getAn52() {
		return an52;
	}
	public void setAn52(String an52) {
		this.an52 = an52;
	}
	public String getAy53() {
		return ay53;
	}
	public void setAy53(String ay53) {
		this.ay53 = ay53;
	}
	public String getAn53() {
		return an53;
	}
	public void setAn53(String an53) {
		this.an53 = an53;
	}
	public String getAy54() {
		return ay54;
	}
	public void setAy54(String ay54) {
		this.ay54 = ay54;
	}
	public String getAn54() {
		return an54;
	}
	public void setAn54(String an54) {
		this.an54 = an54;
	}
	public String getAy55() {
		return ay55;
	}
	public void setAy55(String ay55) {
		this.ay55 = ay55;
	}
	public String getAn55() {
		return an55;
	}
	public void setAn55(String an55) {
		this.an55 = an55;
	}
	public String getAy56() {
		return ay56;
	}
	public void setAy56(String ay56) {
		this.ay56 = ay56;
	}
	public String getAn56() {
		return an56;
	}
	public void setAn56(String an56) {
		this.an56 = an56;
	}
	public String getAy57() {
		return ay57;
	}
	public void setAy57(String ay57) {
		this.ay57 = ay57;
	}
	public String getAn57() {
		return an57;
	}
	public void setAn57(String an57) {
		this.an57 = an57;
	}
	public String getAy58() {
		return ay58;
	}
	public void setAy58(String ay58) {
		this.ay58 = ay58;
	}
	public String getAn58() {
		return an58;
	}
	public void setAn58(String an58) {
		this.an58 = an58;
	}
	public String getAy59() {
		return ay59;
	}
	public void setAy59(String ay59) {
		this.ay59 = ay59;
	}
	public String getAn59() {
		return an59;
	}
	public void setAn59(String an59) {
		this.an59 = an59;
	}
	private String comments;
	private String datecreated;
	private Timestamp datemodified;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrimer1() {
		return primer1;
	}
	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}
	public String getPrimer2() {
		return primer2;
	}
	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	
}
