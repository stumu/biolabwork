package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;
import org.suny.model.ColonypcrId;

public class ColonyPCRCForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private String id;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String y80;
	private String n80;
	private String y81;
	private String n81;
	private String y82;
	private String n82;
	private String y83;
	private String n83;
	private String y84;
	private String n84;
	private String y85;
	private String n85;
	private String y86;
	private String n86;
	private String y87;
	private String n87;
	private String y88;
	private String n88;
	private String y89;
	private String n89;
	
	private String y90;
	private String n90;
	private String y91;
	private String n91;
	private String y92;
	private String n92;
	private String y93;
	private String n93;
	private String y94;
	private String n94;
	private String y95;
	private String n95;
	private String y96;
	private String n96;
	private String y97;
	private String n97;
	private String y98;
	private String n98;
	private String y99;
	private String n99;
	
	public String getY80() {
		return y80;
	}
	public void setY80(String y80) {
		this.y80 = y80;
	}
	public String getN80() {
		return n80;
	}
	public void setN80(String n80) {
		this.n80 = n80;
	}
	public String getY81() {
		return y81;
	}
	public void setY81(String y81) {
		this.y81 = y81;
	}
	public String getN81() {
		return n81;
	}
	public void setN81(String n81) {
		this.n81 = n81;
	}
	public String getY82() {
		return y82;
	}
	public void setY82(String y82) {
		this.y82 = y82;
	}
	public String getN82() {
		return n82;
	}
	public void setN82(String n82) {
		this.n82 = n82;
	}
	public String getY83() {
		return y83;
	}
	public void setY83(String y83) {
		this.y83 = y83;
	}
	public String getN83() {
		return n83;
	}
	public void setN83(String n83) {
		this.n83 = n83;
	}
	public String getY84() {
		return y84;
	}
	public void setY84(String y84) {
		this.y84 = y84;
	}
	public String getN84() {
		return n84;
	}
	public void setN84(String n84) {
		this.n84 = n84;
	}
	public String getY85() {
		return y85;
	}
	public void setY85(String y85) {
		this.y85 = y85;
	}
	public String getN85() {
		return n85;
	}
	public void setN85(String n85) {
		this.n85 = n85;
	}
	public String getY86() {
		return y86;
	}
	public void setY86(String y86) {
		this.y86 = y86;
	}
	public String getN86() {
		return n86;
	}
	public void setN86(String n86) {
		this.n86 = n86;
	}
	public String getY87() {
		return y87;
	}
	public void setY87(String y87) {
		this.y87 = y87;
	}
	public String getN87() {
		return n87;
	}
	public void setN87(String n87) {
		this.n87 = n87;
	}
	public String getY88() {
		return y88;
	}
	public void setY88(String y88) {
		this.y88 = y88;
	}
	public String getN88() {
		return n88;
	}
	public void setN88(String n88) {
		this.n88 = n88;
	}
	public String getY89() {
		return y89;
	}
	public void setY89(String y89) {
		this.y89 = y89;
	}
	public String getN89() {
		return n89;
	}
	public void setN89(String n89) {
		this.n89 = n89;
	}
	public String getY90() {
		return y90;
	}
	public void setY90(String y90) {
		this.y90 = y90;
	}
	public String getN90() {
		return n90;
	}
	public void setN90(String n90) {
		this.n90 = n90;
	}
	public String getY91() {
		return y91;
	}
	public void setY91(String y91) {
		this.y91 = y91;
	}
	public String getN91() {
		return n91;
	}
	public void setN91(String n91) {
		this.n91 = n91;
	}
	public String getY92() {
		return y92;
	}
	public void setY92(String y92) {
		this.y92 = y92;
	}
	public String getN92() {
		return n92;
	}
	public void setN92(String n92) {
		this.n92 = n92;
	}
	public String getY93() {
		return y93;
	}
	public void setY93(String y93) {
		this.y93 = y93;
	}
	public String getN93() {
		return n93;
	}
	public void setN93(String n93) {
		this.n93 = n93;
	}
	public String getY94() {
		return y94;
	}
	public void setY94(String y94) {
		this.y94 = y94;
	}
	public String getN94() {
		return n94;
	}
	public void setN94(String n94) {
		this.n94 = n94;
	}
	public String getY95() {
		return y95;
	}
	public void setY95(String y95) {
		this.y95 = y95;
	}
	public String getN95() {
		return n95;
	}
	public void setN95(String n95) {
		this.n95 = n95;
	}
	public String getY96() {
		return y96;
	}
	public void setY96(String y96) {
		this.y96 = y96;
	}
	public String getN96() {
		return n96;
	}
	public void setN96(String n96) {
		this.n96 = n96;
	}
	public String getY97() {
		return y97;
	}
	public void setY97(String y97) {
		this.y97 = y97;
	}
	public String getN97() {
		return n97;
	}
	public void setN97(String n97) {
		this.n97 = n97;
	}
	public String getY98() {
		return y98;
	}
	public void setY98(String y98) {
		this.y98 = y98;
	}
	public String getN98() {
		return n98;
	}
	public void setN98(String n98) {
		this.n98 = n98;
	}
	public String getY99() {
		return y99;
	}
	public void setY99(String y99) {
		this.y99 = y99;
	}
	public String getN99() {
		return n99;
	}
	public void setN99(String n99) {
		this.n99 = n99;
	}
	private String comments;
	private String datecreated;
	private Timestamp datemodified;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrimer1() {
		return primer1;
	}
	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}
	public String getPrimer2() {
		return primer2;
	}
	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	
}
