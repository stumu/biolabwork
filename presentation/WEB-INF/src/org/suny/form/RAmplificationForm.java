package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;

public class RAmplificationForm extends ActionForm{
	private String basicGeneName  = null;
	private String ml;
	private String volume;
	private String excess;
	private String store;
	private String position;
	private String comments;
	private String userdate;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getMl() {
		return ml;
	}
	public void setMl(String ml) {
		this.ml = ml;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getExcess() {
		return excess;
	}
	public void setExcess(String excess) {
		this.excess = excess;
	}
	public String getStore() {
		return store;
	}
	public void setStore(String store) {
		this.store = store;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
	
}
