

package org.suny.form;

import org.apache.struts.action.ActionForm;

public class ReceiveMachineForm  extends ActionForm {
    
    private String[] templateIds;

	public String[] getTemplateIds() {
		return templateIds;
	}

	public void setTemplateIds(String[] templateIds) {
		this.templateIds = templateIds;
	}
    
	
}
