package org.suny.form;

import org.apache.struts.action.ActionForm;
import org.suny.model.MiniaId;

public class BtsRespotForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private MiniaId id;
	private String userdate;
	private String comments;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public MiniaId getId() {
		return id;
	}
	public void setId(MiniaId id) {
		this.id = id;
	}
	public String getUserdate() {
		return userdate;
	}
	public void setUserdate(String userdate) {
		this.userdate = userdate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}
