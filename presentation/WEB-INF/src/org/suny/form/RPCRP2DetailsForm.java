package org.suny.form;

import org.apache.struts.action.ActionForm;

public class RPCRP2DetailsForm extends ActionForm{
	private String basicGeneName  = null;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	private String pcrdate  = null;
	private String size = null;
	private String ramplicony = null;
	private String rampliconn = null;
	private String comments = null;
	public String getPcrdate() {
		return pcrdate;
	}
	public void setPcrdate(String pcrdate) {
		this.pcrdate = pcrdate;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	
	public String getRamplicony() {
		return ramplicony;
	}
	public void setRamplicony(String ramplicony) {
		this.ramplicony = ramplicony;
	}
	public String getRampliconn() {
		return rampliconn;
	}
	public void setRampliconn(String rampliconn) {
		this.rampliconn = rampliconn;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
