package org.suny.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class SequenceFileUploadForm extends ActionForm{
	private String sequenceFile;
	private String fileSequence;
	public String getFileSequence() {
		return fileSequence;
	}

	public void setFileSequence(String fileSequence) {
		this.fileSequence = fileSequence;
	}

	public String getSequenceFile() {
		return sequenceFile;
	}

	public void setSequenceFile(String sequenceFile) {
		this.sequenceFile = sequenceFile;
	}

	
	
}
