package org.suny.form;

import java.sql.Timestamp;

import org.apache.struts.action.ActionForm;

public class ColonyPCRBForm extends ActionForm{
	private String basicGeneName  = null;
	private String basicVal  = null;
	private String id;
	private String primer1;
	private String primer2;
	private String size;
	private String date1;
	private String date2;
	private String date3;
	private String seq;
	private String by80;
	private String bn80;
	private String by81;
	private String bn81;
	private String by82;
	private String bn82;
	private String by83;
	private String bn83;
	private String by84;
	private String bn84;
	private String by85;
	private String bn85;
	private String by86;
	private String bn86;
	private String by87;
	private String bn87;
	private String by88;
	private String bn88;
	private String by89;
	private String bn89;
	
	private String by90;
	private String bn90;
	private String by91;
	private String bn91;
	private String by92;
	private String bn92;
	private String by93;
	private String bn93;
	private String by94;
	private String bn94;
	private String by95;
	private String bn95;
	private String by96;
	private String bn96;
	private String by97;
	private String bn97;
	private String by98;
	private String bn98;
	private String by99;
	private String bn99;
	

	public String getBy80() {
		return by80;
	}
	public void setBy80(String by80) {
		this.by80 = by80;
	}
	public String getBn80() {
		return bn80;
	}
	public void setBn80(String bn80) {
		this.bn80 = bn80;
	}
	public String getBy81() {
		return by81;
	}
	public void setBy81(String by81) {
		this.by81 = by81;
	}
	public String getBn81() {
		return bn81;
	}
	public void setBn81(String bn81) {
		this.bn81 = bn81;
	}
	public String getBy82() {
		return by82;
	}
	public void setBy82(String by82) {
		this.by82 = by82;
	}
	public String getBn82() {
		return bn82;
	}
	public void setBn82(String bn82) {
		this.bn82 = bn82;
	}
	public String getBy83() {
		return by83;
	}
	public void setBy83(String by83) {
		this.by83 = by83;
	}
	public String getBn83() {
		return bn83;
	}
	public void setBn83(String bn83) {
		this.bn83 = bn83;
	}
	public String getBy84() {
		return by84;
	}
	public void setBy84(String by84) {
		this.by84 = by84;
	}
	public String getBn84() {
		return bn84;
	}
	public void setBn84(String bn84) {
		this.bn84 = bn84;
	}
	public String getBy85() {
		return by85;
	}
	public void setBy85(String by85) {
		this.by85 = by85;
	}
	public String getBn85() {
		return bn85;
	}
	public void setBn85(String bn85) {
		this.bn85 = bn85;
	}
	public String getBy86() {
		return by86;
	}
	public void setBy86(String by86) {
		this.by86 = by86;
	}
	public String getBn86() {
		return bn86;
	}
	public void setBn86(String bn86) {
		this.bn86 = bn86;
	}
	public String getBy87() {
		return by87;
	}
	public void setBy87(String by87) {
		this.by87 = by87;
	}
	public String getBn87() {
		return bn87;
	}
	public void setBn87(String bn87) {
		this.bn87 = bn87;
	}
	public String getBy88() {
		return by88;
	}
	public void setBy88(String by88) {
		this.by88 = by88;
	}
	public String getBn88() {
		return bn88;
	}
	public void setBn88(String bn88) {
		this.bn88 = bn88;
	}
	public String getBy89() {
		return by89;
	}
	public void setBy89(String by89) {
		this.by89 = by89;
	}
	public String getBn89() {
		return bn89;
	}
	public void setBn89(String bn89) {
		this.bn89 = bn89;
	}
	public String getBy90() {
		return by90;
	}
	public void setBy90(String by90) {
		this.by90 = by90;
	}
	public String getBn90() {
		return bn90;
	}
	public void setBn90(String bn90) {
		this.bn90 = bn90;
	}
	public String getBy91() {
		return by91;
	}
	public void setBy91(String by91) {
		this.by91 = by91;
	}
	public String getBn91() {
		return bn91;
	}
	public void setBn91(String bn91) {
		this.bn91 = bn91;
	}
	public String getBy92() {
		return by92;
	}
	public void setBy92(String by92) {
		this.by92 = by92;
	}
	public String getBn92() {
		return bn92;
	}
	public void setBn92(String bn92) {
		this.bn92 = bn92;
	}
	public String getBy93() {
		return by93;
	}
	public void setBy93(String by93) {
		this.by93 = by93;
	}
	public String getBn93() {
		return bn93;
	}
	public void setBn93(String bn93) {
		this.bn93 = bn93;
	}
	public String getBy94() {
		return by94;
	}
	public void setBy94(String by94) {
		this.by94 = by94;
	}
	public String getBn94() {
		return bn94;
	}
	public void setBn94(String bn94) {
		this.bn94 = bn94;
	}
	public String getBy95() {
		return by95;
	}
	public void setBy95(String by95) {
		this.by95 = by95;
	}
	public String getBn95() {
		return bn95;
	}
	public void setBn95(String bn95) {
		this.bn95 = bn95;
	}
	public String getBy96() {
		return by96;
	}
	public void setBy96(String by96) {
		this.by96 = by96;
	}
	public String getBn96() {
		return bn96;
	}
	public void setBn96(String bn96) {
		this.bn96 = bn96;
	}
	public String getBy97() {
		return by97;
	}
	public void setBy97(String by97) {
		this.by97 = by97;
	}
	public String getBn97() {
		return bn97;
	}
	public void setBn97(String bn97) {
		this.bn97 = bn97;
	}
	public String getBy98() {
		return by98;
	}
	public void setBy98(String by98) {
		this.by98 = by98;
	}
	public String getBn98() {
		return bn98;
	}
	public void setBn98(String bn98) {
		this.bn98 = bn98;
	}
	public String getBy99() {
		return by99;
	}
	public void setBy99(String by99) {
		this.by99 = by99;
	}
	public String getBn99() {
		return bn99;
	}
	public void setBn99(String bn99) {
		this.bn99 = bn99;
	}
	private String comments;
	private String datecreated;
	private Timestamp datemodified;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	public String getBasicVal() {
		return basicVal;
	}
	public void setBasicVal(String basicVal) {
		this.basicVal = basicVal;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrimer1() {
		return primer1;
	}
	public void setPrimer1(String primer1) {
		this.primer1 = primer1;
	}
	public String getPrimer2() {
		return primer2;
	}
	public void setPrimer2(String primer2) {
		this.primer2 = primer2;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDate1() {
		return date1;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public String getDate2() {
		return date2;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public String getDate3() {
		return date3;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getDatecreated() {
		return datecreated;
	}
	public void setDatecreated(String datecreated) {
		this.datecreated = datecreated;
	}
	public Timestamp getDatemodified() {
		return datemodified;
	}
	public void setDatemodified(Timestamp datemodified) {
		this.datemodified = datemodified;
	}
	
}
