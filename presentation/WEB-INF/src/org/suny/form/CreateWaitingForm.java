package org.suny.form;

import org.apache.struts.action.ActionForm;


public class CreateWaitingForm extends ActionForm
{
	 
      private String personName=null;
	  private String labName=null;
	  private String machineName=null;
	  
	  private String datetaking=null;
	  private String releaseDate = null;
	  private String phone = null;
	  private String email = null;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDatetaking() {
		return datetaking;
	}
	public void setDatetaking(String datetaking) {
		this.datetaking = datetaking;
	}
	public String getLabName() {
		return labName;
	}
	public void setLabName(String labName) {
		this.labName = labName;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	 

} 