package org.suny.form;

import org.apache.struts.action.ActionForm;

public class PCRP2DetailsForm extends ActionForm{
	private String basicGeneName  = null;
	public String getBasicGeneName() {
		return basicGeneName;
	}
	public void setBasicGeneName(String basicGeneName) {
		this.basicGeneName = basicGeneName;
	}
	private String pcrdate  = null;
	private String size = null;
	private String amplicony = null;
	private String ampliconn = null;
	private String comments = null;
	public String getPcrdate() {
		return pcrdate;
	}
	public void setPcrdate(String pcrdate) {
		this.pcrdate = pcrdate;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getAmplicony() {
		return amplicony;
	}
	public void setAmplicony(String amplicony) {
		this.amplicony = amplicony;
	}
	public String getAmpliconn() {
		return ampliconn;
	}
	public void setAmpliconn(String ampliconn) {
		this.ampliconn = ampliconn;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
