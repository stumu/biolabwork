//require plugin by Mohd Khairi Lamsah (hairiemx at gmail dot com)
//Feel free to modify it. 

(function(jQuery){
   jQuery.plg={
		inited:false,
		list:{},
		css:{},
		fn:[],
		queue:[],
		jsBase:'.',
		cssBase:'.',
		complete:function(){
			jQuery.plg.inited=false;
			jQuery(document).ready(function(){
				while(jQuery.plg.fn.length)
					jQuery.plg.fn.shift().apply(document);
			});
		},
		next: function(){	  
		  var plugin=jQuery.plg.queue.shift();
			jQuery.plg.inited=true;
			if(typeof jQuery.plg.list[plugin.name]=='undefined'){
			  var src = jQuery.plg.jsBase + '/' + plugin.name + '.js';
			  jQuery.plg.list[plugin.name]=false;
			  jQuery.get(src, function(data){
				jQuery.plg.list[plugin.name]=true;
				//Evaluate later when dom is ready
				jQuery.plg.fn.push(function(){eval.call(window,data);});
				//Run plugin onload after dom is ready
				if(plugin.onload)jQuery.plg.fn.push(plugin.onload);
				//Proceed to next 
				jQuery.plg.proceed();
			  });						
			}
			else if (jQuery.plg.list[plugin.name]){
				//If the plugin is already loaded, 
				//push onload to function queue and 
				//proceed to next plugin.
				if(plugin.onload)
					jQuery.plg.fn.push(plugin.onload);
				jQuery.plg.proceed();
			}
			
		},  
		proceed:function(){
			if(jQuery.plg.queue.length)
				jQuery.plg.next();
			else
				jQuery.plg.complete();							
		}
   };
   
   //Try get jquery-require.js base path
   jQuery("head").find("script:last").each(function(){
		var pathArray=this.src.split('/');
		pathArray.pop();
		jQuery.plg.jsBase=pathArray.join('/') || '.';
		jQuery.plg.cssBase=jQuery.plg.jsBase + '/css';
   });
   
   jQuery.extend({
	requirecss: function(c){
	  var cList=c.split(",");
	  for(var i=0;i<cList.length;i++){
		  var cssFile=cList[i];
		  if(typeof jQuery.plg.css[cssFile]=="undefined"){	
				var c = document.createElement('link'); 
				c.type = 'text/css';
				c.rel = 'stylesheet';
				c.href = jQuery.plg.cssBase + '/' + cssFile + '.css';
				jQuery('head')[0].appendChild(c);
				jQuery.plg.css[cssFile]=true;
		  }
	  }			
	},
	require: function(plugin){
	  var p=plugin.split(',');
	  //If no onload function is assigned, assigned load function to null
	  var func=arguments.length > 1?arguments[1]:null;
	  var pLength=p.length;
	  for(var i=0;i<pLength;i++){
	  	  //Push the plugin into queue
		  jQuery.plg.queue.push({name: p[i], onload: (i==pLength-1?func:null)}); 
	  }
	  if(!jQuery.plg.inited){
		  jQuery.plg.next();
	  }
	}	   
  });

 jQuery.fn.extend({
	requireComplete: function(f) {
		//If plugin loading process complete
		if (!jQuery.plg.inited){
			// Execute the function immediately
			f.apply( document );
		}
		// Otherwise, remember the function for later
		else 
			// Add the function to the wait list
			jQuery.plg.fn.push( f );
	
		return this;
	}
  });
})(jQuery);