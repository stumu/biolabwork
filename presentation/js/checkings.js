function listRecordings(formName)
	{
		if(document.forms[formName].personName.value == null || document.forms[formName].personName.value ==""){
			alert("please enter the Person name");
			return;
		}
		
		if(document.forms[formName].email.value==""){
			alert("please enter the Email address");
			return;
		}
		else{
			if(checkemail(document.forms[formName].email.value) == false)
				return;
		}
		if(document.forms[formName].phone.value==""){
			alert("please enter the Phone number");
			return;
		}
		else{
			if(validatePhone(document.forms[formName].phone) == false)
				return;
		}
		if(document.forms[formName].labName.value==""){
			alert("please enter the Lab name");
			return;
		}
		
		
		if(document.getElementById("service").selectedIndex == 0){
			alert("please enter the Machine name");
			return;
		}
		if(document.getElementById("machine").selectedIndex == 0){
			alert("please enter the Machine serviceTag");
			return;
		}
		if(document.forms[formName].datetaking.value == null || document.forms[formName].datetaking.value ==""){
			alert("please enter the Date taking");
			return;
		}
		
		

		post('GetMachineForm', 'mainbody');
		
}
function searchGene(formName)
{
	
	post('SearchGeneForm', 'searchDiv');
}
function uploadSequenceFile(formName)
{
	
	post('SequenceFileUploadForm', 'fileUploadDiv');
}
function submitBasicGeneDetails(formName)
{
	
	post('BasicGeneDetailsForm', 'basicGeneDiv');
}
function submitPCRDetails(formName)
{
	var agree=confirm("Are you sure you want to submit?");
	if (!agree)
		return;	
	post('PCRP2DetailsForm', 'PCRP2div');
}
function submitRPCRDetails(formName)
{
	var agree=confirm("Are you sure you want to submit?");
	if (!agree)
		return;	
	post('RPCRP2DetailsForm', 'RPCRP2div');
}
function submitFormsByName(formName,div)
{
	var agree=confirm("Are you sure you want to submit?");
	if (!agree)
		return;	
	post(formName,div);
}
function submitAmplificationDetails(formName)
{
	var agree=confirm("Are you sure you want to submit?");
	if (!agree)
		return;	
	post('AmpDetailsForm', 'ampdiv');
}
function submitRAmplificationDetails(formName)
{
	var agree=confirm("Are you sure you want to submit?");
	if (!agree)
		return;	
	post('RAmpDetailsForm', 'rampdiv');
}
function showPCRHistory(gene,id,ind)
{
	//alert(gene);
	if(id == null)
		id = 0;
	if(ind != null)
		{
			if(ind == 1) 
				id = parseInt(id) +1;
			else if(ind == 0 & id > 0)
				id = parseInt(id)-1;
		}
	showPopup('getPCRDetailsHistory.do?geneName='+gene+'&id='+id,'PCR History');
}
function submitHistory(formName,direc)
{
	alert(formName);
}
function getSelectedGene(elem)
{
	
	//alert(elem);
	if(document.getElementById(elem).selectedIndex ==0)
	{
		alert("Please select a GENE name");
		return;
	}
	//alert(document.getElementById(elem).value);
	var val = document.getElementById(elem).value;
	get("getGeneDetails.do?geneName="+val,"mainbody");
}

function sendNotifications(formName)
	{
		
		if(document.forms[formName].description.value == null || document.forms[formName].description.value ==""){
			alert("please enter the Message");
			return;
		}
		

		post('SendNotificationsForm', 'mainbody');
		
		alert('Message sent successfully');

		
}
function clearEmailForm(formName){
	document.forms[formName].description.value="";
}


function checkemail(str){
 
	 var filter=/^.+@.+\..{2,3}$/
	 var testresults;
	 if (filter.test(str))
		testresults=true;
	 else {
		alert("Please input a valid email address!")
		testresults=false;
	}
	 return (testresults)
}

function validatePhone(fld) {
    var error = "";
    var stripped = fld.value.replace(/[\(\)\.\-\ ]/g, '');    
	var testresults ="true";
   if (fld.value == "") {
        alert("You didn't enter a phone number.");
        fld.style.background = 'Yellow';
		testresults=false;
    } else if (isNaN(parseInt(stripped))) {
         alert("The phone number contains illegal characters.");
        fld.style.background = 'Yellow';
		testresults=false;
    } else if (!(stripped.length == 10)) {
        alert("The phone number is the wrong length. Make sure you included an area code.");
        fld.style.background = 'Yellow';
		testresults=false;
    }
    return testresults;
}
function clearContentForm(formName){
	document.forms[formName].personName.value="";
	document.forms[formName].email.value="";
	document.forms[formName].phone.value="";
	document.forms[formName].labName.selectedIndex=0;
	document.getElementById("service").selectedIndex = 0;
	document.getElementById("machine").selectedIndex = 0;
	document.forms[formName].datetaking.value = "";
	//document.forms[formName].releaseDate.value = "";
}

function fillupExpectedDate(formName){
	alert(document.forms[formName].datetaking);
}
function saveWaitingEntry(formName)
	{
		if(document.forms[formName].personName.value == null || document.forms[formName].personName.value ==""){
			alert("please enter the Person name");
			return;
		}
		if(document.forms[formName].email.value==""){
			alert("please enter the Email address");
			return;
		}
		else{
			if(checkemail(document.forms[formName].email.value) == false)
				return;
		}
		if(document.forms[formName].phone.value==""){
			alert("please enter the Phone number");
			return;
		}
		else{
			if(validatePhone(document.forms[formName].phone) == false)
				return;
		}
		if(document.forms[formName].labName.value==""){
			alert("please enter the Lab name");
			return;
		}

		
		if(document.forms[formName].datetaking.value == null || document.forms[formName].datetaking.value ==""){
			alert("please enter the Date taking");
			return;
		}
		
		

		post('CreateWaitingForm', 'mainbody');
}

function clearWaitingListContentForm(formName){
	document.forms[formName].personName.value="";
	document.forms[formName].labName.selectedIndex=0;
	document.forms[formName].datetaking.value = "";
	document.forms[formName].email.value="";
	document.forms[formName].phone.value="";
}

function getMachineSerialNumberbyMachineName(formName){
	
		document.getElementById("service").selectedIndex = formName.selectedIndex;
	
}

function selectCheckbox(obj,counterEl)
{
	//alert(counterEl);
	if(obj.checked == true)
		document.getElementById(counterEl).checked = false;
	else
		document.getElementById(counterEl).checked = true;
}