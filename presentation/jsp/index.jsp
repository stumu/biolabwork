<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>

<html:html locale="true">
<head>
	<title>Tom Santangelo's Lab</title>
	<html:base/>

<!-- <script language="javascript">
	function validate(objForm){
		
		if(objForm.username.value.length==0){
			alert("Please enter  UserID!");
			objForm.username.focus();
			return false;
		}
		
		if(objForm.password.value.length==0){
			alert("Please enter Password!");
			objForm.password.focus();
			return false;
		}

		if(objForm.usertype.selectedIndex == 0 ){

		   alert("Please Select User Type!");
			objForm.usertype.focus();
			return false;
	   }

		return true;
	}
</script> -->

</head>

<body bgcolor="white">


<html:form action="/login" method="post" >
  <center>
    
	<!-- display errors -->
				  <tr>
                        <td><font color="red"><html:errors/></font></td>
		          </tr>
     <!-- ..................-->
 
	<table width="400" border="1" align="center" cellpadding="0" cellspacing="0">
	 
	<tr>
		   <td>
			      <table border="0" cellspacing="2" cellpadding="1" width="100%" >
                  
				  <tr bgcolor="#C0C0C0">
				     <td align="left" colspan="2"><font size="5">User Login</font></td>
				  </tr> 
				  
				  <tr><td colspan="2"><p>&nbsp;</p></td></tr>
				  
				  <tr align="center">
					  <td align="right">User ID:</td> 
					  <td><html:text property="username" size="30" maxlength="30"/></td>
				 </tr> 
				 
				 <tr align="center">
					  <td align="right">Password:</td> 
					  <td><html:password property="password" size="30" maxlength="30"/></td>
				 </tr> 
				 
				 

                  <tr><td colspan="2"><p>&nbsp;</p></td></tr>

				  <tr>
				      <td  align="center" colspan="2"><html:submit>Login Now !</html:submit></td>
				 </tr> 
				 
          
							  
		</table>
     </td>
    </tr>
</table>

	</center>


</html:form>
<body>
</html:html>
