<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="cpcraDetails" scope="request" class="org.suny.beans.ColonyPCRDetailsBean"/>
<%
org.suny.beans.ColonyPCRDetailsBean cpcra = (org.suny.beans.ColonyPCRDetailsBean)request.getAttribute("cpcraDetails");
%>
<b>Colony PCR confirm A (700-For/700-Rev) <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('ColonyPCRAForm','colonypcrbdiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/ColonypcrADetails" onsubmit="javascript:post('ColonypcrADetails', 'colonypcradiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="4" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

<tr>
<td  colspan='3'  class="formLabel" width="20%">Expected size =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="size"  value="<bean:write name="cpcraDetails"   property="size"/>" />&nbsp;bp
		</td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">030-039 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date1" value="<bean:write name="cpcraDetails"   property="date1"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRAForm'].date1,'anchor31','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRAForm');" TITLE="cal1.select(document.forms['ColonyPCRAForm'].date1,'anchor31','MM/dd/yyyy'); return false;" NAME="anchor31" ID="anchor31"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
	

<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">030</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay80" value="" name="ay80" onClick="javascript:selectCheckbox(this,'an80');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[0].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an80" value="" name="an80" onClick="javascript:selectCheckbox(this,'ay80');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[0].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">035</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay85" value="" name="ay85" onClick="javascript:selectCheckbox(this,'an85');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[5].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an85" value="" name="an85" onClick="javascript:selectCheckbox(this,'ay85');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[5].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">031</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay81" value="" name="ay81" onClick="javascript:selectCheckbox(this,'an81');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[1].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an81" value="" name="an81" onClick="javascript:selectCheckbox(this,'ay81');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[1].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">036</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay86" value="" name="ay86" onClick="javascript:selectCheckbox(this,'an86');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[6].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an86" value="" name="an86" onClick="javascript:selectCheckbox(this,'ay86');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[6].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">032</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay82" value="" name="ay82" onClick="javascript:selectCheckbox(this,'an82');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[2].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an82" value="" name="an82" onClick="javascript:selectCheckbox(this,'ay82');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[2].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">037</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay87" value="" name="ay87" onClick="javascript:selectCheckbox(this,'an87');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[7].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an87" value="" name="an87" onClick="javascript:selectCheckbox(this,'ay87');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[7].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">033</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay83" value="" name="ay83" onClick="javascript:selectCheckbox(this,'an83');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[3].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an83" value="" name="an83" onClick="javascript:selectCheckbox(this,'ay83');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[3].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">038</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay88" value="" name="ay88" onClick="javascript:selectCheckbox(this,'an88');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[8].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an88" value="" name="an88" onClick="javascript:selectCheckbox(this,'ay88');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[8].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">034</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay84" value="" name="ay84" onClick="javascript:selectCheckbox(this,'an84');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[4].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an84" value="" name="an84" onClick="javascript:selectCheckbox(this,'ay84');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[4].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">039</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay89" value="" name="ay89" onClick="javascript:selectCheckbox(this,'an89');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[9].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an89" value="" name="an89" onClick="javascript:selectCheckbox(this,'ay89');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[9].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">040-049 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date2" value="<bean:write name="cpcraDetails"   property="date2"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRAForm'].date2,'anchor32','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRAForm');" TITLE="cal1.select(document.forms['ColonyPCRAForm'].date2,'anchor32','MM/dd/yyyy'); return false;" NAME="anchor32" ID="anchor32"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">040</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay90" value="" name="ay90" onClick="javascript:selectCheckbox(this,'an90');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[10].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an90" value="" name="an90" onClick="javascript:selectCheckbox(this,'ay90');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[10].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">045</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay95" value="" name="ay95" onClick="javascript:selectCheckbox(this,'an95');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[15].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an95" value="" name="an95" onClick="javascript:selectCheckbox(this,'ay95');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[15].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">041</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay91" value="" name="ay91" onClick="javascript:selectCheckbox(this,'an91');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[11].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an91" value="" name="an91" onClick="javascript:selectCheckbox(this,'ay91');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[11].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">046</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay96" value="" name="ay96" onClick="javascript:selectCheckbox(this,'an96');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[16].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an96" value="" name="an96" onClick="javascript:selectCheckbox(this,'ay96');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[16].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">042</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay92" value="" name="ay92" onClick="javascript:selectCheckbox(this,'an92');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[12].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an92" value="" name="an92" onClick="javascript:selectCheckbox(this,'ay92');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[12].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">047</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay97" value="" name="ay97" onClick="javascript:selectCheckbox(this,'an97');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[17].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an97" value="" name="an97" onClick="javascript:selectCheckbox(this,'ay97');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[17].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">043</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay93" value="" name="ay93" onClick="javascript:selectCheckbox(this,'an93');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[13].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an93" value="" name="an93" onClick="javascript:selectCheckbox(this,'ay93');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[13].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">048</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay98" value="" name="ay98" onClick="javascript:selectCheckbox(this,'an98');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[18].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an98" value="" name="an98" onClick="javascript:selectCheckbox(this,'ay98');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[18].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">044</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay94" value="" name="ay94" onClick="javascript:selectCheckbox(this,'an94');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[14].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an94" value="" name="an94" onClick="javascript:selectCheckbox(this,'ay94');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[14].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">049</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay99" value="" name="ay99" onClick="javascript:selectCheckbox(this,'an99');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[19].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an99" value="" name="an99" onClick="javascript:selectCheckbox(this,'ay99');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[19].equals("0")){%> checked<%}%>  /></td>
</tr>

<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">050-059 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date3" value="<bean:write name="cpcraDetails"   property="date3"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRAForm'].date3,'anchor33','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRAForm');" TITLE="cal1.select(document.forms['ColonyPCRAForm'].date3,'anchor33','MM/dd/yyyy'); return false;" NAME="anchor33" ID="anchor33"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">050</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="ay50" value="" name="ay50" onClick="javascript:selectCheckbox(this,'an50');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[20].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="an50" value="" name="an50" onClick="javascript:selectCheckbox(this,'ay50');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[20].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">055</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay55" value="" name="ay55" onClick="javascript:selectCheckbox(this,'an55');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[25].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an55" value="" name="an55" onClick="javascript:selectCheckbox(this,'ay55');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[25].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">051</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay51" value="" name="ay51" onClick="javascript:selectCheckbox(this,'an51');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[21].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an51" value="" name="an51" onClick="javascript:selectCheckbox(this,'ay51');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[21].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">056</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay56" value="" name="ay56" onClick="javascript:selectCheckbox(this,'an56');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[26].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an56" value="" name="an56" onClick="javascript:selectCheckbox(this,'ay56');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[26].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">052</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay52" value="" name="ay52" onClick="javascript:selectCheckbox(this,'an52');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[22].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an52" value="" name="an52" onClick="javascript:selectCheckbox(this,'ay52');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[22].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">057</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay57" value="" name="ay57" onClick="javascript:selectCheckbox(this,'an57');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[27].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an57" value="" name="an57" onClick="javascript:selectCheckbox(this,'ay57');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[27].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">053</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay53" value="" name="ay53" onClick="javascript:selectCheckbox(this,'an53');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[23].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an53" value="" name="an53" onClick="javascript:selectCheckbox(this,'ay53');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[23].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">058</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay58" value="" name="ay58" onClick="javascript:selectCheckbox(this,'an58');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[28].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an58" value="" name="an58" onClick="javascript:selectCheckbox(this,'ay58');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[28].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">054</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay54" value="" name="ay54" onClick="javascript:selectCheckbox(this,'an54');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[24].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an54" value="" name="an54" onClick="javascript:selectCheckbox(this,'ay54');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[24].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">059</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="ay59" value="" name="ay59" onClick="javascript:selectCheckbox(this,'an59');" <%if(cpcra.getSequence() != null && cpcra.getSequence()[29].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="an59" value="" name="an59" onClick="javascript:selectCheckbox(this,'ay59');" <%if(cpcra.getSequence()!= null && cpcra.getSequence()[29].equals("0")){%> checked<%}%>  /></td>
</tr>




</table>
</html:form>