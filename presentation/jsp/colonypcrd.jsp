<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="cpcrdDetails" scope="request" class="org.suny.beans.ColonyPCRDetailsBean"/>
<%
org.suny.beans.ColonyPCRDetailsBean cpcrd = (org.suny.beans.ColonyPCRDetailsBean)request.getAttribute("cpcrdDetails");
%>
<b>Colony PCR confirm D  <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('ColonyPCRDForm','colonypcrddiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/ColonypcrDDetails" onsubmit="javascript:post('ColonypcrDDetails', 'colonypcrddiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="2" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
<td  colspan='3'  class="formLabel" width="20%">Primer #1 =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="primer1"  value="<bean:write name="cpcrdDetails"   property="primer1"/>" />&nbsp; 
		</td>
</tr>
<tr>
<td  colspan='3'  class="formLabel" width="20%">Primer #2 =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="primer2"  value="<bean:write name="cpcrdDetails"   property="primer2"/>" />&nbsp;
		</td>
</tr>
<tr>
<td  colspan='3'  class="formLabel" width="20%">Expected size =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="size"  value="<bean:write name="cpcrdDetails"   property="size"/>" />&nbsp;bp
		</td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">100-104 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date1" value="<bean:write name="cpcrdDetails"   property="date1"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRDForm'].date1,'anchor25','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRDForm');" TITLE="cal1.select(document.forms['ColonyPCRDForm'].date1,'anchor25','MM/dd/yyyy'); return false;" NAME="anchor25" ID="anchor25"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
	<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">105-109 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date2" value="<bean:write name="cpcrdDetails"   property="date2"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRDForm'].date2,'anchor26','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRDForm');" TITLE="cal1.select(document.forms['ColonyPCRDForm'].date2,'anchor26','MM/dd/yyyy'); return false;" NAME="anchor26" ID="anchor26"> &nbsp;select</A>
		
		</td>
		
		
	</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">100</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy80" value="" name="dy80" onClick="javascript:selectCheckbox(this,'dn80');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[0].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn80" value="" name="dn80" onClick="javascript:selectCheckbox(this,'dy80');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[0].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">105</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy85" value="" name="dy85" onClick="javascript:selectCheckbox(this,'dn85');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[5].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn85" value="" name="dn85" onClick="javascript:selectCheckbox(this,'dy85');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[5].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">101</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy81" value="" name="dy81" onClick="javascript:selectCheckbox(this,'dn81');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[1].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn81" value="" name="dn81" onClick="javascript:selectCheckbox(this,'dy81');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[1].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">106</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy86" value="" name="dy86" onClick="javascript:selectCheckbox(this,'dn86');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[6].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn86" value="" name="dn86" onClick="javascript:selectCheckbox(this,'dy86');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[6].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">102</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy82" value="" name="dy82" onClick="javascript:selectCheckbox(this,'dn82');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[2].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn82" value="" name="dn82" onClick="javascript:selectCheckbox(this,'dy82');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[2].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">107</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy87" value="" name="dy87" onClick="javascript:selectCheckbox(this,'dn87');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[7].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn87" value="" name="dn87" onClick="javascript:selectCheckbox(this,'dy87');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[7].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">103</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy83" value="" name="dy83" onClick="javascript:selectCheckbox(this,'dn83');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[3].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn83" value="" name="dn83" onClick="javascript:selectCheckbox(this,'dy83');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[3].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">108</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy88" value="" name="dy88" onClick="javascript:selectCheckbox(this,'dn88');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[8].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn88" value="" name="dn88" onClick="javascript:selectCheckbox(this,'dy88');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[8].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">104</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy84" value="" name="dy84" onClick="javascript:selectCheckbox(this,'dn84');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[4].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn84" value="" name="dn84" onClick="javascript:selectCheckbox(this,'dy84');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[4].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">109</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy89" value="" name="dy89" onClick="javascript:selectCheckbox(this,'dn89');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[9].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn89" value="" name="dn89" onClick="javascript:selectCheckbox(this,'dy89');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[9].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">110-119 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date3" value="<bean:write name="cpcrdDetails"   property="date3"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRDForm'].date3,'anchor27','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRDForm');" TITLE="cal1.select(document.forms['ColonyPCRDForm'].date3,'anchor27','MM/dd/yyyy'); return false;" NAME="anchor27" ID="anchor27"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">110</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dy90" value="" name="dy90" onClick="javascript:selectCheckbox(this,'dn90');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[10].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="dn90" value="" name="dn90" onClick="javascript:selectCheckbox(this,'dy90');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[10].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">115</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy95" value="" name="dy95" onClick="javascript:selectCheckbox(this,'dn95');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[15].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn95" value="" name="dn95" onClick="javascript:selectCheckbox(this,'dy95');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[15].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">111</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy91" value="" name="dy91" onClick="javascript:selectCheckbox(this,'dn91');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[11].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn91" value="" name="dn91" onClick="javascript:selectCheckbox(this,'dy91');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[11].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">116</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy96" value="" name="dy96" onClick="javascript:selectCheckbox(this,'dn96');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[16].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn96" value="" name="dn96" onClick="javascript:selectCheckbox(this,'dy96');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[16].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">112</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy92" value="" name="dy92" onClick="javascript:selectCheckbox(this,'dn92');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[12].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn92" value="" name="dn92" onClick="javascript:selectCheckbox(this,'dy92');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[12].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">117</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy97" value="" name="dy97" onClick="javascript:selectCheckbox(this,'dn97');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[17].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn97" value="" name="dn97" onClick="javascript:selectCheckbox(this,'dy97');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[17].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">113</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy93" value="" name="dy93" onClick="javascript:selectCheckbox(this,'dn93');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[13].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn93" value="" name="dn93" onClick="javascript:selectCheckbox(this,'dy93');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[13].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">118</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy98" value="" name="dy98" onClick="javascript:selectCheckbox(this,'dn98');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[18].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn98" value="" name="dn98" onClick="javascript:selectCheckbox(this,'dy98');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[18].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">114</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy94" value="" name="dy94" onClick="javascript:selectCheckbox(this,'dn94');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[14].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn94" value="" name="dn94" onClick="javascript:selectCheckbox(this,'dy94');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[14].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">119</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dy99" value="" name="dy99" onClick="javascript:selectCheckbox(this,'dn99');" <%if(cpcrd.getSequence() != null && cpcrd.getSequence()[19].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="dn99" value="" name="dn99" onClick="javascript:selectCheckbox(this,'dy99');" <%if(cpcrd.getSequence()!= null && cpcrd.getSequence()[19].equals("0")){%> checked<%}%>  /></td>
</tr>
</table>
</html:form>