<table width="100%"  class="formElements marginT5">
<tr>
<td  style="background-color:#566D7E;font-family:arial;color:BLACK;font-size:20px;">
			Sequence Upload
</td>
</tr>
</table>

<%if(request.getAttribute("erMsg") != null) {%>
<font size="1" color="red"><%=request.getAttribute("erMsg")%></font>
<% } %>
<html:form action="/uploadSequenceFile.do" method="post" onsubmit="javascript:post('SequenceFileUploadForm', 'fileUploadDiv')">
<html:hidden property="fileSequence"/>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
	<tr>
	<td nowrap class="formLabel" width="20%">File Location<span class="Required">*</span>:</td>
	<td class="formData" width="80%">
	<!--input type="file" name="sequenceFile" styleClass="textBox"  size="40" onchange="javascript:copyTitle();"-->
	
	<html:file property="sequenceFile" styleClass="textBox"  size="40" onchange="javascript:copyTitle();"></html:file>
	</td>
	</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Note:</td>
	<td class="formData" width="80%">
	1) Please select the  <b>.CSV</b> file you would like to submit <br />					
	2) To convert an EXCEL file to a .CSV file, open the EXCEL file in MS-EXCEL and click on FILE > SAVE AS   <br />		and then select the file type: COMMA SEPARATED
	<br/>
	3) Once you select the file, Please click on SUBMIT FILE button 
														
	</td>
	</tr>
	<tr><td colspan="2" style='text-align: center;'>
	<input name="Button" type="button" class="pushButton" value="Submit File" onclick="javascript:uploadSequenceFile('SequenceFileUploadForm')">
	</td></tr>
</table>

</html:form>
<script>
function copyTitle()
{
	document.forms['SequenceFileUploadForm'].fileSequence.value = document.forms['SequenceFileUploadForm'].sequenceFile.value;
}

</script>