<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>

<p align="center"><font color="#000080" size="5">Pathway Studio Laptop Reservation System</font></p>
<br/>
<p align="left"><font color="#000080" size="3">Waiting List</font></p>

<jsp:useBean id="deviceList" scope="request" class="org.suny.beans.ValueList"/>
<jsp:useBean id="labList" scope="request" class="org.suny.beans.ValueList"/>


<% if(deviceList.size() == 0){%>
No records to display...
<%}%>
<div id="waitingList">
<html:form  action="/deleteWaitingEntry" onsubmit="javascript:post('ReceiveMachineForm', 'mainbody')">
<display:table name="deviceList"  id="row"  class="list marginT5" cellspacing="1" cellpadding="0" >
  <display:column sortable="false" title="Delete" >
		<input type="checkbox" name="templateIds" value="<bean:write name="row" property="id"/>" />
	</display:column>	

  <display:column property="labName" title="Lab name"/>
  <display:column property="personName" title="User"/>
  <display:column property="email" title="Email"/>
  <display:column property="phone" title="Phone"/>
  <display:column property="borrowdDate" title="Reserved on"/>
<display:column property="releaseDate" title="Due on"/>
</display:table>
</br>
<% if(session.getAttribute("username") != null && session.getAttribute("username").equals("admin")){ %>
<input name="Button" type="button" class="pushButton" value="Delete" onclick="javascript:post('ReceiveMachineForm', 'mainbody')">
<% } %>
</html:form>

</div>
<p align="left"><font color="#000080" size="3">Create Waiting Entry</font></p>

<div id="createWaiting">
<html:form  action="/saveWaitingEntry" onsubmit="javascript:post('CreateWaitingForm', 'mainbody')">

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
	<tr>
		<td nowrap class="formLabel" width="20%">Name<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><html:text property="personName" maxlength="30" size="50" styleClass="textBox"/>&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Email<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><html:text property="email" maxlength="30" size="50" styleClass="textBox"/>&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Phone<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><html:text property="phone" maxlength="30" size="50" styleClass="textBox"/>&nbsp;</td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Lab name<span class="Required">*</span> :</td>
		<td class="formData" width="80%">
			<html:select property="labName" tabindex="12" styleClass="listBox">
  				<option value="" SELECTED>select</option>
				<html:options collection="labName" property="labName" />
		    </html:select>
	</td>
	</tr>
	
		<tr>
		<td nowrap class="formLabel" width="20%">Reserved date<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><html:text readonly="true" property="datetaking" maxlength="30" size="50" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['CreateWaitingForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>


	</tr>
	

	
	
	
	
	</table>

</br>
<input name="Button" type="button" class="pushButton" value="Submit" onclick="javascript:saveWaitingEntry('CreateWaitingForm')">

</html:form>

</div>
<script>
clearWaitingListContentForm('CreateWaitingForm');
</script>