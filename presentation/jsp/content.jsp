<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>

<jsp:useBean id="sequenceList" scope="request" class="org.suny.beans.ValueList"/>
<!--p align="center"><font color="#000080" size="5">Master Genome Tracking System</font></p-->
<br/>


<jsp:useBean id="deviceList" scope="request" class="org.suny.beans.ValueList"/>
<jsp:useBean id="labList" scope="request" class="org.suny.beans.ValueList"/>
<jsp:useBean id="machinesList" scope="request" class="org.suny.beans.ValueList"/>

<table width="100%"  class="formElements marginT5">
<tr>
<td colspan="4" style="background-color:#566D7E;font-family:arial;color:BLACK;font-size:20px;">
			<%=request.getAttribute("selectedGeneName")%>
</td>
</tr>
</table>

<script>

</script>
<div id="assignMachine">

<div id="basicGeneDiv">
<%@ include file="/jsp/basicGene.jsp"%>
</div>


<table  border="0" cellspacing="1" cellpadding="0" class="formElements marginT5" width='100%'>
	<tr>
	<td style="background-color:#eeeeee; text-align='center';"> <img  src="images/TK1827.jpg" alt="Sequence" /> </td>
	</tr>
</table>
<table width="100%"  class="formElements marginT5" border='0'>
<tr>
<td  style="background-color:#566D7E;width:25%;">
PRIMER
</td>
<td  style="background-color:#566D7E;width:25%;">
SEQUENCE
</td>
<td  style="background-color:#566D7E;width:25%;">
GENOMIC LOCATION
</td>
<td  style="background-color:#566D7E;width:15%;">
PLATE #
</td>
<td  style="background-color:#566D7E;width:10%;">
WELL #
</td>
</tr>
<% for(int i=0;i<sequenceList.size();i++){ 
	org.suny.beans.PrimerSequenceDataBean mp = (org.suny.beans.PrimerSequenceDataBean)sequenceList.get(i);
%>
<tr>
<td  style="width:25%;">
<%=mp.getPrimer()%>
</td>
<td  style="width:25%;">
<%=mp.getPrimerSequence()%>
</td>
<td  style="width:25%;">
<%=mp.getStart()%> - <%=mp.getEnd()%>
</td>
<td  style="width:15%;">
<%=mp.getPlate()%> 
</td>
<td  style="width:10%;">
<%=mp.getWell_Position()%> 
</td>
</tr>
<% } %>
<tr>

</table>


<table width="100%"  class="formElements marginT5" border='0'>
<tr>
<td colspan="4" style="background-color:#566D7E;">
Page 2:
</td>
</tr>

<tr valign="top">

<td style="width:25%;">

<div id="PCRP2div">
<%@ include file="/jsp/P2D1.jsp"%>
</div>
<div id="RPCRP2div">
<%@ include file="/jsp/P2D2.jsp"%>
</div>
<div id="ampdiv">
<%@ include file="/jsp/amplification.jsp"%>
</div>
<div id="rampdiv">
<%@ include file="/jsp/ramplification.jsp"%>
</div>
<div id="colonypcradiv">
<%@ include file="/jsp/colonypcra.jsp"%>
</div>


</td>

<td style="width:25%;">
<div id="miniadiv">
<%@ include file="/jsp/minia.jsp"%>
</div>
<div id="rminiadiv">
<%@ include file="/jsp/rminia.jsp"%>
</div>
<div id="qchangebdiv">
<%@ include file="/jsp/qchangeb.jsp"%>
</div>
<div id="rqchangebdiv">
<%@ include file="/jsp/rqchangeb.jsp"%>
</div>

<div id="colonypcrbdiv">
<%@ include file="/jsp/colonypcrb.jsp"%>
</div>
<div id="minibdiv">
<%@ include file="/jsp/minib.jsp"%>
</div>

<div id="rminibdiv">
<%@ include file="/jsp/rminib.jsp"%>
</div>

</td>

<td style="width:25%;">
<div id="qchangecdiv">
<%@ include file="/jsp/qchangec.jsp"%>
</div>
<div id="rqchangecdiv">
<%@ include file="/jsp/rqchangec.jsp"%>
</div>

<div id="colonypcrcdiv">
<%@ include file="/jsp/colonypcrc.jsp"%>
</div>

<div id="minicdiv">
<%@ include file="/jsp/minic.jsp"%>
</div>
<div id="rminicdiv">
<%@ include file="/jsp/rminic.jsp"%>
</div>
</td>

<td style="width:25%;">
<div id="qchangeddiv">
<%@ include file="/jsp/qchanged.jsp"%>
</div>
<div id="rqchangeddiv">
<%@ include file="/jsp/rqchanged.jsp"%>
</div>

<div id="colonypcrddiv">
<%@ include file="/jsp/colonypcrd.jsp"%>
</div>
<div id="miniddiv">
<%@ include file="/jsp/minid.jsp"%>
</div>
<div id="rminiddiv">
<%@ include file="/jsp/rminid.jsp"%>
</div>
</td>
</tr>

</table>
<table width="100%"  class="formElements marginT5" border='0'>
<tr>
<td colspan="2" style="background-color:#566D7E;">
Page 3:
</td>
</tr>
<tr valign="top">

<td style="width:50%;">
<div id="BTS559div">
<%@ include file="/jsp/BTS559.jsp"%>
</div>
</td>
	
</tr>
<tr valign="top">

<td style="width:50%;">
<div id="BRespotdiv">
<%@ include file="/jsp/bRespot.jsp"%>
</div>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(160-169)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR (160-169)</td>
				<td nowrap class="formLabel" width="20%"> X=700For/002</td>
				<td nowrap class="formLabel" width="20%"> Y=700Rev/001</td>
				<td nowrap class="formLabel" width="30%"> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">160 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">161 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">162 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">163 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">164 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">165 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">166 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">167 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">168 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">169 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	</td>
	
</tr>
<tr valign="top">

<td style="width:50%;">

<div id="RepeatBTS559div">
<%@ include file="/jsp/RepeatBTS559.jsp"%>
</div>

</td>

</tr>

<tr valign="top">

<td style="width:50%;">

<div id="RepeatBSpotteddiv">
<%@ include file="/jsp/RepeatBtsRespotted.jsp"%>

</div>

</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(170-179)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR (170-179)</td>
				<td nowrap class="formLabel" width="20%"> X=700For/002</td>
				<td nowrap class="formLabel" width="20%"> Y=700Rev/001</td>
				<td nowrap class="formLabel" width="30%"> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">170 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">171 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">172 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">173 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">174 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">175 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">176 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">177 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">178 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">179 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	
</tr>


<tr valign="top">

<td style="width:50%;">
<b>Plate to 6MP <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from finals <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">(180-189)&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">(190-199)&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR (001/002) Expect 1400 bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">180 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">181 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>

					<tr>
						<td nowrap class="formLabel" width="40%">182 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">183 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">184 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">185 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>

					<tr>
						<td nowrap class="formLabel" width="40%">186 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">187</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				<tr>
						<td nowrap class="formLabel" width="40%">188 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">189 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">190 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">191 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">192 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">193 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">194 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">195 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">196 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">197 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">198 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">199 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
</tr>
</table>
</td>
	
</tr>
<tr valign="top">

<td style="width:50%;">
<b>"W" PCR from finals (use TS559 g-DNA as control) <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Primer #1 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;</td>
		
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Primer #2 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;</td>
		
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Expected size =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
		
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Strains</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;</td>
		
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>
<tr valign="top">

<td style="width:50%;">
<b>REPEAT Plate to 6MP <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from finals (200-209) <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR (001/002) Expect 1400 bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">200 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">201 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>

					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">202 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">203 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>

					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				<tr>
						<td nowrap class="formLabel" width="40%">204 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">205</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">206</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">207 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">208 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">209</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
</tr>
</table>
</td>
	
</tr>
<tr valign="top">
<td style="width:50%;" > <img  src="images/TK1827.jpg" alt="Sequence" /> </td>
</tr>
<tr valign="top">
<td style="width:50%;">
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr>
		<td nowrap class="formLabel" width="20%">General Comments: </td>
		<td class="formData" width="80%"><textarea rows="200" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
		</tr>
	</table>
</td>
</tr>
</table>

<table width="100%"  class="formElements marginT5" border='0'>
<tr>
<td colspan="2" style="background-color:#566D7E;">
Page 4:
</td>
</tr>
<tr valign="top">

<td style="width:50%;">
<div id="CTS559div">
<!-- %@ include file="/jsp/cts559.jsp"% -->
</div>
</td>
	


</tr>

<tr valign="top">

<td style="width:50%;">
<b>C Respotted <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(210-219) &nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR</td>
				<td nowrap class="formLabel" width="20%" style='text-align: left;'> X=</td>
				<td nowrap class="formLabel" width="20%"style='text-align: left;'> Y=</td>
				<td nowrap class="formLabel" width="30%"style='text-align: left;'> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">210 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">211</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">212 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">213 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">214 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">215</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">216 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">217 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">218 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">219 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	
	
</tr>
<tr valign="top">

<td style="width:50%;">
<b> REPEAT &nbsp;&nbsp;&nbsp;&nbsp;C &nbsp;&nbsp;&nbsp;&nbsp; TS559 <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT &nbsp;&nbsp;C &nbsp;&nbsp;Respotted <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(220-229) &nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR</td>
				<td nowrap class="formLabel" width="20%" style='text-align: left;'> X=</td>
				<td nowrap class="formLabel" width="20%"style='text-align: left;'> Y=</td>
				<td nowrap class="formLabel" width="30%"style='text-align: left;'> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">220 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">221</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">222 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">223 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">224 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">225</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">226 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">227 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">228 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">229</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Plate to 6MP  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from finals <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">230/239&nbsp;<br/>Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">240/249&nbsp;<br/>Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp;Expect <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp; bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					
					<tr>
						<td nowrap class="formLabel" width="40%">230 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">231 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
					<tr>
						<td nowrap class="formLabel" width="40%">232 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">233 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="100%" colspan='3' style='text-align:center;'>283 </td>
						
						
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">234 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">235 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">236 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">237 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					<tr>
						<td nowrap class="formLabel" width="40%">238</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">239</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">240 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">241 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">242</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">243</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">244 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">245 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					
					<tr>
						<td nowrap class="formLabel" width="40%">246</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">247</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">248 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">249 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
</tr>
</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Plate to 6MP <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>

</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from finals (250-259) <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp;Expect <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp; bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					
					<tr>
						<td nowrap class="formLabel" width="40%">250 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">251 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">252 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">253 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
				
				<tr>
						<td nowrap class="formLabel" width="40%">254 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">255</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">256</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">257 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">258 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">259</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>

</tr>
</table>
</td>
</tr>
<tr valign="top">
<td style="width:50%;" > <img  src="images/TK1827.jpg" alt="Sequence" /> </td>
</tr>
<tr valign="top">
<td style="width:50%;">
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr>
		<td nowrap class="formLabel" width="20%">General Comments: </td>
		<td class="formData" width="80%"><textarea rows="200" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
		</tr>
	</table>
</td>
</tr>
<tr valign="top">

<td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr valign="top">

<td style="width:50%;">
<b> Amplicon PCR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Strain= <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/save.jpg" alt="Click here to save" /> </b>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">001/002 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" name="templateIds" value=""> &nbsp;NO<input type="checkbox" name="templateIds" value=""></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
</tr>
</table>
</td>
</tr>

<tr valign="top">
<td >
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr valign="top">

<td style="width:50%;">
<b>REPEAT Amplicon PCR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Strain= <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">001/002 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" name="templateIds" value=""> &nbsp;NO<input type="checkbox" name="templateIds" value=""></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
</tr>

</table>
</td>
</tr>

</table>



<table width="100%"  class="formElements marginT5" border='0'>
<tr>
<td colspan="2" style="background-color:#566D7E;">
Page 5:
</td>
</tr>
<tr valign="top">

<td style="width:50%;">
<b> D   &nbsp;&nbsp;&nbsp; TS559 <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>D Respotted <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(260-269) &nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR</td>
				<td nowrap class="formLabel" width="20%" style='text-align: left;'> X=</td>
				<td nowrap class="formLabel" width="20%"style='text-align: left;'> Y=</td>
				<td nowrap class="formLabel" width="30%"style='text-align: left;'> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">260 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">261</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">262 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">263 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">264 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">265</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">266 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">267 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">268 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">269 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	
	
</tr>
<tr valign="top">

<td style="width:50%;">
<b> REPEAT &nbsp;&nbsp;&nbsp;&nbsp;D &nbsp;&nbsp;&nbsp;&nbsp; TS559 <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT &nbsp;&nbsp;D &nbsp;&nbsp;Respotted <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from intermediates <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">(270-279) &nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>
		
		
	</tr>
	<tr >
		<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="30%"> Diagnostic PCR</td>
				<td nowrap class="formLabel" width="20%" style='text-align: left;'> X=</td>
				<td nowrap class="formLabel" width="20%"style='text-align: left;'> Y=</td>
				<td nowrap class="formLabel" width="30%"style='text-align: left;'> Z=Control(TFB primers)</td>
			</tr>
		
			<tr>
				<td nowrap class="formLabel" width="30%"> Expected Size</td>
				<td nowrap class="formData" width="20%"> X=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="20%"> Y=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
				<td nowrap class="formData" width="30%"> Z=<input type="text" maxlength="30" size="20" styleClass="textBox" />&nbsp;bp</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">270 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">271</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">272 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">273 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">274 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">275</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">276 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">277 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td nowrap class="formLabel" width="25%"> </td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> X</td>
						<td nowrap  colspan="2" class="formLabel" width="25%"> Y</td>
						<td nowrap colspan="2" class="formLabel" width="35%"> Z</td>
					</tr>
				
					<tr>
						<td nowrap class="formLabel" width="40%">278 </td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">279</td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="10%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="10%"> Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%"> N</td>
						<td nowrap class="formLabel" width="10%">Y</td>
						<td nowrap class="formLabel" width="10%">N</td>
					</tr>
				</table>
			</td>
			</tr>
			</table>
	</td>

	</tr>
	
</table>
</td>
	
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Plate to 6MP  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>Genomic Prep from finals <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">280/289&nbsp;<br/>Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">290/299&nbsp;<br/>Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp;Expect <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp; bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					
					<tr>
						<td nowrap class="formLabel" width="40%">280 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">281 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
					<tr>
						<td nowrap class="formLabel" width="40%">282 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">283 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="100%" colspan='3' style='text-align:center;'>283 </td>
						
						
					</tr>
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">284 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">285 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">286 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">287 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					<tr>
						<td nowrap class="formLabel" width="40%">288</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">289</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">290 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">291 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">292</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">293</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">294 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">295 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					
					<tr>
						<td nowrap class="formLabel" width="40%">296</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">297</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					<tr>
						<td nowrap class="formLabel" width="40%">298 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">299 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
</tr>
</table>
</td>
	
</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Plate to 6MP <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Strain</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Intermediate</td>
		
		<td class="formData" width="80%">1 &nbsp;<input type="checkbox" name="templateIds" value=""> &nbsp;2 <input type="checkbox" name="templateIds" value="">&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>

</tr>

<tr valign="top">

<td style="width:50%;">
<b>REPEAT Genomic Prep from finals (300-309) <img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

	<tr>
		<td nowrap class="formLabel" width="20%">&nbsp;Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	
	<tr>
		<td nowrap class="formLabel" colspan='2'   style='text-align: left;' width="100%">PCR <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp;Expect <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" /> &nbsp;&nbsp;&nbsp;&nbsp; bp</td>
		
		

	</tr>
	
	
	<tr>
	<td colspan="2">
		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
					
					<tr>
						<td nowrap class="formLabel" width="40%">300 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">301 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">302 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">303 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
				
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
				
				<tr>
						<td nowrap class="formLabel" width="40%">304 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">305</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">306</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">307 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
				</table>
			</td>
			<td>
				<table width="20%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			
				
					<tr>
						<td nowrap class="formLabel" width="40%">308 </td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
						
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%">309</td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
						<td nowrap class="formLabel" width="30%"> <input type="checkbox" name="templateIds" value=""></td>
					</tr>
					<tr>
						<td nowrap class="formLabel" width="40%"> </td>
						<td nowrap class="formLabel" width="30%"> Y</td>
						<td nowrap class="formLabel" width="30%"> N</td>
					</tr>
					
					
				</table>
			</td>
			</tr>
</table>
</td>
	
</tr>
<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>

</tr>
</table>
</td>
</tr>
<tr valign="top">
<td style="width:50%;" > <img  src="images/TK1827.jpg" alt="Sequence" /> </td>
</tr>
<tr valign="top">
<td style="width:50%;">
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr>
		<td nowrap class="formLabel" width="20%">General Comments: </td>
		<td class="formData" width="80%"><textarea rows="200" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
		</tr>
	</table>
</td>
</tr>
<tr valign="top">

<td>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr valign="top">

<td style="width:50%;">
<b> Amplicon PCR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Strain= <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/save.jpg" alt="Click here to save" /> </b>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">001/002 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" name="templateIds" value=""> &nbsp;NO<input type="checkbox" name="templateIds" value=""></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
</tr>
</table>
</td>
</tr>

<tr valign="top">
<td >
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
		<tr valign="top">

<td style="width:50%;">
<b>REPEAT Amplicon PCR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Strain= <input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/save.jpg" alt="Click here to save" /> </b>

<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['GetMachineForm'].datetaking,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('GetMachineForm');" TITLE="cal1.select(document.forms['GetMachineForm'].releaseDate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">001/002 =</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" name="templateIds" value=""> &nbsp;NO<input type="checkbox" name="templateIds" value=""></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" >&nbsp;</textarea> 
		</td>
	</tr>

</table>
</td>
</tr>

</table>
</td>
</tr>

</table>

</div>

