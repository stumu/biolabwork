<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="rpcrDetails" scope="request" class="org.suny.beans.PCR2PDetailsBean"/>
<%
org.suny.beans.PCR2PDetailsBean pc1 = (org.suny.beans.PCR2PDetailsBean)request.getAttribute("rpcrDetails");
%>
<b>REPEAT 001/002 PCR 
<% if(request.getAttribute("oid") == null) {%>
<img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitRPCRDetails('RPCRP2DetailsForm')"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPopup('RPCRP2Details.do?geneName=<%=request.getAttribute("selectedGeneName")%>','History');"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /> </b>
<%}%>
<html:form  action="/RPCRP2Details" onsubmit="javascript:post('RPCRP2Details', 'RPCRP2div')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<% if(request.getAttribute("oid") != null) {%>
<input type="hidden" name="oid" id="oid" value="<%=request.getAttribute("oid")%>" />
<%}%>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date<span class="Required">*</span>:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="pcrdate" value="<bean:write name="rpcrDetails"   property="pcrdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['RPCRP2DetailsForm'].pcrdate,'anchor2','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('RPCRP2DetailsForm');" TITLE="cal1.select(document.forms['RPCRP2DetailsForm'].pcrdate,'anchor2','MM/dd/yyyy'); return false;" NAME="anchor2" ID="anchor2">select</A>
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Amplicon<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" id="ramplicony" value="" name="ramplicony" onClick="javascript:selectCheckbox(this,'rampliconn');" <%if(pc1.getAmplicony() != null && pc1.getAmplicony().equals("true")){%> checked<%}%>  /> &nbsp;NO<input type="checkbox"  id="rampliconn" value="" name="rampliconn" onClick="javascript:selectCheckbox(this,'ramplicony');" <%if(pc1.getAmpliconn() != null && pc1.getAmpliconn().equals("true")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" name="comments" styleClass="textBox" ><bean:write name="rpcrDetails"   property="comments"/></textarea> 
		</td>
	</tr>

</table>
</html:form>