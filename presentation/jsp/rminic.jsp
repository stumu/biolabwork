<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="rminicDetails" scope="request" class="org.suny.beans.MiniADetailsBean"/>
<%
org.suny.beans.MiniADetailsBean rmc = (org.suny.beans.MiniADetailsBean)request.getAttribute("rminicDetails");
%>

<b>REPEAT Mini C <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('RMiniCDetailsForm','rminicdiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/RMiniCDetails" onsubmit="javascript:post('RMiniCDetails', 'rminicdiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="6" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="userdate" value="<bean:write name="rminibDetails"   property="userdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['RMiniCDetailsForm'].userdate,'anchor16','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('RMiniCDetailsForm');" TITLE="cal1.select(document.forms['RMiniCDetailsForm'].userdate,'anchor16','MM/dd/yyyy'); return false;" NAME="anchor16" ID="anchor16">select</A>
		
		</td>
		
		
	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">Colony =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox"  name="colony" styleClass="textBox" value="<bean:write name="rminibDetails"   property="colony"/>" />&nbsp; 
		</td>

	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">[ng/ml] =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="ml" styleClass="textBox" value="<bean:write name="rminibDetails"   property="ml"/>" />&nbsp;
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" id="scy" value="" name="scy" onClick="javascript:selectCheckbox(this,'scn');" <%if(rmc.getSequence() != null && rmc.getSequence().equals("true")){%> checked<%}%>  /> &nbsp;NO<input type="checkbox"  id="scn" value="" name="scn" onClick="javascript:selectCheckbox(this,'scy');" <%if(rmc.getSequence() != null && rmc.getSequence().equals("false")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" name="comments"><bean:write name="rminibDetails"   property="comments"/>&nbsp;</textarea> 
		</td>
	</tr>
</table>
</html:form>