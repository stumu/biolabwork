<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="geneList" scope="request" class="org.suny.beans.ValueList"/>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="20%">Search results: </td>
				<td class="formData" width="80%"> 
				<html:select styleId="machine" property="geneName" tabindex="12" styleClass="listBox" size="10" multiple="multiple">
				  <option value="" selected>select</option>
				  <html:options collection="geneList" property="name"/>
				</html:select>
				</td>
			</tr>
			<tr> <td colspan='2' style='text-align:left;'>
			<input name="Button" type="button" class="pushButton" value="Submit" onclick="javascript:listRecordings('GetMachineForm')">
			</td>
			</tr>	
		</table>