<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="cpcrbDetails" scope="request" class="org.suny.beans.ColonyPCRDetailsBean"/>
<%
org.suny.beans.ColonyPCRDetailsBean cpcrb = (org.suny.beans.ColonyPCRDetailsBean)request.getAttribute("cpcrbDetails");
%>
<b>Colony PCR confirm B (700-For/700-Rev) <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('ColonyPCRBForm','colonypcrbdiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/ColonypcrBDetails" onsubmit="javascript:post('ColonypcrBDetails', 'colonypcrbdiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="3" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">

<tr>
<td  colspan='3'  class="formLabel" width="20%">Expected size =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="size"  value="<bean:write name="cpcrbDetails"   property="size"/>" />&nbsp;bp
		</td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">060-064 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date1" value="<bean:write name="cpcrbDetails"   property="date1"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRBForm'].date1,'anchor28','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRBForm');" TITLE="cal1.select(document.forms['ColonyPCRBForm'].date1,'anchor28','MM/dd/yyyy'); return false;" NAME="anchor28" ID="anchor28"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
	<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">065-069 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date2" value="<bean:write name="cpcrbDetails"   property="date2"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRBForm'].date2,'anchor29','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRBForm');" TITLE="cal1.select(document.forms['ColonyPCRBForm'].date2,'anchor29','MM/dd/yyyy'); return false;" NAME="anchor29" ID="anchor29"> &nbsp;select</A>
		
		</td>
		
		
	</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">060</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by80" value="" name="by80" onClick="javascript:selectCheckbox(this,'bn80');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[0].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn80" value="" name="bn80" onClick="javascript:selectCheckbox(this,'by80');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[0].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">065</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by85" value="" name="by85" onClick="javascript:selectCheckbox(this,'bn85');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[5].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn85" value="" name="bn85" onClick="javascript:selectCheckbox(this,'by85');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[5].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">061</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by81" value="" name="by81" onClick="javascript:selectCheckbox(this,'bn81');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[1].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn81" value="" name="bn81" onClick="javascript:selectCheckbox(this,'by81');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[1].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">066</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by86" value="" name="by86" onClick="javascript:selectCheckbox(this,'bn86');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[6].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn86" value="" name="bn86" onClick="javascript:selectCheckbox(this,'by86');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[6].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">062</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by82" value="" name="by82" onClick="javascript:selectCheckbox(this,'bn82');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[2].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn82" value="" name="bn82" onClick="javascript:selectCheckbox(this,'by82');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[2].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">067</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by87" value="" name="by87" onClick="javascript:selectCheckbox(this,'bn87');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[7].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn87" value="" name="bn87" onClick="javascript:selectCheckbox(this,'by87');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[7].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">063</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by83" value="" name="by83" onClick="javascript:selectCheckbox(this,'bn83');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[3].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn83" value="" name="bn83" onClick="javascript:selectCheckbox(this,'by83');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[3].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">068</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by88" value="" name="by88" onClick="javascript:selectCheckbox(this,'bn88');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[8].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn88" value="" name="bn88" onClick="javascript:selectCheckbox(this,'by88');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[8].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">064</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by84" value="" name="by84" onClick="javascript:selectCheckbox(this,'bn84');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[4].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn84" value="" name="bn84" onClick="javascript:selectCheckbox(this,'by84');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[4].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">069</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by89" value="" name="by89" onClick="javascript:selectCheckbox(this,'bn89');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[9].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn89" value="" name="bn89" onClick="javascript:selectCheckbox(this,'by89');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[9].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">070-079 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date3" value="<bean:write name="cpcrbDetails"   property="date3"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRBForm'].date3,'anchor30','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRBForm');" TITLE="cal1.select(document.forms['ColonyPCRBForm'].date3,'anchor30','MM/dd/yyyy'); return false;" NAME="anchor30" ID="anchor30"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">070</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="by90" value="" name="by90" onClick="javascript:selectCheckbox(this,'bn90');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[10].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="bn90" value="" name="bn90" onClick="javascript:selectCheckbox(this,'by90');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[10].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">075</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by95" value="" name="by95" onClick="javascript:selectCheckbox(this,'bn95');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[15].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn95" value="" name="bn95" onClick="javascript:selectCheckbox(this,'by95');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[15].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">071</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by91" value="" name="by91" onClick="javascript:selectCheckbox(this,'bn91');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[11].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn91" value="" name="bn91" onClick="javascript:selectCheckbox(this,'by91');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[11].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">076</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by96" value="" name="by96" onClick="javascript:selectCheckbox(this,'bn96');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[16].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn96" value="" name="bn96" onClick="javascript:selectCheckbox(this,'by96');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[16].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">072</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by92" value="" name="by92" onClick="javascript:selectCheckbox(this,'bn92');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[12].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn92" value="" name="bn92" onClick="javascript:selectCheckbox(this,'by92');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[12].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">077</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by97" value="" name="by97" onClick="javascript:selectCheckbox(this,'bn97');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[17].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn97" value="" name="bn97" onClick="javascript:selectCheckbox(this,'by97');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[17].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">073</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by93" value="" name="by93" onClick="javascript:selectCheckbox(this,'bn93');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[13].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn93" value="" name="bn93" onClick="javascript:selectCheckbox(this,'by93');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[13].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">078</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by98" value="" name="by98" onClick="javascript:selectCheckbox(this,'bn98');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[18].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn98" value="" name="bn98" onClick="javascript:selectCheckbox(this,'by98');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[18].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">074</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by94" value="" name="by94" onClick="javascript:selectCheckbox(this,'bn94');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[14].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn94" value="" name="bn94" onClick="javascript:selectCheckbox(this,'by94');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[14].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">079</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="by99" value="" name="by99" onClick="javascript:selectCheckbox(this,'bn99');" <%if(cpcrb.getSequence() != null && cpcrb.getSequence()[19].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="bn99" value="" name="bn99" onClick="javascript:selectCheckbox(this,'by99');" <%if(cpcrb.getSequence()!= null && cpcrb.getSequence()[19].equals("0")){%> checked<%}%>  /></td>
</tr>
</table>
</html:form>