<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%
//getting value
String username=request.getParameter("username");
String password=request.getParameter("password");
String usertype=request.getParameter("usertype");
%>

<html>

<head>
<title>Success</title>
</head>

<body>
<tiles:insert page="/jsp/template.jsp" flush="true">
	<tiles:put name="title" type="string" value="Welcome" />
	<tiles:put name="header" value="/jsp/top.jsp" />
	<tiles:put name="menu" value="/jsp/left.jsp" />
	<tiles:put name="body" value="/jsp/content.jsp" />
	<tiles:put name="bottom" value="/jsp/bottom.jsp" />	
</tiles:insert>
</body>

</html>
