<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="rminidDetails" scope="request" class="org.suny.beans.MiniADetailsBean"/>
<%
org.suny.beans.MiniADetailsBean rmd = (org.suny.beans.MiniADetailsBean)request.getAttribute("rminidDetails");
%>

<b>REPEAT Mini C <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('RMiniDDetailsForm','rminiddiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/RMiniDDetails" onsubmit="javascript:post('RMiniDDetails', 'rminiddiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="8" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="userdate" value="<bean:write name="rminibDetails"   property="userdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['RMiniDDetailsForm'].userdate,'anchor17','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('RMiniDDetailsForm');" TITLE="cal1.select(document.forms['RMiniDDetailsForm'].userdate,'anchor17','MM/dd/yyyy'); return false;" NAME="anchor17" ID="anchor17">select</A>
		
		</td>
		
		
	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">Colony =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox"  name="colony" styleClass="textBox" value="<bean:write name="rminibDetails"   property="colony"/>" />&nbsp; 
		</td>

	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">[ng/ml] =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="ml" styleClass="textBox" value="<bean:write name="rminibDetails"   property="ml"/>" />&nbsp;
		</td>

	</tr>
	<tr>
		<td  width="20%" wrap="true" class="formLabel">Sequence <br/>confirmed<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" id="scy" value="" name="scy" onClick="javascript:selectCheckbox(this,'scn');" <%if(rmd.getSequence() != null && rmd.getSequence().equals("true")){%> checked<%}%>  /> &nbsp;NO<input type="checkbox"  id="scn" value="" name="scn" onClick="javascript:selectCheckbox(this,'scy');" <%if(rmd.getSequence() != null && rmd.getSequence().equals("false")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" name="comments"><bean:write name="rminibDetails"   property="comments"/>&nbsp;</textarea> 
		</td>
	</tr>
</table>
</html:form>