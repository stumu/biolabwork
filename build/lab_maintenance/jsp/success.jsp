<%@ page language="java" %>


<%@ include file="/jsp/taglibs.jsp"%>
<script type="text/javascript" src="js/jquery-1.1.3.1.js"></script> 
<script type="text/javascript" src="js/jquery.form.js"></script> 
<script type="text/javascript" src="js/jquery.history.js"></script>
<script type="text/javascript" src="js/jquery-require.js"></script>
<script language="javascript" src="js/prototype-1.4.0.js"></script>
<script language="javascript" src="js/xmlhttprequest.js"></script>
<script language="javascript" src="js/CalendarPopup.js"></script>
<script language="javascript" src="js/popupLayer.js"></script>
<script language="javascript" src="js/checkings.js"></script>
<script language="javascript" src="js/overlib.js"></script>
<script language="javascript" src="js/CalendarPopup.js"></script>

<SCRIPT LANGUAGE="JavaScript" ID="js1">
var cal1 = new CalendarPopup();
</SCRIPT>
<SCRIPT LANGUAGE="JavaScript" ID="jscal1x">
var cal1x = new CalendarPopup("testdiv1");
</SCRIPT>

<html>
<head>
<title>
Welcome
</title>
<link rel="stylesheet" type="text/css" href="css/common.css">
<link rel="stylesheet" type="text/css" href="css/treestyle.css">
<link rel="stylesheet" type="text/css" href="css/calendar.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<%@ include file="/jsp/header.jsp"%>
</head>
<body bgcolor="#C0C0C0" onLoad="initPage();">
<style>
#waitDiv
{
	position:absolute;
	display: none;
	top: 50px;
	width: 100%;
	height: 90%;
	background-color: white;
	filter: Alpha(Opacity=50)
	opacity: .5;
	overflow: hidden;
}
</style>
<DIV ID="testdiv1" STYLE="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>

<div id="waitDiv" style="top: 50px; opacity:.5">
	<table width="100%" border="0">
		<tr><td  height="400" align="center" valign="middle">
			<img src="images/loading.gif">
		</td></tr>
	</table>
</div>
<DIV id="overDiv" style="Z-INDEX: 1000; VISIBILITY: hidden; POSITION: absolute;" ></DIV>
<div id="issueLayer1" style="position:absolute; z-index:5; left: 179px; top: 234px; display: none; background: white;" class="drag">
    <table width="400" height="15" border="1" cellspacing="0" cellpadding="0">
    <tr><td>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="titleBar">
		  <tr>
			<td valign="middle" width="22"><span><img src="images/info.gif" width="18" height="18"></span></td><td nowrap valign="middle"><span id="issueTitle1"></span></td>
			<td align="right"><a href="javascript:closeIssue1()" title="Close"><span>&nbsp;<font color="white">X</font>&nbsp;</span></a></td>
		  </tr>
		</table>
    </td></tr>
      <tr>
      <td class="issueDetailsLayer">
      <div id="issueDetailsLayer1" class="noDrag"></div>
      </td>
      </tr>
    </table>
	
</div>
<!--a href="javascript:getAjax('hello.html', 'issueDetailsLayer')">hello</a-->
<div id="issueLayer" style="position:absolute; z-index:4; left: 179px; top: 234px; display: none; background: white;" class="drag">
    <table width="400" height="15" border="1" cellspacing="0" cellpadding="0">
    <tr><td>
		<table width="100%" cellspacing="0" cellpadding="0" border="0" class="titleBar">
		  <tr>
			<td valign="middle" width="22"><span><img src="images/info.gif" width="18" height="18"></span></td><td nowrap valign="middle"><span id="issueTitle"></span></td>
			<td align="right"><a href="javascript:closeIssue()" title="Close"><span>&nbsp;<font color="white">X</font>&nbsp;</span></a></td>
		  </tr>
		</table>
    </td></tr>
      <tr>
      <td class="issueDetailsLayer">
      <div id="issueDetailsLayer" class="noDrag"></div>
      </td>
      </tr>
    </table>
	<div id="arrosKeys">
		<table>
		<tr>
		<td nowrap class="formLabel" width="50%"><input name="Button" type="button" class="pushButton" value="<<" id="<<"> </td>
		<td class="formData" width="50%"><input name="Button" type="button" class="pushButton" value=">>" id=">>"></textarea> 
		</td>
		</tr>
		</table>
	</div>
</div>
<SCRIPT LANGUAGE="javascript">
function initPage()
{
 	issuePopup = new PopupWindow('issueLayer');
	issuePopup.autoHide();
	issuePopup1 = new PopupWindow('issueLayer1');
	issuePopup1.autoHide();
	overlibPop = new PopupWindow('overDiv');
	overlibPop.autoHide();
//	DynLayerInit();
}
function hidePopups(){
	if(document.getElementById("issueLayer") != null)
		document.getElementById("issueLayer").style.display = "none";
	if(document.getElementById("issueLayer1") != null)
		document.getElementById("issueLayer1").style.display = "none";
	if(document.getElementById("overDiv") != null)
		document.getElementById("overDiv").style.display = "none";		
}

</script>
<table border="0" width="100%" cellspacing="0" cellpadding="0" class="height98" align="center"> 
  
<tr>

<td valign="top" >
	<div id="searchDiv">
		<%@ include file="/jsp/search.jsp"%>
	</div>
	<div id="fileUploadDiv">
		<%@ include file="/jsp/fileUploadJsp.jsp"%>
	</div>
	 <div id="mainbody">
	 <!-- %@ include file="/jsp/content.jsp"%-->
     </div>
	 </td>
  </tr>
 <tr >
<td  class="copyright" align="center">
<div id="copyright">Copyright&copy; 2010-2011. All rights reserved. &nbsp;<a href="http://www.osu.edu" target="_blank">www.osu.edu</a></div>
</td>
</tr>
</table>
</body>

</html>


