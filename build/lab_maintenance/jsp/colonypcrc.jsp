<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="cpcrcDetails" scope="request" class="org.suny.beans.ColonyPCRDetailsBean"/>
<%
org.suny.beans.ColonyPCRDetailsBean cpcrc = (org.suny.beans.ColonyPCRDetailsBean)request.getAttribute("cpcrcDetails");
%>
<b>Colony PCR confirm C   <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('ColonyPCRCForm','colonypcrcdiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/ColonypcrCDetails" onsubmit="javascript:post('ColonypcrCDetails', 'colonypcrcdiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="1" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
<td  colspan='3'  class="formLabel" width="20%">Primer #1 =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="primer1"  value="<bean:write name="cpcrcDetails"   property="primer1"/>" />&nbsp; 
		</td>
</tr>
<tr>
<td  colspan='3'  class="formLabel" width="20%">Primer #2 =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="primer2"  value="<bean:write name="cpcrcDetails"   property="primer2"/>" />&nbsp;
		</td>
</tr>
<tr>
<td  colspan='3'  class="formLabel" width="20%">Expected size =</td> 
		<td colspan='3' class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox" name="size"  value="<bean:write name="cpcrcDetails"   property="size"/>" />&nbsp;bp
		</td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">080-084 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date1" value="<bean:write name="cpcrcDetails"   property="date1"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRCForm'].date1,'anchor22','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRCForm');" TITLE="cal1.select(document.forms['ColonyPCRCForm'].date1,'anchor22','MM/dd/yyyy'); return false;" NAME="anchor22" ID="anchor22"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
	<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">085-089 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date2" value="<bean:write name="cpcrcDetails"   property="date2"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRCForm'].date2,'anchor23','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRCForm');" TITLE="cal1.select(document.forms['ColonyPCRCForm'].date2,'anchor23','MM/dd/yyyy'); return false;" NAME="anchor23" ID="anchor23"> &nbsp;select</A>
		
		</td>
		
		
	</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">080</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y80" value="" name="y80" onClick="javascript:selectCheckbox(this,'n80');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[0].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n80" value="" name="n80" onClick="javascript:selectCheckbox(this,'y80');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[0].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">085</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y85" value="" name="y85" onClick="javascript:selectCheckbox(this,'n85');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[5].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n85" value="" name="n85" onClick="javascript:selectCheckbox(this,'y85');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[5].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">081</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y81" value="" name="y81" onClick="javascript:selectCheckbox(this,'n81');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[1].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n81" value="" name="n81" onClick="javascript:selectCheckbox(this,'y81');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[1].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">086</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y86" value="" name="y86" onClick="javascript:selectCheckbox(this,'n86');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[6].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n86" value="" name="n86" onClick="javascript:selectCheckbox(this,'y86');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[6].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">082</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y82" value="" name="y82" onClick="javascript:selectCheckbox(this,'n82');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[2].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n82" value="" name="n82" onClick="javascript:selectCheckbox(this,'y82');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[2].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">087</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y87" value="" name="y87" onClick="javascript:selectCheckbox(this,'n87');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[7].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n87" value="" name="n87" onClick="javascript:selectCheckbox(this,'y87');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[7].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">083</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y83" value="" name="y83" onClick="javascript:selectCheckbox(this,'n83');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[3].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n83" value="" name="n83" onClick="javascript:selectCheckbox(this,'y83');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[3].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">088</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y88" value="" name="y88" onClick="javascript:selectCheckbox(this,'n88');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[8].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n88" value="" name="n88" onClick="javascript:selectCheckbox(this,'y88');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[8].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">084</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y84" value="" name="y84" onClick="javascript:selectCheckbox(this,'n84');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[4].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n84" value="" name="n84" onClick="javascript:selectCheckbox(this,'y84');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[4].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">089</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y89" value="" name="y89" onClick="javascript:selectCheckbox(this,'n89');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[9].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n89" value="" name="n89" onClick="javascript:selectCheckbox(this,'y89');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[9].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td colspan='3' nowrap class="formLabel" width="20%">090-099 Date:</td>
		
		<td colspan='3' class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" styleClass="textBox" name="date3" value="<bean:write name="cpcrcDetails"   property="date3"/>"/>&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['ColonyPCRCForm'].date3,'anchor24','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('ColonyPCRCForm');" TITLE="cal1.select(document.forms['ColonyPCRCForm'].date3,'anchor24','MM/dd/yyyy'); return false;" NAME="anchor24" ID="anchor24"> &nbsp;select</A>
		
		</td>
		
		
	</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
		<td  nowrap class="formLabel" width="20%">&nbsp;</td>
		<td  nowrap class="formLabel" width="15%">YES</td>
		<td  nowrap class="formLabel" width="15%">NO</td>
</tr>

<tr>
		<td  nowrap class="formLabel" width="20%">090</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="y90" value="" name="y90" onClick="javascript:selectCheckbox(this,'n90');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[10].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox"  id="n90" value="" name="n90" onClick="javascript:selectCheckbox(this,'y90');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[10].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">095</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y95" value="" name="y95" onClick="javascript:selectCheckbox(this,'n95');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[15].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n95" value="" name="n95" onClick="javascript:selectCheckbox(this,'y95');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[15].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">091</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y91" value="" name="y91" onClick="javascript:selectCheckbox(this,'n91');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[11].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n91" value="" name="n91" onClick="javascript:selectCheckbox(this,'y91');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[11].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">096</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y96" value="" name="y96" onClick="javascript:selectCheckbox(this,'n96');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[16].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n96" value="" name="n96" onClick="javascript:selectCheckbox(this,'y96');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[16].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">092</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y92" value="" name="y92" onClick="javascript:selectCheckbox(this,'n92');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[12].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n92" value="" name="n92" onClick="javascript:selectCheckbox(this,'y92');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[12].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">097</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y97" value="" name="y97" onClick="javascript:selectCheckbox(this,'n97');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[17].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n97" value="" name="n97" onClick="javascript:selectCheckbox(this,'y97');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[17].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">093</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y93" value="" name="y93" onClick="javascript:selectCheckbox(this,'n93');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[13].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n93" value="" name="n93" onClick="javascript:selectCheckbox(this,'y93');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[13].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">098</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y98" value="" name="y98" onClick="javascript:selectCheckbox(this,'n98');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[18].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n98" value="" name="n98" onClick="javascript:selectCheckbox(this,'y98');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[18].equals("0")){%> checked<%}%>  /></td>
</tr>
<tr>
		<td  nowrap class="formLabel" width="20%">094</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y94" value="" name="y94" onClick="javascript:selectCheckbox(this,'n94');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[14].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n94" value="" name="n94" onClick="javascript:selectCheckbox(this,'y94');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[14].equals("0")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="20%">099</td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="y99" value="" name="y99" onClick="javascript:selectCheckbox(this,'n99');" <%if(cpcrc.getSequence() != null && cpcrc.getSequence()[19].equals("1")){%> checked<%}%>  /></td>
		<td  nowrap class="formLabel" width="15%"><input type="checkbox" id="n99" value="" name="n99" onClick="javascript:selectCheckbox(this,'y99');" <%if(cpcrc.getSequence()!= null && cpcrc.getSequence()[19].equals("0")){%> checked<%}%>  /></td>
</tr>
</table>
</html:form>