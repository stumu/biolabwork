<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="qchangeCDetails" scope="request" class="org.suny.beans.QuickChangeDetailsBean"/>

<b>Quickchange C <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('QchangeCDetailsForm','qchangecdiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/QchangeCDetails" onsubmit="javascript:post('QchangeCDetails', 'qchangecdiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="3" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date<span class="Required">*</span>:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="userdate" value="<bean:write name="qchangeCDetails"   property="userdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['QchangeCDetailsForm'].userdate,'anchor18','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('QchangeCDetailsForm');" TITLE="cal1.select(document.forms['QchangeCDetailsForm'].userdate,'anchor18','MM/dd/yyyy'); return false;" NAME="anchor18" ID="anchor18">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" name="comments"><bean:write name="qchangeCDetails"   property="comments"/>&nbsp;</textarea> 
		</td>
	</tr>
</table>
</html:form>