
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="pcrDetails" scope="request" class="org.suny.beans.PCR2PDetailsBean"/>
<%
org.suny.beans.PCR2PDetailsBean pc = (org.suny.beans.PCR2PDetailsBean)request.getAttribute("pcrDetails");
%>
<b>001/002 PCR <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitPCRDetails('PCRP2DetailsForm')"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>
<html:form  action="/PCRP2Details" onsubmit="javascript:post('PCRP2Details', 'PCRP2div')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="pcrdate" value="<bean:write name="pcrDetails"   property="pcrdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['PCRP2DetailsForm'].pcrdate,'anchor1','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('PCRP2DetailsForm');" TITLE="cal1.select(document.forms['PCRP2DetailsForm'].pcrdate,'anchor1','MM/dd/yyyy'); return false;" NAME="anchor1" ID="anchor1">select</A>
		
		</td>

	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">Size =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="size" styleClass="textBox" value="<bean:write name="pcrDetails"   property="size"/>" />&nbsp; bp
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Amplicon<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" id="amplicony" value="" name="amplicony" onClick="javascript:selectCheckbox(this,'ampliconn');" <%if(pc.getAmplicony() != null && pc.getAmplicony().equals("true")){%> checked<%}%>  /> &nbsp;NO<input type="checkbox"  id="ampliconn" value="" name="ampliconn" onClick="javascript:selectCheckbox(this,'amplicony');" <%if(pc.getAmpliconn() != null && pc.getAmpliconn().equals("true")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" name="comments" styleClass="textBox" ><bean:write name="pcrDetails"   property="comments"/></textarea> 
		</td>
	</tr>

</table>
</html:form>