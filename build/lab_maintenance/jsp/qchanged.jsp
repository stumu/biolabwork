<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="qchangeDDetails" scope="request" class="org.suny.beans.QuickChangeDetailsBean"/>

<b>Quickchange D <img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitFormsByName('QchangeDDetailsForm','qchangeddiv');"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>')"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>

<html:form  action="/QchangeDDetails" onsubmit="javascript:post('QchangeDDetails', 'qchangeddiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="basicVal" id="basicVal" value="4" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date<span class="Required">*</span>:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="userdate" value="<bean:write name="qchangeDDetails"   property="userdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['QchangeDDetailsForm'].userdate,'anchor19','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('QchangeDDetailsForm');" TITLE="cal1.select(document.forms['QchangeDDetailsForm'].userdate,'anchor19','MM/dd/yyyy'); return false;" NAME="anchor19" ID="anchor19">select</A>
		
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" styleClass="textBox" name="comments"><bean:write name="qchangeDDetails"   property="comments"/>&nbsp;</textarea> 
		</td>
	</tr>
</table>
</html:form>