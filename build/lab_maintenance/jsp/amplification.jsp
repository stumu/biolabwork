

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="ampDetails" scope="request" class="org.suny.beans.AmplificationDetailsBean"/>


<b>Amplification purification and LIC
<% if(request.getAttribute("oid") == null) {%>
<img  src="images/save.jpg" alt="Click here to save" onclick="javascript:submitAmplificationDetails('AmpDetailsForm')"/>  &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/history.jpg" height='20' width='20' alt="Click here to save" onclick="javascript:showPopup('AmpDetails.do?geneName=<%=request.getAttribute("selectedGeneName")%>','History');"/>
 &nbsp;&nbsp;&nbsp;&nbsp;<img  src="images/problem.jpg" height='20' width='20' alt="Click here to save" /></b>
<%}%>
 <html:form  action="/AmpDetails" onsubmit="javascript:post('AmpDetails', 'ampdiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<% if(request.getAttribute("oid") != null) {%>
<input type="hidden" name="oid" id="oid" value="<%=request.getAttribute("oid")%>" />
<%}%>
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20"  name="userdate" value="<bean:write name="ampDetails"   property="userdate"/>"  styleClass="textBox" />&nbsp;
		<A HREF="#" onClick="cal1x.select(document.forms['AmpDetailsForm'].userdate,'anchor3','MM/dd/yyyy'); return false;" onchange= "javascript:fillupExpectedDate('AmpDetailsForm');" TITLE="cal1.select(document.forms['AmpDetailsForm'].userdate,'anchor3','MM/dd/yyyy'); return false;" NAME="anchor3" ID="anchor3">select</A>
		
		</td>
		
		
	</tr>
	
	<tr>
	<td nowrap class="formLabel" width="20%">[ng/ml] =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="ml" styleClass="textBox" value="<bean:write name="ampDetails"   property="ml"/>" />&nbsp; 
		</td>

	</tr>
	<tr>
	<td class="formLabel" width="20%">Volume used<br/> in LIC =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20"    name="volume" styleClass="textBox"  value="<bean:write name="ampDetails"   property="volume"/>" />&nbsp;ml
		</td>

	</tr>
	<tr>
	<td class="formLabel" width="20%">Excess =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" styleClass="textBox"  name="excess" value="<bean:write name="ampDetails"   property="excess"/>" />&nbsp;ml
		</td>

	</tr>
	<td class="formLabel" width="20%">Stored in <br/> freezer box =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="store" styleClass="textBox" value="<bean:write name="ampDetails"   property="store"/>"/>&nbsp;
		</td>

	</tr>
	<td class="formLabel" width="20%">Position =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="position" styleClass="textBox" value="<bean:write name="ampDetails"   property="position"/>"/>&nbsp;
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" name="comments" styleClass="textBox" ><bean:write name="ampDetails"   property="comments"/></textarea> 
		</td>
	</tr>
</table>
</html:form>