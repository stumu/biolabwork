
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="pcrDetails" scope="request" class="org.suny.beans.PCR2PDetailsBean"/>
<%
org.suny.beans.PCR2PDetailsBean pc = (org.suny.beans.PCR2PDetailsBean)request.getAttribute("pcrDetails");
%>

<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<input type="hidden" name="id" id="id" value="<%=request.getAttribute("id")%>" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
<tr>
		<td nowrap class="formLabel" width="20%">Date:</td>
		
		<td class="formData" width="80%"><input type="text" readonly="true"  maxlength="30" size="20" name="pcrdate" value="<bean:write name="pcrDetails"   property="pcrdate"/>"  styleClass="textBox" />&nbsp;
		
		
		</td>

	</tr>
	<tr>
	<td nowrap class="formLabel" width="20%">Size =</td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="size" styleClass="textBox" value="<bean:write name="pcrDetails"   property="size"/>" />&nbsp; bp
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Amplicon<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" id="amplicony" value="" name="amplicony" onClick="javascript:selectCheckbox(this,'ampliconn');" <%if(pc.getAmplicony() != null && pc.getAmplicony().equals("true")){%> checked<%}%>  /> &nbsp;NO<input type="checkbox"  id="ampliconn" value="" name="ampliconn" onClick="javascript:selectCheckbox(this,'amplicony');" <%if(pc.getAmpliconn() != null && pc.getAmpliconn().equals("true")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Comments: </td>
		<td class="formData" width="80%"><textarea rows="2" cols="20" name="comments" styleClass="textBox" ><bean:write name="pcrDetails"   property="comments"/></textarea> 
		</td>
	</tr>
	<td nowrap class="formLabel" width="20%">User: </td> 
		<td class="formData" width="80%"><input type="text" maxlength="30" size="20" name="size" styleClass="textBox" value="<bean:write name="pcrDetails"   property="user"/>" />
		</td>

	</tr>
	<tr>
		<td nowrap class="formLabel" width="50%"><input name="Button" type="button" class="pushButton" value="<<" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>','<%=request.getAttribute("id")%>',0)"> </td>
		<td class="formData" width="50%"><input name="Button" type="button" class="pushButton" value=">>" onclick="javascript:showPCRHistory('<%=request.getAttribute("selectedGeneName")%>','<%=request.getAttribute("id")%>',1)"></textarea> 
		</td>
	</tr>
</table>
