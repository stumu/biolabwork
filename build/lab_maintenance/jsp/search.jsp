<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<table width="100%"  class="formElements marginT5">
<tr>
<td  style="background-color:#566D7E;font-family:arial;color:BLACK;font-size:20px;">
			Search Gene
</td>
</tr>
</table>


<html:form  action="/searchGene" onsubmit="javascript:post('SearchGeneForm', 'mainbody')">
<table width="100%"  class="formElements marginT5" border='0'>
  <TR>
   <TD WIDTH="50%" valign='top'>
    <TABLE  WIDTH="100%">
     <TR>
      <TD valign='top'>
       

		<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="20%">Gene name<span class="Required">*</span>:</td>
				<td class="formData" width="80%"><html:text property="geneName" maxlength="30" size="20" styleClass="textBox"/>&nbsp;</td>
			</tr>
			<tr> <td colspan='2' style='text-align:center;'>
			<input name="Button" type="button" class="pushButton" value=">>" onclick="javascript:searchGene('SearchGeneForm')">
			</td>
			</tr>
		</table>

		

		
      </TD>
     </TR>
    </TABLE>
   </TD>
   <TD WIDTH="50%" valign='top'>
	<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
			<tr>
				<td nowrap class="formLabel" width="20%">Search results: </td>
				<td class="formData" width="80%"> 
				<html:select styleId="geneListId" property="geneName" tabindex="12" styleClass="listBox" size="10" >
				  <option value="" selected>select</option>
				  <html:options collection="geneList" property="name"/>
				</html:select>
				</td>
			</tr>
			<tr> <td colspan='2' style='text-align:left;'>
			<input name="Button" type="button" class="pushButton" value="Submit" onclick="javascript:getSelectedGene('geneListId')">
			</td>
			</tr>	
		</table>
   </TD>
  </TR>
</TABLE>
</html:form>