<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/jsp/taglibs.jsp"%>
<jsp:useBean id="geneDetails" scope="request" class="org.suny.beans.PrimaryGeneDetailsBean"/>
<%
org.suny.beans.PrimaryGeneDetailsBean p = (org.suny.beans.PrimaryGeneDetailsBean)request.getAttribute("geneDetails");
%>
<html:form  action="/basicGeneDetails" onsubmit="javascript:post('BasicGeneDetailsForm', 'basicGeneDiv')">
<input type="hidden" name="basicGeneName" id="basicGeneName" value="<%=request.getAttribute("selectedGeneName")%>" />
<table width="100%" border="0" cellspacing="1" cellpadding="0" class="formElements marginT5">
	<tr>
		<td nowrap class="formLabel" width="20%">Gene length<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><input type="text" name="geneLength" styleClass="textBox" value="<bean:write name="geneDetails"   property="geneLength"/>"&nbsp;/>&nbsp;bp</td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Gene location<span class="Required">*</span>:</td>
		<td class="formData" width="80%"><input type="text" name="geneLocationStart" styleClass="textBox" value="<bean:write name="geneDetails"   property="geneLocationStart"/>"&nbsp;/>&nbsp;-&nbsp;<input type="text" name="geneLocationEnd" styleClass="textBox" value="<bean:write name="geneDetails"   property="geneLocationEnd"/>"&nbsp;/> </td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">Strand<span class="Required">*</span>:</td>
		<td class="formData" width="80%">(-)<input id="nstrand" type="checkbox" onClick="javascript:selectCheckbox(this,'pstrand');" name="nstrand" value=""
		<%if(p.getNegativeStrand().equals("true")){%> checked<%}%>
		/> &nbsp;(+)<input  id="pstrand" type="checkbox" onClick="javascript:selectCheckbox(this,'nstrand');" name="pstrand" 
		<%if(p.getPositiveStrand().equals("true")){%> checked<%}%>/></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">N-terminal overlap<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" name="ntoverlapy" onClick="javascript:selectCheckbox(this,'ntoverlapn');" id="ntoverlapy" value="" <%if(p.getNtermY().equals("true")){%> checked<%}%> /> &nbsp;NO<input type="checkbox" name="ntoverlapn"  id="ntoverlapn" value="" onClick="javascript:selectCheckbox(this,'ntoverlapy');" <%if(p.getNtermN().equals("true")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel" width="20%">C-terminal overlap<span class="Required">*</span>:</td>
		<td class="formData" width="80%">YES<input type="checkbox" onClick="javascript:selectCheckbox(this,'ctoverlapn');" id="ctoverlapy" name="ctoverlapy" value="" <%if(p.getCtermY().equals("true")){%> checked<%}%> /> &nbsp;NO<input type="checkbox" id="ctoverlapn" onClick="javascript:selectCheckbox(this,'ctoverlapy');" name="ctoverlapn" value="" <%if(p.getCtermN().equals("true")){%> checked<%}%> /></td>
	</tr>
	<tr>
		<td nowrap class="formLabel">Comments:</td>
		<td class="formData"><textarea  name="geneDetails" cols="55" rows="6" /><bean:write name="geneDetails"   property="comments"/></textarea></td>
	</tr>
	

	
	
	
</table>

</br>
<input name="Button" type="button" class="pushButton" value="Submit" onclick="javascript:submitBasicGeneDetails('BasicGeneDetailsForm')">

</html:form>