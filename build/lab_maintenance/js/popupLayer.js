var popupTitle, popupTitle1;
var debugWin = null;

function showPopup(url, title)
{
	//alert("showPopup("+url+")");
	//popupLayerFlag = "get"
	//showWaiting();
	if(title != null) popupTitle = title;
	//window.popupContentFrame.location = url;
	//if(divName == null) divName = 'contentArea';
	var getAjaxQuery = new Ajax.Updater(
				"issueDetailsLayer",
				url,
				{
					method: 'get',
					evalScripts: true,
					onComplete: showPopupInfoLayer(url)
				});
}

function showWaiting()
{
	//alert("showwaiting");
	loadTimerStart = new Date();
	window.status = "Loading page. Please wait ...";
	var waitDivHeight = window.document.body.clientHeight - 80
	var waitDivWidth = window.document.body.clientWidth;
	if(window.document.body.scrollHeight > window.document.body.clientHeight)
		waitDivHeight = window.document.body.scrollHeight - 50
	if(window.document.body.scrollWidth > window.document.body.clientWidth)
		waitDivWidth = window.document.body.scrollWidth

	if(window.document.getElementById("waitDiv") != null)
	{
		window.document.getElementById("waitDiv").style.height = waitDivHeight;
		window.document.getElementById("waitDiv").style.width = waitDivWidth;
		window.document.getElementById("waitDiv").style.display = "block"
	}
}

function hideWaiting()
{
	//alert("hidewaiting");
	if(window.document.getElementById("waitDiv") != null)
		window.document.getElementById("waitDiv").style.display = "none";
		//window.document.getElementById("waitDiv").style.visibility = "hidden";
}


function showTicketDetail(ticketId, subject)
{
	//alert("showTicketDetail("+ticketId+")");
	var url = "index.php?action=ticketDetails&ticketId="+ticketId;
	popupTitle1 = "Ticket #"+ticketId+" - "+subject;
	var getAjaxQuery = new Ajax.Updater(
				"issueDetailsLayer1",
				url,
				{
					method: 'get',
					evalScripts: true,
					onComplete: showPopupInfoLayer1
				});
}

function showDeviceMonitorPage(vpmgId,vpmgDeviceId, param, title,monitorInstance)//,fromDate,toDate,dropDownVal)
{
        //alert("passing to fun: "+monitorInstance);
        //alert("after fun: "+monitorInstance);
        fromDate = document.timeSelectForm.fromDate.value;
        toDate = document.timeSelectForm.toDate.value;
        dropDownVal = document.timeSelectForm.dateDropdown.value;
        showPopup("index.php?action=deviceMonitorPage&vpmgId="+escape(vpmgId)+"&vpmgdeviceId="+escape(vpmgDeviceId)+"&param="+escape(param)+"&monitorInstance="+escape(monitorInstance)+"&fromDate1="+fromDate+"&toDate1="+toDate+"&dateDropdown="+dropDownVal+"&mode=firstTime", title);
}

var showLayerTimer = null;
	function showPopupInfoLayer1()
	{
		dragged = false;// fresh popup
		if(popupTitle1 != null)
		{
			window.document.getElementById("issueTitle1").title = popupTitle1; // to show full name on mouse over
			if(popupTitle1.length > 50) //In the title section, only 50 characters are showing
				popupTitle1 = popupTitle1.substring(0,50);
			window.document.getElementById("issueTitle1").innerHTML = "Info: "+popupTitle1

		}
		window.document.getElementById("issueLayer1").style.display = "block"
		//window.document.getElementById("issueLayer1").style.visibility = "visible";		
		popupTitle1 = null;
		positionPopup("issueLayer1");
	}
	
	var docWidth, docHeight;
	function showPopupInfoLayer(url)
	{
		//alert("show popup info layer");
		dragged = false;// fresh popup
		//issuesTab = new DynLayer("issueLayer");
		newPosition = false;

		docWidth = document.body.clientWidth;
		docHeight = document.body.clientHeight;
		
		if(document.getElementById("issueLayer").style.display == "none")
			newPosition = true;
			//issuesTab.moveTo(o3_x, o3_y+10);*/
		if(popupTitle != null)
			document.getElementById("issueTitle").innerHTML = "Info: "+popupTitle
		document.getElementById("issueLayer").style.display = "block";
		if(newPosition)
			positionPopup("issueLayer");			
		
		var temp = new Array();
		temp = url.split('&direc=');;
		url = temp[0];
		//alert(oid);
		document.getElementById("<<").onclick = function()
			{
						var oid = 0;
						if(document.getElementById("oid") != null)
							oid = document.getElementById("oid").value;
						showPopup(url+'&direc=0&oid='+oid,'History',1);
			}
			document.getElementById(">>").onclick = function()
			{
						var oid = 0;
						if(document.getElementById("oid") != null)
							oid = document.getElementById("oid").value;	
						showPopup(url+'&direc=1&oid='+oid,'History',1);
			}
		//alert(document.getElementById("<<").onclick );
		//if(showLayerTimer != null)
			//window.clearTimeout(showLayerTimer)
		//showLayerTimer = window.setTimeout("closeIdlePopupLayer()", 5000);
		popupTitle = null;
	}

	function positionPopup(divName)
	{
		//alert(divName);
		//winAlert("body size ["+docWidth+", "+docHeight+"]");
		//winAlert("popup size ["+$(divName).clientWidth+", "+$(divName).clientHeight+"]");
		//window.document.getElementById("issueLayer").style.visibility = "hidden";
		// check if popup is within the visible boundary		
		
		popupLeft = 0, 
		
		popupTop = o3_y+20;
		
		popupWidth = document.getElementById(divName).clientWidth;
		popupHeight = document.getElementById(divName).clientHeight;
		 
		document.getElementById(divName).style.display = "none";
		popupLeft = o3_x-popupWidth/2;
		if(popupTop+popupHeight > document.body.clientHeight){
			if(popupHeight < document.body.clientHeight){
				popupTop = document.body.scrollTop + 20;//o3_y-popupHeight-30;
				popupLeft = o3_x+30;
			}
			//if(popupTop <= 0)
		 	//popupTop = 100;
		}
		
		//popupLeft = o3_x;
		if(popupLeft+popupWidth > document.body.clientWidth) // popup overflowing to the right
		{
			if(popupWidth < document.body.clientWidth)
			{
				popupLeft = document.body.clientWidth-popupWidth-50 ;
			}
			//else popupLeft = 0;
		}
		if(popupLeft < 0) popupLeft = 5;
		//alert("left: "+popupLeft);
		//alert("top: "+popupTop);
		document.getElementById(divName).style.left = popupLeft;
		document.getElementById(divName).style.top = popupTop;
		//if(divName == "issueLayer1")
			//Effect.Grow(divName);
		//Effect.Appear(divName);
		//alert(divName);
		//Query("#"+divName).fadeIn("slow");
		document.getElementById(divName).style.display  = "block";	
		document.getElementById(divName).style.visibility  = "visible";	
	}

	function closeIdlePopupLayer()
	{
		//alert("drag is:"+dragged)
		if(dragged)
		{
			dragged = false;
			return;
		}
		closeIssue()
	}

	function closeIssue()
	{
		//Effect.Fade("issueLayer");
		jQuery("#issueLayer").fadeOut("slow");
	}

	function closeIssue1()
	{
		//Effect.Fade("issueLayer1");
		jQuery("#issueLayer1").fadeOut("slow");
	}


function dashboardPopup(view,id,param,monit,dev,fromDate,toDate) {

if(param=='cpu-util')
	title='CPU Utilization';
else if(param=='disk-util')
	title='Disk Utilization';
else if(param=='network-util')
	title='Network Utilization';

	showPopup('index.php?action=devicesInfo&view='+view+'&id='+id+'&param='+param+'&monitID='+monit+'&dev='+dev+'&fromDate1='+fromDate+'&toDate1='+toDate,title);
}

function viewAttachments(url, subject)
{
	popupTitle1 = subject;
	var getAjaxQuery = new Ajax.Updater(
				"issueDetailsLayer1",
				url,
				{
					method: 'get',
					evalScripts: true,
					onComplete: showPopupInfoLayer1
				});
}

function openCustomPopup(url,title){
	popupTitle1 = title;
	var getAjaxQuery = new Ajax.Updater(
				"issueDetailsLayer1",
				url,
				{
					method: 'get',
					evalScripts: true,
					onComplete: showPopupInfoLayer1
				});
	
}
