function getHTTPRequestObject() {
    var xmlHttpRequest;
/*@cc_on
@if (@_jscript_version >= 5)
    try {
        xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (exception1) {
        try {
            xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (exception2) {
			xmlhttp = false;
        }
    }
@else
    xmlhttpRequest = false;
@end @*/

    if (!xmlHttpRequest && typeof XMLHttpRequest != 'undefined') {
        try {
            xmlHttpRequest = new XMLHttpRequest();
        } catch (exception) {
			 xmlHttpRequest = false;
        }
    }
    return xmlHttpRequest;
}

function showLoading()
{
    msg = document.getElementById('loadmsg');
    if (msg) {
        msg.innerHTML = 'Loading...';
        msg.className ='runningText';
        msg.style.visibility='visible';
    }
}

function hideLoading()
{
    msg = document.getElementById('loadmsg');
    if (msg) {
        msg.innerHTML = '';
        msg.style.visibility='hidden';
    }
}



var xmlhttp = getHTTPRequestObject();
var division;
var xmlhttpLoaded = false;
var loadTimerStart = new Date();

function showWaiting()
{
	loadTimerStart = new Date()
	window.status = "Loading page. Please wait ..."
	var waitDivHeight = window.document.body.clientHeight - 80
	var waitDivWidth = window.document.body.clientWidth
	if(window.document.body.scrollHeight > window.document.body.clientHeight)
		waitDivHeight = window.document.body.scrollHeight - 50
	if(window.document.body.scrollWidth > window.document.body.clientWidth)
		waitDivWidth = window.document.body.scrollWidth

	if(window.document.getElementById("waitDiv") != null)
	{
		window.document.getElementById("waitDiv").style.height = waitDivHeight;
		window.document.getElementById("waitDiv").style.width = waitDivWidth;
		window.document.getElementById("waitDiv").style.display = "block"
	}
}


function hideWaiting()
{
	
	
	if(window.document.getElementById("waitDiv") != null)
		window.document.getElementById("waitDiv").style.display = "none";
	//window.document.getElementById("waitDiv").style.visibility = "hidden";
}

function showDownloadInfo(contentSize)
{
	loadTimerEnd = new Date();
	tDiff = loadTimerEnd.getTime()-loadTimerStart.getTime();
	// calculate download info
	showInfo("Received "+contentSize+" bytes in "+tDiff+" milliseconds. Virtual bandwidth is "+((contentSize*1000.0)/(1024.0*tDiff)).toFixed(2)+" Kilo bytes per second.")
}

function showInfo(text)
{
	window.status = text;
}

function postForm(formObject, divName){
	showWaiting()
	getFlag = "get"
	getURL = formObject.action
	if(divName) divisionName = divName
	formObject.target = "mainbody"
	formObject.submit()
}
function post(formName, divName){
	//postForm(window.document.forms[formName], divName);
	//formPost(window.document.forms[formName], divName);
	formPost(formName, divName);
}

function formPost(formName, divName)
{
	showWaiting();
	if(divName == null) divName = 'mainbody';
	var getAjax = new Ajax.Updater(
				divName,
				document.forms[formName].action,
				{
					method: 'get',
					parameters: Form.serialize(document.forms[formName]), //parameters,
					evalScripts: true,
					onComplete: hideWaiting
				});
}




/*function get(url, divName)
{
	showWaiting()
	getFlag = "get";
	getItems = new Array();
	xmlhttp.abort();
	getURL = url;
	if(divName) divisionName = divName;
	window.contentFrame.location = url;
}
*/

function get(url, divName,parameters)
{
   // if(showWaitingDiv != false)
		showWaiting();
			
	var getAjax = new Ajax.Updater(
				divName,
				url,
				{
					method: 'get',
					parameters: parameters,
					evalScripts: true,
					onComplete: hideWaiting
				});
}
function clearDashboardTimer() {
	 if(timerId) {
	  window.clearInterval(timerId);
      timerId = undefined;
	 }
}

function sessionout() {
    if (window.opener) {
        window.opener.top.location="login.do?sessionout=true";
        window.close()
    } else {
        top.location="login.do?sessionout=true";
    }
}

function execJSMethod(xml) {
    try {
        var method = xml.getElementsByTagName('method')[0].firstChild.data;
        if (method) {
            //var parameter = xml.getElementsByTagName('result')[0].firstChild.data;
            eval(method + '()');
            return true;
        }
    } catch (exception) {
    }
    return false;
}

function handleResponse() {
    return handleXmlHttpResponse(xmlhttp);
}

function handleXmlHttpResponse(req) {

    if (req.readyState == 3) {
        xmlhttpLoaded = true;
    } else if (req.readyState == 4) {
        hideLoading();
        try {
            if (req.status == 200) {
                if ( !(req.responseXML && execJSMethod(req.responseXML.documentElement)) ) {
			//alert(req.responseText)
                    if (division) {
                        division.innerHTML = req.responseText;

                        return true;
                    }
                }
            } else {
                //TODO: handle errors
                if (req.status == 12029) {
                    //WinInet error
                    alert("12029 - The attempt to connect to the server failed.");
                    return false;
                }
            }
        } catch (exception) {
			if(xmlhttp) {
				if (!xmlhttpLoaded) {
					alert("The attempt to connect to the server failed.");
					return false;
				}
			}
		}
    }
    return false;
}

