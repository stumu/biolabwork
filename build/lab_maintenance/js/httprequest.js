function post(formName, divName)
{
	getFlag = "get"
	getURL = window.document.forms[formName].action
	if(divName) divisionName = divName
		//window.top.document.getElementById("divisionName").innerHTML = divName;
	window.document.forms[formName].target = "contentFrame"
	window.document.forms[formName].submit()
}


function get(url, divName)
{
	getFlag = "get";
	getURL = url;
	if(divName) divisionName = divName;
	window.contentFrame.location = url;
}

function getHTTPRequestObject() {
    var xmlHttpRequest;
/*@cc_on
@if (@_jscript_version >= 5)
    try {
        xmlHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (exception1) {
        try {
            xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (exception2) {
			xmlhttp = false;
        }
    }
@else
    xmlhttpRequest = false;
@end @*/

    if (!xmlHttpRequest && typeof XMLHttpRequest != 'undefined') {
        try {
            xmlHttpRequest = new XMLHttpRequest();
        } catch (exception) {
			 xmlHttpRequest = false;
        }
    }
    return xmlHttpRequest;
}
